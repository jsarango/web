package com.hackaton.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hackaton.core.web.rest.TestUtil;

public class ComercialTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Comercial.class);
        Comercial comercial1 = new Comercial();
        comercial1.setId(1L);
        Comercial comercial2 = new Comercial();
        comercial2.setId(comercial1.getId());
        assertThat(comercial1).isEqualTo(comercial2);
        comercial2.setId(2L);
        assertThat(comercial1).isNotEqualTo(comercial2);
        comercial1.setId(null);
        assertThat(comercial1).isNotEqualTo(comercial2);
    }
}
