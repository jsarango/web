package com.hackaton.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hackaton.core.web.rest.TestUtil;

public class DesercionTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Desercion.class);
        Desercion desercion1 = new Desercion();
        desercion1.setId(1L);
        Desercion desercion2 = new Desercion();
        desercion2.setId(desercion1.getId());
        assertThat(desercion1).isEqualTo(desercion2);
        desercion2.setId(2L);
        assertThat(desercion1).isNotEqualTo(desercion2);
        desercion1.setId(null);
        assertThat(desercion1).isNotEqualTo(desercion2);
    }
}
