package com.hackaton.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hackaton.core.web.rest.TestUtil;

public class PerfilAhorroTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PerfilAhorro.class);
        PerfilAhorro perfilAhorro1 = new PerfilAhorro();
        perfilAhorro1.setId(1L);
        PerfilAhorro perfilAhorro2 = new PerfilAhorro();
        perfilAhorro2.setId(perfilAhorro1.getId());
        assertThat(perfilAhorro1).isEqualTo(perfilAhorro2);
        perfilAhorro2.setId(2L);
        assertThat(perfilAhorro1).isNotEqualTo(perfilAhorro2);
        perfilAhorro1.setId(null);
        assertThat(perfilAhorro1).isNotEqualTo(perfilAhorro2);
    }
}
