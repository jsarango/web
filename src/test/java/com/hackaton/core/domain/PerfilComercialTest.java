package com.hackaton.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hackaton.core.web.rest.TestUtil;

public class PerfilComercialTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PerfilComercial.class);
        PerfilComercial perfilComercial1 = new PerfilComercial();
        perfilComercial1.setId(1L);
        PerfilComercial perfilComercial2 = new PerfilComercial();
        perfilComercial2.setId(perfilComercial1.getId());
        assertThat(perfilComercial1).isEqualTo(perfilComercial2);
        perfilComercial2.setId(2L);
        assertThat(perfilComercial1).isNotEqualTo(perfilComercial2);
        perfilComercial1.setId(null);
        assertThat(perfilComercial1).isNotEqualTo(perfilComercial2);
    }
}
