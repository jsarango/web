package com.hackaton.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hackaton.core.web.rest.TestUtil;

public class FondoTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Fondo.class);
        Fondo fondo1 = new Fondo();
        fondo1.setId(1L);
        Fondo fondo2 = new Fondo();
        fondo2.setId(fondo1.getId());
        assertThat(fondo1).isEqualTo(fondo2);
        fondo2.setId(2L);
        assertThat(fondo1).isNotEqualTo(fondo2);
        fondo1.setId(null);
        assertThat(fondo1).isNotEqualTo(fondo2);
    }
}
