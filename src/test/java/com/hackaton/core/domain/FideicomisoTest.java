package com.hackaton.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hackaton.core.web.rest.TestUtil;

public class FideicomisoTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Fideicomiso.class);
        Fideicomiso fideicomiso1 = new Fideicomiso();
        fideicomiso1.setId(1L);
        Fideicomiso fideicomiso2 = new Fideicomiso();
        fideicomiso2.setId(fideicomiso1.getId());
        assertThat(fideicomiso1).isEqualTo(fideicomiso2);
        fideicomiso2.setId(2L);
        assertThat(fideicomiso1).isNotEqualTo(fideicomiso2);
        fideicomiso1.setId(null);
        assertThat(fideicomiso1).isNotEqualTo(fideicomiso2);
    }
}
