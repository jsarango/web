package com.hackaton.core.web.rest;

import com.hackaton.core.FrontApp;
import com.hackaton.core.domain.Comercial;
import com.hackaton.core.repository.ComercialRepository;
import com.hackaton.core.service.ComercialService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ComercialResource} REST controller.
 */
@SpringBootTest(classes = FrontApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ComercialResourceIT {

    private static final String DEFAULT_CODIGO = "AAAAA";
    private static final String UPDATED_CODIGO = "BBBBB";

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    @Autowired
    private ComercialRepository comercialRepository;

    @Autowired
    private ComercialService comercialService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restComercialMockMvc;

    private Comercial comercial;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Comercial createEntity(EntityManager em) {
        Comercial comercial = new Comercial()
            .codigo(DEFAULT_CODIGO)
            .nombre(DEFAULT_NOMBRE);
        return comercial;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Comercial createUpdatedEntity(EntityManager em) {
        Comercial comercial = new Comercial()
            .codigo(UPDATED_CODIGO)
            .nombre(UPDATED_NOMBRE);
        return comercial;
    }

    @BeforeEach
    public void initTest() {
        comercial = createEntity(em);
    }

    @Test
    @Transactional
    public void createComercial() throws Exception {
        int databaseSizeBeforeCreate = comercialRepository.findAll().size();
        // Create the Comercial
        restComercialMockMvc.perform(post("/api/comercials")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(comercial)))
            .andExpect(status().isCreated());

        // Validate the Comercial in the database
        List<Comercial> comercialList = comercialRepository.findAll();
        assertThat(comercialList).hasSize(databaseSizeBeforeCreate + 1);
        Comercial testComercial = comercialList.get(comercialList.size() - 1);
        assertThat(testComercial.getCodigo()).isEqualTo(DEFAULT_CODIGO);
        assertThat(testComercial.getNombre()).isEqualTo(DEFAULT_NOMBRE);
    }

    @Test
    @Transactional
    public void createComercialWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = comercialRepository.findAll().size();

        // Create the Comercial with an existing ID
        comercial.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restComercialMockMvc.perform(post("/api/comercials")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(comercial)))
            .andExpect(status().isBadRequest());

        // Validate the Comercial in the database
        List<Comercial> comercialList = comercialRepository.findAll();
        assertThat(comercialList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllComercials() throws Exception {
        // Initialize the database
        comercialRepository.saveAndFlush(comercial);

        // Get all the comercialList
        restComercialMockMvc.perform(get("/api/comercials?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(comercial.getId().intValue())))
            .andExpect(jsonPath("$.[*].codigo").value(hasItem(DEFAULT_CODIGO)))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE)));
    }
    
    @Test
    @Transactional
    public void getComercial() throws Exception {
        // Initialize the database
        comercialRepository.saveAndFlush(comercial);

        // Get the comercial
        restComercialMockMvc.perform(get("/api/comercials/{id}", comercial.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(comercial.getId().intValue()))
            .andExpect(jsonPath("$.codigo").value(DEFAULT_CODIGO))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE));
    }
    @Test
    @Transactional
    public void getNonExistingComercial() throws Exception {
        // Get the comercial
        restComercialMockMvc.perform(get("/api/comercials/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateComercial() throws Exception {
        // Initialize the database
        comercialService.save(comercial);

        int databaseSizeBeforeUpdate = comercialRepository.findAll().size();

        // Update the comercial
        Comercial updatedComercial = comercialRepository.findById(comercial.getId()).get();
        // Disconnect from session so that the updates on updatedComercial are not directly saved in db
        em.detach(updatedComercial);
        updatedComercial
            .codigo(UPDATED_CODIGO)
            .nombre(UPDATED_NOMBRE);

        restComercialMockMvc.perform(put("/api/comercials")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedComercial)))
            .andExpect(status().isOk());

        // Validate the Comercial in the database
        List<Comercial> comercialList = comercialRepository.findAll();
        assertThat(comercialList).hasSize(databaseSizeBeforeUpdate);
        Comercial testComercial = comercialList.get(comercialList.size() - 1);
        assertThat(testComercial.getCodigo()).isEqualTo(UPDATED_CODIGO);
        assertThat(testComercial.getNombre()).isEqualTo(UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void updateNonExistingComercial() throws Exception {
        int databaseSizeBeforeUpdate = comercialRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restComercialMockMvc.perform(put("/api/comercials")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(comercial)))
            .andExpect(status().isBadRequest());

        // Validate the Comercial in the database
        List<Comercial> comercialList = comercialRepository.findAll();
        assertThat(comercialList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteComercial() throws Exception {
        // Initialize the database
        comercialService.save(comercial);

        int databaseSizeBeforeDelete = comercialRepository.findAll().size();

        // Delete the comercial
        restComercialMockMvc.perform(delete("/api/comercials/{id}", comercial.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Comercial> comercialList = comercialRepository.findAll();
        assertThat(comercialList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
