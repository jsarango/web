package com.hackaton.core.web.rest;

import com.hackaton.core.FrontApp;
import com.hackaton.core.domain.PerfilAhorro;
import com.hackaton.core.repository.PerfilAhorroRepository;
import com.hackaton.core.service.PerfilAhorroService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PerfilAhorroResource} REST controller.
 */
@SpringBootTest(classes = FrontApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PerfilAhorroResourceIT {

    private static final String DEFAULT_CODIGO = "AAAAAAAAAA";
    private static final String UPDATED_CODIGO = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPCION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION = "BBBBBBBBBB";

    @Autowired
    private PerfilAhorroRepository perfilAhorroRepository;

    @Autowired
    private PerfilAhorroService perfilAhorroService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPerfilAhorroMockMvc;

    private PerfilAhorro perfilAhorro;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PerfilAhorro createEntity(EntityManager em) {
        PerfilAhorro perfilAhorro = new PerfilAhorro()
            .codigo(DEFAULT_CODIGO)
            .descripcion(DEFAULT_DESCRIPCION);
        return perfilAhorro;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PerfilAhorro createUpdatedEntity(EntityManager em) {
        PerfilAhorro perfilAhorro = new PerfilAhorro()
            .codigo(UPDATED_CODIGO)
            .descripcion(UPDATED_DESCRIPCION);
        return perfilAhorro;
    }

    @BeforeEach
    public void initTest() {
        perfilAhorro = createEntity(em);
    }

    @Test
    @Transactional
    public void createPerfilAhorro() throws Exception {
        int databaseSizeBeforeCreate = perfilAhorroRepository.findAll().size();
        // Create the PerfilAhorro
        restPerfilAhorroMockMvc.perform(post("/api/perfil-ahorros")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(perfilAhorro)))
            .andExpect(status().isCreated());

        // Validate the PerfilAhorro in the database
        List<PerfilAhorro> perfilAhorroList = perfilAhorroRepository.findAll();
        assertThat(perfilAhorroList).hasSize(databaseSizeBeforeCreate + 1);
        PerfilAhorro testPerfilAhorro = perfilAhorroList.get(perfilAhorroList.size() - 1);
        assertThat(testPerfilAhorro.getCodigo()).isEqualTo(DEFAULT_CODIGO);
        assertThat(testPerfilAhorro.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
    }

    @Test
    @Transactional
    public void createPerfilAhorroWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = perfilAhorroRepository.findAll().size();

        // Create the PerfilAhorro with an existing ID
        perfilAhorro.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPerfilAhorroMockMvc.perform(post("/api/perfil-ahorros")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(perfilAhorro)))
            .andExpect(status().isBadRequest());

        // Validate the PerfilAhorro in the database
        List<PerfilAhorro> perfilAhorroList = perfilAhorroRepository.findAll();
        assertThat(perfilAhorroList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPerfilAhorros() throws Exception {
        // Initialize the database
        perfilAhorroRepository.saveAndFlush(perfilAhorro);

        // Get all the perfilAhorroList
        restPerfilAhorroMockMvc.perform(get("/api/perfil-ahorros?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(perfilAhorro.getId().intValue())))
            .andExpect(jsonPath("$.[*].codigo").value(hasItem(DEFAULT_CODIGO)))
            .andExpect(jsonPath("$.[*].descripcion").value(hasItem(DEFAULT_DESCRIPCION)));
    }
    
    @Test
    @Transactional
    public void getPerfilAhorro() throws Exception {
        // Initialize the database
        perfilAhorroRepository.saveAndFlush(perfilAhorro);

        // Get the perfilAhorro
        restPerfilAhorroMockMvc.perform(get("/api/perfil-ahorros/{id}", perfilAhorro.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(perfilAhorro.getId().intValue()))
            .andExpect(jsonPath("$.codigo").value(DEFAULT_CODIGO))
            .andExpect(jsonPath("$.descripcion").value(DEFAULT_DESCRIPCION));
    }
    @Test
    @Transactional
    public void getNonExistingPerfilAhorro() throws Exception {
        // Get the perfilAhorro
        restPerfilAhorroMockMvc.perform(get("/api/perfil-ahorros/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePerfilAhorro() throws Exception {
        // Initialize the database
        perfilAhorroService.save(perfilAhorro);

        int databaseSizeBeforeUpdate = perfilAhorroRepository.findAll().size();

        // Update the perfilAhorro
        PerfilAhorro updatedPerfilAhorro = perfilAhorroRepository.findById(perfilAhorro.getId()).get();
        // Disconnect from session so that the updates on updatedPerfilAhorro are not directly saved in db
        em.detach(updatedPerfilAhorro);
        updatedPerfilAhorro
            .codigo(UPDATED_CODIGO)
            .descripcion(UPDATED_DESCRIPCION);

        restPerfilAhorroMockMvc.perform(put("/api/perfil-ahorros")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedPerfilAhorro)))
            .andExpect(status().isOk());

        // Validate the PerfilAhorro in the database
        List<PerfilAhorro> perfilAhorroList = perfilAhorroRepository.findAll();
        assertThat(perfilAhorroList).hasSize(databaseSizeBeforeUpdate);
        PerfilAhorro testPerfilAhorro = perfilAhorroList.get(perfilAhorroList.size() - 1);
        assertThat(testPerfilAhorro.getCodigo()).isEqualTo(UPDATED_CODIGO);
        assertThat(testPerfilAhorro.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
    }

    @Test
    @Transactional
    public void updateNonExistingPerfilAhorro() throws Exception {
        int databaseSizeBeforeUpdate = perfilAhorroRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPerfilAhorroMockMvc.perform(put("/api/perfil-ahorros")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(perfilAhorro)))
            .andExpect(status().isBadRequest());

        // Validate the PerfilAhorro in the database
        List<PerfilAhorro> perfilAhorroList = perfilAhorroRepository.findAll();
        assertThat(perfilAhorroList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePerfilAhorro() throws Exception {
        // Initialize the database
        perfilAhorroService.save(perfilAhorro);

        int databaseSizeBeforeDelete = perfilAhorroRepository.findAll().size();

        // Delete the perfilAhorro
        restPerfilAhorroMockMvc.perform(delete("/api/perfil-ahorros/{id}", perfilAhorro.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PerfilAhorro> perfilAhorroList = perfilAhorroRepository.findAll();
        assertThat(perfilAhorroList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
