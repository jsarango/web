package com.hackaton.core.web.rest;

import com.hackaton.core.FrontApp;
import com.hackaton.core.domain.Desercion;
import com.hackaton.core.domain.Cliente;
import com.hackaton.core.domain.Fondo;
import com.hackaton.core.domain.Fideicomiso;
import com.hackaton.core.domain.Comercial;
import com.hackaton.core.domain.PerfilAhorro;
import com.hackaton.core.domain.PerfilComercial;
import com.hackaton.core.repository.DesercionRepository;
import com.hackaton.core.service.DesercionService;
import com.hackaton.core.service.dto.DesercionCriteria;
import com.hackaton.core.service.DesercionQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DesercionResource} REST controller.
 */
@SpringBootTest(classes = FrontApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class DesercionResourceIT {

    private static final LocalDate DEFAULT_FECHA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_FECHA = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_EDAD = "AAA";
    private static final String UPDATED_EDAD = "BBB";

    private static final Integer DEFAULT_NRO_PROMEDIO_RETIROS = 1;
    private static final Integer UPDATED_NRO_PROMEDIO_RETIROS = 2;
    private static final Integer SMALLER_NRO_PROMEDIO_RETIROS = 1 - 1;

    private static final Integer DEFAULT_NRO_PROMEDIO_APORTES = 1;
    private static final Integer UPDATED_NRO_PROMEDIO_APORTES = 2;
    private static final Integer SMALLER_NRO_PROMEDIO_APORTES = 1 - 1;

    private static final Integer DEFAULT_NETO_PROM_MOVIMIENTOS = 1;
    private static final Integer UPDATED_NETO_PROM_MOVIMIENTOS = 2;
    private static final Integer SMALLER_NETO_PROM_MOVIMIENTOS = 1 - 1;

    private static final Integer DEFAULT_DIAS_ULTIMO_MOV = 1;
    private static final Integer UPDATED_DIAS_ULTIMO_MOV = 2;
    private static final Integer SMALLER_DIAS_ULTIMO_MOV = 1 - 1;

    private static final Boolean DEFAULT_POSIBLE_SUL = false;
    private static final Boolean UPDATED_POSIBLE_SUL = true;

    @Autowired
    private DesercionRepository desercionRepository;

    @Autowired
    private DesercionService desercionService;

    @Autowired
    private DesercionQueryService desercionQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDesercionMockMvc;

    private Desercion desercion;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Desercion createEntity(EntityManager em) {
        Desercion desercion = new Desercion()
            .fecha(DEFAULT_FECHA)
            .edad(DEFAULT_EDAD)
            .nroPromedioRetiros(DEFAULT_NRO_PROMEDIO_RETIROS)
            .nroPromedioAportes(DEFAULT_NRO_PROMEDIO_APORTES)
            .netoPromMovimientos(DEFAULT_NETO_PROM_MOVIMIENTOS)
            .diasUltimoMov(DEFAULT_DIAS_ULTIMO_MOV)
            .posibleSul(DEFAULT_POSIBLE_SUL);
        return desercion;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Desercion createUpdatedEntity(EntityManager em) {
        Desercion desercion = new Desercion()
            .fecha(UPDATED_FECHA)
            .edad(UPDATED_EDAD)
            .nroPromedioRetiros(UPDATED_NRO_PROMEDIO_RETIROS)
            .nroPromedioAportes(UPDATED_NRO_PROMEDIO_APORTES)
            .netoPromMovimientos(UPDATED_NETO_PROM_MOVIMIENTOS)
            .diasUltimoMov(UPDATED_DIAS_ULTIMO_MOV)
            .posibleSul(UPDATED_POSIBLE_SUL);
        return desercion;
    }

    @BeforeEach
    public void initTest() {
        desercion = createEntity(em);
    }

    @Test
    @Transactional
    public void createDesercion() throws Exception {
        int databaseSizeBeforeCreate = desercionRepository.findAll().size();
        // Create the Desercion
        restDesercionMockMvc.perform(post("/api/desercions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(desercion)))
            .andExpect(status().isCreated());

        // Validate the Desercion in the database
        List<Desercion> desercionList = desercionRepository.findAll();
        assertThat(desercionList).hasSize(databaseSizeBeforeCreate + 1);
        Desercion testDesercion = desercionList.get(desercionList.size() - 1);
        assertThat(testDesercion.getFecha()).isEqualTo(DEFAULT_FECHA);
        assertThat(testDesercion.getEdad()).isEqualTo(DEFAULT_EDAD);
        assertThat(testDesercion.getNroPromedioRetiros()).isEqualTo(DEFAULT_NRO_PROMEDIO_RETIROS);
        assertThat(testDesercion.getNroPromedioAportes()).isEqualTo(DEFAULT_NRO_PROMEDIO_APORTES);
        assertThat(testDesercion.getNetoPromMovimientos()).isEqualTo(DEFAULT_NETO_PROM_MOVIMIENTOS);
        assertThat(testDesercion.getDiasUltimoMov()).isEqualTo(DEFAULT_DIAS_ULTIMO_MOV);
        assertThat(testDesercion.isPosibleSul()).isEqualTo(DEFAULT_POSIBLE_SUL);
    }

    @Test
    @Transactional
    public void createDesercionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = desercionRepository.findAll().size();

        // Create the Desercion with an existing ID
        desercion.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDesercionMockMvc.perform(post("/api/desercions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(desercion)))
            .andExpect(status().isBadRequest());

        // Validate the Desercion in the database
        List<Desercion> desercionList = desercionRepository.findAll();
        assertThat(desercionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDesercions() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList
        restDesercionMockMvc.perform(get("/api/desercions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(desercion.getId().intValue())))
            .andExpect(jsonPath("$.[*].fecha").value(hasItem(DEFAULT_FECHA.toString())))
            .andExpect(jsonPath("$.[*].edad").value(hasItem(DEFAULT_EDAD)))
            .andExpect(jsonPath("$.[*].nroPromedioRetiros").value(hasItem(DEFAULT_NRO_PROMEDIO_RETIROS)))
            .andExpect(jsonPath("$.[*].nroPromedioAportes").value(hasItem(DEFAULT_NRO_PROMEDIO_APORTES)))
            .andExpect(jsonPath("$.[*].netoPromMovimientos").value(hasItem(DEFAULT_NETO_PROM_MOVIMIENTOS)))
            .andExpect(jsonPath("$.[*].diasUltimoMov").value(hasItem(DEFAULT_DIAS_ULTIMO_MOV)))
            .andExpect(jsonPath("$.[*].posibleSul").value(hasItem(DEFAULT_POSIBLE_SUL.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getDesercion() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get the desercion
        restDesercionMockMvc.perform(get("/api/desercions/{id}", desercion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(desercion.getId().intValue()))
            .andExpect(jsonPath("$.fecha").value(DEFAULT_FECHA.toString()))
            .andExpect(jsonPath("$.edad").value(DEFAULT_EDAD))
            .andExpect(jsonPath("$.nroPromedioRetiros").value(DEFAULT_NRO_PROMEDIO_RETIROS))
            .andExpect(jsonPath("$.nroPromedioAportes").value(DEFAULT_NRO_PROMEDIO_APORTES))
            .andExpect(jsonPath("$.netoPromMovimientos").value(DEFAULT_NETO_PROM_MOVIMIENTOS))
            .andExpect(jsonPath("$.diasUltimoMov").value(DEFAULT_DIAS_ULTIMO_MOV))
            .andExpect(jsonPath("$.posibleSul").value(DEFAULT_POSIBLE_SUL.booleanValue()));
    }


    @Test
    @Transactional
    public void getDesercionsByIdFiltering() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        Long id = desercion.getId();

        defaultDesercionShouldBeFound("id.equals=" + id);
        defaultDesercionShouldNotBeFound("id.notEquals=" + id);

        defaultDesercionShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDesercionShouldNotBeFound("id.greaterThan=" + id);

        defaultDesercionShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDesercionShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllDesercionsByFechaIsEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where fecha equals to DEFAULT_FECHA
        defaultDesercionShouldBeFound("fecha.equals=" + DEFAULT_FECHA);

        // Get all the desercionList where fecha equals to UPDATED_FECHA
        defaultDesercionShouldNotBeFound("fecha.equals=" + UPDATED_FECHA);
    }

    @Test
    @Transactional
    public void getAllDesercionsByFechaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where fecha not equals to DEFAULT_FECHA
        defaultDesercionShouldNotBeFound("fecha.notEquals=" + DEFAULT_FECHA);

        // Get all the desercionList where fecha not equals to UPDATED_FECHA
        defaultDesercionShouldBeFound("fecha.notEquals=" + UPDATED_FECHA);
    }

    @Test
    @Transactional
    public void getAllDesercionsByFechaIsInShouldWork() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where fecha in DEFAULT_FECHA or UPDATED_FECHA
        defaultDesercionShouldBeFound("fecha.in=" + DEFAULT_FECHA + "," + UPDATED_FECHA);

        // Get all the desercionList where fecha equals to UPDATED_FECHA
        defaultDesercionShouldNotBeFound("fecha.in=" + UPDATED_FECHA);
    }

    @Test
    @Transactional
    public void getAllDesercionsByFechaIsNullOrNotNull() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where fecha is not null
        defaultDesercionShouldBeFound("fecha.specified=true");

        // Get all the desercionList where fecha is null
        defaultDesercionShouldNotBeFound("fecha.specified=false");
    }

    @Test
    @Transactional
    public void getAllDesercionsByFechaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where fecha is greater than or equal to DEFAULT_FECHA
        defaultDesercionShouldBeFound("fecha.greaterThanOrEqual=" + DEFAULT_FECHA);

        // Get all the desercionList where fecha is greater than or equal to UPDATED_FECHA
        defaultDesercionShouldNotBeFound("fecha.greaterThanOrEqual=" + UPDATED_FECHA);
    }

    @Test
    @Transactional
    public void getAllDesercionsByFechaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where fecha is less than or equal to DEFAULT_FECHA
        defaultDesercionShouldBeFound("fecha.lessThanOrEqual=" + DEFAULT_FECHA);

        // Get all the desercionList where fecha is less than or equal to SMALLER_FECHA
        defaultDesercionShouldNotBeFound("fecha.lessThanOrEqual=" + SMALLER_FECHA);
    }

    @Test
    @Transactional
    public void getAllDesercionsByFechaIsLessThanSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where fecha is less than DEFAULT_FECHA
        defaultDesercionShouldNotBeFound("fecha.lessThan=" + DEFAULT_FECHA);

        // Get all the desercionList where fecha is less than UPDATED_FECHA
        defaultDesercionShouldBeFound("fecha.lessThan=" + UPDATED_FECHA);
    }

    @Test
    @Transactional
    public void getAllDesercionsByFechaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where fecha is greater than DEFAULT_FECHA
        defaultDesercionShouldNotBeFound("fecha.greaterThan=" + DEFAULT_FECHA);

        // Get all the desercionList where fecha is greater than SMALLER_FECHA
        defaultDesercionShouldBeFound("fecha.greaterThan=" + SMALLER_FECHA);
    }


    @Test
    @Transactional
    public void getAllDesercionsByEdadIsEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where edad equals to DEFAULT_EDAD
        defaultDesercionShouldBeFound("edad.equals=" + DEFAULT_EDAD);

        // Get all the desercionList where edad equals to UPDATED_EDAD
        defaultDesercionShouldNotBeFound("edad.equals=" + UPDATED_EDAD);
    }

    @Test
    @Transactional
    public void getAllDesercionsByEdadIsNotEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where edad not equals to DEFAULT_EDAD
        defaultDesercionShouldNotBeFound("edad.notEquals=" + DEFAULT_EDAD);

        // Get all the desercionList where edad not equals to UPDATED_EDAD
        defaultDesercionShouldBeFound("edad.notEquals=" + UPDATED_EDAD);
    }

    @Test
    @Transactional
    public void getAllDesercionsByEdadIsInShouldWork() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where edad in DEFAULT_EDAD or UPDATED_EDAD
        defaultDesercionShouldBeFound("edad.in=" + DEFAULT_EDAD + "," + UPDATED_EDAD);

        // Get all the desercionList where edad equals to UPDATED_EDAD
        defaultDesercionShouldNotBeFound("edad.in=" + UPDATED_EDAD);
    }

    @Test
    @Transactional
    public void getAllDesercionsByEdadIsNullOrNotNull() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where edad is not null
        defaultDesercionShouldBeFound("edad.specified=true");

        // Get all the desercionList where edad is null
        defaultDesercionShouldNotBeFound("edad.specified=false");
    }
                @Test
    @Transactional
    public void getAllDesercionsByEdadContainsSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where edad contains DEFAULT_EDAD
        defaultDesercionShouldBeFound("edad.contains=" + DEFAULT_EDAD);

        // Get all the desercionList where edad contains UPDATED_EDAD
        defaultDesercionShouldNotBeFound("edad.contains=" + UPDATED_EDAD);
    }

    @Test
    @Transactional
    public void getAllDesercionsByEdadNotContainsSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where edad does not contain DEFAULT_EDAD
        defaultDesercionShouldNotBeFound("edad.doesNotContain=" + DEFAULT_EDAD);

        // Get all the desercionList where edad does not contain UPDATED_EDAD
        defaultDesercionShouldBeFound("edad.doesNotContain=" + UPDATED_EDAD);
    }


    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioRetirosIsEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioRetiros equals to DEFAULT_NRO_PROMEDIO_RETIROS
        defaultDesercionShouldBeFound("nroPromedioRetiros.equals=" + DEFAULT_NRO_PROMEDIO_RETIROS);

        // Get all the desercionList where nroPromedioRetiros equals to UPDATED_NRO_PROMEDIO_RETIROS
        defaultDesercionShouldNotBeFound("nroPromedioRetiros.equals=" + UPDATED_NRO_PROMEDIO_RETIROS);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioRetirosIsNotEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioRetiros not equals to DEFAULT_NRO_PROMEDIO_RETIROS
        defaultDesercionShouldNotBeFound("nroPromedioRetiros.notEquals=" + DEFAULT_NRO_PROMEDIO_RETIROS);

        // Get all the desercionList where nroPromedioRetiros not equals to UPDATED_NRO_PROMEDIO_RETIROS
        defaultDesercionShouldBeFound("nroPromedioRetiros.notEquals=" + UPDATED_NRO_PROMEDIO_RETIROS);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioRetirosIsInShouldWork() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioRetiros in DEFAULT_NRO_PROMEDIO_RETIROS or UPDATED_NRO_PROMEDIO_RETIROS
        defaultDesercionShouldBeFound("nroPromedioRetiros.in=" + DEFAULT_NRO_PROMEDIO_RETIROS + "," + UPDATED_NRO_PROMEDIO_RETIROS);

        // Get all the desercionList where nroPromedioRetiros equals to UPDATED_NRO_PROMEDIO_RETIROS
        defaultDesercionShouldNotBeFound("nroPromedioRetiros.in=" + UPDATED_NRO_PROMEDIO_RETIROS);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioRetirosIsNullOrNotNull() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioRetiros is not null
        defaultDesercionShouldBeFound("nroPromedioRetiros.specified=true");

        // Get all the desercionList where nroPromedioRetiros is null
        defaultDesercionShouldNotBeFound("nroPromedioRetiros.specified=false");
    }

    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioRetirosIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioRetiros is greater than or equal to DEFAULT_NRO_PROMEDIO_RETIROS
        defaultDesercionShouldBeFound("nroPromedioRetiros.greaterThanOrEqual=" + DEFAULT_NRO_PROMEDIO_RETIROS);

        // Get all the desercionList where nroPromedioRetiros is greater than or equal to UPDATED_NRO_PROMEDIO_RETIROS
        defaultDesercionShouldNotBeFound("nroPromedioRetiros.greaterThanOrEqual=" + UPDATED_NRO_PROMEDIO_RETIROS);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioRetirosIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioRetiros is less than or equal to DEFAULT_NRO_PROMEDIO_RETIROS
        defaultDesercionShouldBeFound("nroPromedioRetiros.lessThanOrEqual=" + DEFAULT_NRO_PROMEDIO_RETIROS);

        // Get all the desercionList where nroPromedioRetiros is less than or equal to SMALLER_NRO_PROMEDIO_RETIROS
        defaultDesercionShouldNotBeFound("nroPromedioRetiros.lessThanOrEqual=" + SMALLER_NRO_PROMEDIO_RETIROS);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioRetirosIsLessThanSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioRetiros is less than DEFAULT_NRO_PROMEDIO_RETIROS
        defaultDesercionShouldNotBeFound("nroPromedioRetiros.lessThan=" + DEFAULT_NRO_PROMEDIO_RETIROS);

        // Get all the desercionList where nroPromedioRetiros is less than UPDATED_NRO_PROMEDIO_RETIROS
        defaultDesercionShouldBeFound("nroPromedioRetiros.lessThan=" + UPDATED_NRO_PROMEDIO_RETIROS);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioRetirosIsGreaterThanSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioRetiros is greater than DEFAULT_NRO_PROMEDIO_RETIROS
        defaultDesercionShouldNotBeFound("nroPromedioRetiros.greaterThan=" + DEFAULT_NRO_PROMEDIO_RETIROS);

        // Get all the desercionList where nroPromedioRetiros is greater than SMALLER_NRO_PROMEDIO_RETIROS
        defaultDesercionShouldBeFound("nroPromedioRetiros.greaterThan=" + SMALLER_NRO_PROMEDIO_RETIROS);
    }


    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioAportesIsEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioAportes equals to DEFAULT_NRO_PROMEDIO_APORTES
        defaultDesercionShouldBeFound("nroPromedioAportes.equals=" + DEFAULT_NRO_PROMEDIO_APORTES);

        // Get all the desercionList where nroPromedioAportes equals to UPDATED_NRO_PROMEDIO_APORTES
        defaultDesercionShouldNotBeFound("nroPromedioAportes.equals=" + UPDATED_NRO_PROMEDIO_APORTES);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioAportesIsNotEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioAportes not equals to DEFAULT_NRO_PROMEDIO_APORTES
        defaultDesercionShouldNotBeFound("nroPromedioAportes.notEquals=" + DEFAULT_NRO_PROMEDIO_APORTES);

        // Get all the desercionList where nroPromedioAportes not equals to UPDATED_NRO_PROMEDIO_APORTES
        defaultDesercionShouldBeFound("nroPromedioAportes.notEquals=" + UPDATED_NRO_PROMEDIO_APORTES);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioAportesIsInShouldWork() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioAportes in DEFAULT_NRO_PROMEDIO_APORTES or UPDATED_NRO_PROMEDIO_APORTES
        defaultDesercionShouldBeFound("nroPromedioAportes.in=" + DEFAULT_NRO_PROMEDIO_APORTES + "," + UPDATED_NRO_PROMEDIO_APORTES);

        // Get all the desercionList where nroPromedioAportes equals to UPDATED_NRO_PROMEDIO_APORTES
        defaultDesercionShouldNotBeFound("nroPromedioAportes.in=" + UPDATED_NRO_PROMEDIO_APORTES);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioAportesIsNullOrNotNull() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioAportes is not null
        defaultDesercionShouldBeFound("nroPromedioAportes.specified=true");

        // Get all the desercionList where nroPromedioAportes is null
        defaultDesercionShouldNotBeFound("nroPromedioAportes.specified=false");
    }

    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioAportesIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioAportes is greater than or equal to DEFAULT_NRO_PROMEDIO_APORTES
        defaultDesercionShouldBeFound("nroPromedioAportes.greaterThanOrEqual=" + DEFAULT_NRO_PROMEDIO_APORTES);

        // Get all the desercionList where nroPromedioAportes is greater than or equal to UPDATED_NRO_PROMEDIO_APORTES
        defaultDesercionShouldNotBeFound("nroPromedioAportes.greaterThanOrEqual=" + UPDATED_NRO_PROMEDIO_APORTES);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioAportesIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioAportes is less than or equal to DEFAULT_NRO_PROMEDIO_APORTES
        defaultDesercionShouldBeFound("nroPromedioAportes.lessThanOrEqual=" + DEFAULT_NRO_PROMEDIO_APORTES);

        // Get all the desercionList where nroPromedioAportes is less than or equal to SMALLER_NRO_PROMEDIO_APORTES
        defaultDesercionShouldNotBeFound("nroPromedioAportes.lessThanOrEqual=" + SMALLER_NRO_PROMEDIO_APORTES);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioAportesIsLessThanSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioAportes is less than DEFAULT_NRO_PROMEDIO_APORTES
        defaultDesercionShouldNotBeFound("nroPromedioAportes.lessThan=" + DEFAULT_NRO_PROMEDIO_APORTES);

        // Get all the desercionList where nroPromedioAportes is less than UPDATED_NRO_PROMEDIO_APORTES
        defaultDesercionShouldBeFound("nroPromedioAportes.lessThan=" + UPDATED_NRO_PROMEDIO_APORTES);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNroPromedioAportesIsGreaterThanSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where nroPromedioAportes is greater than DEFAULT_NRO_PROMEDIO_APORTES
        defaultDesercionShouldNotBeFound("nroPromedioAportes.greaterThan=" + DEFAULT_NRO_PROMEDIO_APORTES);

        // Get all the desercionList where nroPromedioAportes is greater than SMALLER_NRO_PROMEDIO_APORTES
        defaultDesercionShouldBeFound("nroPromedioAportes.greaterThan=" + SMALLER_NRO_PROMEDIO_APORTES);
    }


    @Test
    @Transactional
    public void getAllDesercionsByNetoPromMovimientosIsEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where netoPromMovimientos equals to DEFAULT_NETO_PROM_MOVIMIENTOS
        defaultDesercionShouldBeFound("netoPromMovimientos.equals=" + DEFAULT_NETO_PROM_MOVIMIENTOS);

        // Get all the desercionList where netoPromMovimientos equals to UPDATED_NETO_PROM_MOVIMIENTOS
        defaultDesercionShouldNotBeFound("netoPromMovimientos.equals=" + UPDATED_NETO_PROM_MOVIMIENTOS);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNetoPromMovimientosIsNotEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where netoPromMovimientos not equals to DEFAULT_NETO_PROM_MOVIMIENTOS
        defaultDesercionShouldNotBeFound("netoPromMovimientos.notEquals=" + DEFAULT_NETO_PROM_MOVIMIENTOS);

        // Get all the desercionList where netoPromMovimientos not equals to UPDATED_NETO_PROM_MOVIMIENTOS
        defaultDesercionShouldBeFound("netoPromMovimientos.notEquals=" + UPDATED_NETO_PROM_MOVIMIENTOS);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNetoPromMovimientosIsInShouldWork() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where netoPromMovimientos in DEFAULT_NETO_PROM_MOVIMIENTOS or UPDATED_NETO_PROM_MOVIMIENTOS
        defaultDesercionShouldBeFound("netoPromMovimientos.in=" + DEFAULT_NETO_PROM_MOVIMIENTOS + "," + UPDATED_NETO_PROM_MOVIMIENTOS);

        // Get all the desercionList where netoPromMovimientos equals to UPDATED_NETO_PROM_MOVIMIENTOS
        defaultDesercionShouldNotBeFound("netoPromMovimientos.in=" + UPDATED_NETO_PROM_MOVIMIENTOS);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNetoPromMovimientosIsNullOrNotNull() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where netoPromMovimientos is not null
        defaultDesercionShouldBeFound("netoPromMovimientos.specified=true");

        // Get all the desercionList where netoPromMovimientos is null
        defaultDesercionShouldNotBeFound("netoPromMovimientos.specified=false");
    }

    @Test
    @Transactional
    public void getAllDesercionsByNetoPromMovimientosIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where netoPromMovimientos is greater than or equal to DEFAULT_NETO_PROM_MOVIMIENTOS
        defaultDesercionShouldBeFound("netoPromMovimientos.greaterThanOrEqual=" + DEFAULT_NETO_PROM_MOVIMIENTOS);

        // Get all the desercionList where netoPromMovimientos is greater than or equal to UPDATED_NETO_PROM_MOVIMIENTOS
        defaultDesercionShouldNotBeFound("netoPromMovimientos.greaterThanOrEqual=" + UPDATED_NETO_PROM_MOVIMIENTOS);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNetoPromMovimientosIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where netoPromMovimientos is less than or equal to DEFAULT_NETO_PROM_MOVIMIENTOS
        defaultDesercionShouldBeFound("netoPromMovimientos.lessThanOrEqual=" + DEFAULT_NETO_PROM_MOVIMIENTOS);

        // Get all the desercionList where netoPromMovimientos is less than or equal to SMALLER_NETO_PROM_MOVIMIENTOS
        defaultDesercionShouldNotBeFound("netoPromMovimientos.lessThanOrEqual=" + SMALLER_NETO_PROM_MOVIMIENTOS);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNetoPromMovimientosIsLessThanSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where netoPromMovimientos is less than DEFAULT_NETO_PROM_MOVIMIENTOS
        defaultDesercionShouldNotBeFound("netoPromMovimientos.lessThan=" + DEFAULT_NETO_PROM_MOVIMIENTOS);

        // Get all the desercionList where netoPromMovimientos is less than UPDATED_NETO_PROM_MOVIMIENTOS
        defaultDesercionShouldBeFound("netoPromMovimientos.lessThan=" + UPDATED_NETO_PROM_MOVIMIENTOS);
    }

    @Test
    @Transactional
    public void getAllDesercionsByNetoPromMovimientosIsGreaterThanSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where netoPromMovimientos is greater than DEFAULT_NETO_PROM_MOVIMIENTOS
        defaultDesercionShouldNotBeFound("netoPromMovimientos.greaterThan=" + DEFAULT_NETO_PROM_MOVIMIENTOS);

        // Get all the desercionList where netoPromMovimientos is greater than SMALLER_NETO_PROM_MOVIMIENTOS
        defaultDesercionShouldBeFound("netoPromMovimientos.greaterThan=" + SMALLER_NETO_PROM_MOVIMIENTOS);
    }


    @Test
    @Transactional
    public void getAllDesercionsByDiasUltimoMovIsEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where diasUltimoMov equals to DEFAULT_DIAS_ULTIMO_MOV
        defaultDesercionShouldBeFound("diasUltimoMov.equals=" + DEFAULT_DIAS_ULTIMO_MOV);

        // Get all the desercionList where diasUltimoMov equals to UPDATED_DIAS_ULTIMO_MOV
        defaultDesercionShouldNotBeFound("diasUltimoMov.equals=" + UPDATED_DIAS_ULTIMO_MOV);
    }

    @Test
    @Transactional
    public void getAllDesercionsByDiasUltimoMovIsNotEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where diasUltimoMov not equals to DEFAULT_DIAS_ULTIMO_MOV
        defaultDesercionShouldNotBeFound("diasUltimoMov.notEquals=" + DEFAULT_DIAS_ULTIMO_MOV);

        // Get all the desercionList where diasUltimoMov not equals to UPDATED_DIAS_ULTIMO_MOV
        defaultDesercionShouldBeFound("diasUltimoMov.notEquals=" + UPDATED_DIAS_ULTIMO_MOV);
    }

    @Test
    @Transactional
    public void getAllDesercionsByDiasUltimoMovIsInShouldWork() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where diasUltimoMov in DEFAULT_DIAS_ULTIMO_MOV or UPDATED_DIAS_ULTIMO_MOV
        defaultDesercionShouldBeFound("diasUltimoMov.in=" + DEFAULT_DIAS_ULTIMO_MOV + "," + UPDATED_DIAS_ULTIMO_MOV);

        // Get all the desercionList where diasUltimoMov equals to UPDATED_DIAS_ULTIMO_MOV
        defaultDesercionShouldNotBeFound("diasUltimoMov.in=" + UPDATED_DIAS_ULTIMO_MOV);
    }

    @Test
    @Transactional
    public void getAllDesercionsByDiasUltimoMovIsNullOrNotNull() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where diasUltimoMov is not null
        defaultDesercionShouldBeFound("diasUltimoMov.specified=true");

        // Get all the desercionList where diasUltimoMov is null
        defaultDesercionShouldNotBeFound("diasUltimoMov.specified=false");
    }

    @Test
    @Transactional
    public void getAllDesercionsByDiasUltimoMovIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where diasUltimoMov is greater than or equal to DEFAULT_DIAS_ULTIMO_MOV
        defaultDesercionShouldBeFound("diasUltimoMov.greaterThanOrEqual=" + DEFAULT_DIAS_ULTIMO_MOV);

        // Get all the desercionList where diasUltimoMov is greater than or equal to UPDATED_DIAS_ULTIMO_MOV
        defaultDesercionShouldNotBeFound("diasUltimoMov.greaterThanOrEqual=" + UPDATED_DIAS_ULTIMO_MOV);
    }

    @Test
    @Transactional
    public void getAllDesercionsByDiasUltimoMovIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where diasUltimoMov is less than or equal to DEFAULT_DIAS_ULTIMO_MOV
        defaultDesercionShouldBeFound("diasUltimoMov.lessThanOrEqual=" + DEFAULT_DIAS_ULTIMO_MOV);

        // Get all the desercionList where diasUltimoMov is less than or equal to SMALLER_DIAS_ULTIMO_MOV
        defaultDesercionShouldNotBeFound("diasUltimoMov.lessThanOrEqual=" + SMALLER_DIAS_ULTIMO_MOV);
    }

    @Test
    @Transactional
    public void getAllDesercionsByDiasUltimoMovIsLessThanSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where diasUltimoMov is less than DEFAULT_DIAS_ULTIMO_MOV
        defaultDesercionShouldNotBeFound("diasUltimoMov.lessThan=" + DEFAULT_DIAS_ULTIMO_MOV);

        // Get all the desercionList where diasUltimoMov is less than UPDATED_DIAS_ULTIMO_MOV
        defaultDesercionShouldBeFound("diasUltimoMov.lessThan=" + UPDATED_DIAS_ULTIMO_MOV);
    }

    @Test
    @Transactional
    public void getAllDesercionsByDiasUltimoMovIsGreaterThanSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where diasUltimoMov is greater than DEFAULT_DIAS_ULTIMO_MOV
        defaultDesercionShouldNotBeFound("diasUltimoMov.greaterThan=" + DEFAULT_DIAS_ULTIMO_MOV);

        // Get all the desercionList where diasUltimoMov is greater than SMALLER_DIAS_ULTIMO_MOV
        defaultDesercionShouldBeFound("diasUltimoMov.greaterThan=" + SMALLER_DIAS_ULTIMO_MOV);
    }


    @Test
    @Transactional
    public void getAllDesercionsByPosibleSulIsEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where posibleSul equals to DEFAULT_POSIBLE_SUL
        defaultDesercionShouldBeFound("posibleSul.equals=" + DEFAULT_POSIBLE_SUL);

        // Get all the desercionList where posibleSul equals to UPDATED_POSIBLE_SUL
        defaultDesercionShouldNotBeFound("posibleSul.equals=" + UPDATED_POSIBLE_SUL);
    }

    @Test
    @Transactional
    public void getAllDesercionsByPosibleSulIsNotEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where posibleSul not equals to DEFAULT_POSIBLE_SUL
        defaultDesercionShouldNotBeFound("posibleSul.notEquals=" + DEFAULT_POSIBLE_SUL);

        // Get all the desercionList where posibleSul not equals to UPDATED_POSIBLE_SUL
        defaultDesercionShouldBeFound("posibleSul.notEquals=" + UPDATED_POSIBLE_SUL);
    }

    @Test
    @Transactional
    public void getAllDesercionsByPosibleSulIsInShouldWork() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where posibleSul in DEFAULT_POSIBLE_SUL or UPDATED_POSIBLE_SUL
        defaultDesercionShouldBeFound("posibleSul.in=" + DEFAULT_POSIBLE_SUL + "," + UPDATED_POSIBLE_SUL);

        // Get all the desercionList where posibleSul equals to UPDATED_POSIBLE_SUL
        defaultDesercionShouldNotBeFound("posibleSul.in=" + UPDATED_POSIBLE_SUL);
    }

    @Test
    @Transactional
    public void getAllDesercionsByPosibleSulIsNullOrNotNull() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);

        // Get all the desercionList where posibleSul is not null
        defaultDesercionShouldBeFound("posibleSul.specified=true");

        // Get all the desercionList where posibleSul is null
        defaultDesercionShouldNotBeFound("posibleSul.specified=false");
    }

    @Test
    @Transactional
    public void getAllDesercionsByIdClienteIsEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);
        Cliente idCliente = ClienteResourceIT.createEntity(em);
        em.persist(idCliente);
        em.flush();
        desercion.setIdCliente(idCliente);
        desercionRepository.saveAndFlush(desercion);
        Long idClienteId = idCliente.getId();

        // Get all the desercionList where idCliente equals to idClienteId
        defaultDesercionShouldBeFound("idClienteId.equals=" + idClienteId);

        // Get all the desercionList where idCliente equals to idClienteId + 1
        defaultDesercionShouldNotBeFound("idClienteId.equals=" + (idClienteId + 1));
    }


    @Test
    @Transactional
    public void getAllDesercionsByIdFondoIsEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);
        Fondo idFondo = FondoResourceIT.createEntity(em);
        em.persist(idFondo);
        em.flush();
        desercion.setIdFondo(idFondo);
        desercionRepository.saveAndFlush(desercion);
        Long idFondoId = idFondo.getId();

        // Get all the desercionList where idFondo equals to idFondoId
        defaultDesercionShouldBeFound("idFondoId.equals=" + idFondoId);

        // Get all the desercionList where idFondo equals to idFondoId + 1
        defaultDesercionShouldNotBeFound("idFondoId.equals=" + (idFondoId + 1));
    }


    @Test
    @Transactional
    public void getAllDesercionsByIdFideicomisoIsEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);
        Fideicomiso idFideicomiso = FideicomisoResourceIT.createEntity(em);
        em.persist(idFideicomiso);
        em.flush();
        desercion.setIdFideicomiso(idFideicomiso);
        desercionRepository.saveAndFlush(desercion);
        Long idFideicomisoId = idFideicomiso.getId();

        // Get all the desercionList where idFideicomiso equals to idFideicomisoId
        defaultDesercionShouldBeFound("idFideicomisoId.equals=" + idFideicomisoId);

        // Get all the desercionList where idFideicomiso equals to idFideicomisoId + 1
        defaultDesercionShouldNotBeFound("idFideicomisoId.equals=" + (idFideicomisoId + 1));
    }


    @Test
    @Transactional
    public void getAllDesercionsByIdComercialIsEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);
        Comercial idComercial = ComercialResourceIT.createEntity(em);
        em.persist(idComercial);
        em.flush();
        desercion.setIdComercial(idComercial);
        desercionRepository.saveAndFlush(desercion);
        Long idComercialId = idComercial.getId();

        // Get all the desercionList where idComercial equals to idComercialId
        defaultDesercionShouldBeFound("idComercialId.equals=" + idComercialId);

        // Get all the desercionList where idComercial equals to idComercialId + 1
        defaultDesercionShouldNotBeFound("idComercialId.equals=" + (idComercialId + 1));
    }


    @Test
    @Transactional
    public void getAllDesercionsByIdPerfilAhorroIsEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);
        PerfilAhorro idPerfilAhorro = PerfilAhorroResourceIT.createEntity(em);
        em.persist(idPerfilAhorro);
        em.flush();
        desercion.setIdPerfilAhorro(idPerfilAhorro);
        desercionRepository.saveAndFlush(desercion);
        Long idPerfilAhorroId = idPerfilAhorro.getId();

        // Get all the desercionList where idPerfilAhorro equals to idPerfilAhorroId
        defaultDesercionShouldBeFound("idPerfilAhorroId.equals=" + idPerfilAhorroId);

        // Get all the desercionList where idPerfilAhorro equals to idPerfilAhorroId + 1
        defaultDesercionShouldNotBeFound("idPerfilAhorroId.equals=" + (idPerfilAhorroId + 1));
    }


    @Test
    @Transactional
    public void getAllDesercionsByIdPerfilComercialIsEqualToSomething() throws Exception {
        // Initialize the database
        desercionRepository.saveAndFlush(desercion);
        PerfilComercial idPerfilComercial = PerfilComercialResourceIT.createEntity(em);
        em.persist(idPerfilComercial);
        em.flush();
        desercion.setIdPerfilComercial(idPerfilComercial);
        desercionRepository.saveAndFlush(desercion);
        Long idPerfilComercialId = idPerfilComercial.getId();

        // Get all the desercionList where idPerfilComercial equals to idPerfilComercialId
        defaultDesercionShouldBeFound("idPerfilComercialId.equals=" + idPerfilComercialId);

        // Get all the desercionList where idPerfilComercial equals to idPerfilComercialId + 1
        defaultDesercionShouldNotBeFound("idPerfilComercialId.equals=" + (idPerfilComercialId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDesercionShouldBeFound(String filter) throws Exception {
        restDesercionMockMvc.perform(get("/api/desercions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(desercion.getId().intValue())))
            .andExpect(jsonPath("$.[*].fecha").value(hasItem(DEFAULT_FECHA.toString())))
            .andExpect(jsonPath("$.[*].edad").value(hasItem(DEFAULT_EDAD)))
            .andExpect(jsonPath("$.[*].nroPromedioRetiros").value(hasItem(DEFAULT_NRO_PROMEDIO_RETIROS)))
            .andExpect(jsonPath("$.[*].nroPromedioAportes").value(hasItem(DEFAULT_NRO_PROMEDIO_APORTES)))
            .andExpect(jsonPath("$.[*].netoPromMovimientos").value(hasItem(DEFAULT_NETO_PROM_MOVIMIENTOS)))
            .andExpect(jsonPath("$.[*].diasUltimoMov").value(hasItem(DEFAULT_DIAS_ULTIMO_MOV)))
            .andExpect(jsonPath("$.[*].posibleSul").value(hasItem(DEFAULT_POSIBLE_SUL.booleanValue())));

        // Check, that the count call also returns 1
        restDesercionMockMvc.perform(get("/api/desercions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDesercionShouldNotBeFound(String filter) throws Exception {
        restDesercionMockMvc.perform(get("/api/desercions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDesercionMockMvc.perform(get("/api/desercions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingDesercion() throws Exception {
        // Get the desercion
        restDesercionMockMvc.perform(get("/api/desercions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDesercion() throws Exception {
        // Initialize the database
        desercionService.save(desercion);

        int databaseSizeBeforeUpdate = desercionRepository.findAll().size();

        // Update the desercion
        Desercion updatedDesercion = desercionRepository.findById(desercion.getId()).get();
        // Disconnect from session so that the updates on updatedDesercion are not directly saved in db
        em.detach(updatedDesercion);
        updatedDesercion
            .fecha(UPDATED_FECHA)
            .edad(UPDATED_EDAD)
            .nroPromedioRetiros(UPDATED_NRO_PROMEDIO_RETIROS)
            .nroPromedioAportes(UPDATED_NRO_PROMEDIO_APORTES)
            .netoPromMovimientos(UPDATED_NETO_PROM_MOVIMIENTOS)
            .diasUltimoMov(UPDATED_DIAS_ULTIMO_MOV)
            .posibleSul(UPDATED_POSIBLE_SUL);

        restDesercionMockMvc.perform(put("/api/desercions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedDesercion)))
            .andExpect(status().isOk());

        // Validate the Desercion in the database
        List<Desercion> desercionList = desercionRepository.findAll();
        assertThat(desercionList).hasSize(databaseSizeBeforeUpdate);
        Desercion testDesercion = desercionList.get(desercionList.size() - 1);
        assertThat(testDesercion.getFecha()).isEqualTo(UPDATED_FECHA);
        assertThat(testDesercion.getEdad()).isEqualTo(UPDATED_EDAD);
        assertThat(testDesercion.getNroPromedioRetiros()).isEqualTo(UPDATED_NRO_PROMEDIO_RETIROS);
        assertThat(testDesercion.getNroPromedioAportes()).isEqualTo(UPDATED_NRO_PROMEDIO_APORTES);
        assertThat(testDesercion.getNetoPromMovimientos()).isEqualTo(UPDATED_NETO_PROM_MOVIMIENTOS);
        assertThat(testDesercion.getDiasUltimoMov()).isEqualTo(UPDATED_DIAS_ULTIMO_MOV);
        assertThat(testDesercion.isPosibleSul()).isEqualTo(UPDATED_POSIBLE_SUL);
    }

    @Test
    @Transactional
    public void updateNonExistingDesercion() throws Exception {
        int databaseSizeBeforeUpdate = desercionRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDesercionMockMvc.perform(put("/api/desercions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(desercion)))
            .andExpect(status().isBadRequest());

        // Validate the Desercion in the database
        List<Desercion> desercionList = desercionRepository.findAll();
        assertThat(desercionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDesercion() throws Exception {
        // Initialize the database
        desercionService.save(desercion);

        int databaseSizeBeforeDelete = desercionRepository.findAll().size();

        // Delete the desercion
        restDesercionMockMvc.perform(delete("/api/desercions/{id}", desercion.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Desercion> desercionList = desercionRepository.findAll();
        assertThat(desercionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
