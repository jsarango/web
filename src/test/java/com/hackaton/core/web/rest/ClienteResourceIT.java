package com.hackaton.core.web.rest;

import com.hackaton.core.FrontApp;
import com.hackaton.core.domain.Cliente;
import com.hackaton.core.repository.ClienteRepository;
import com.hackaton.core.service.ClienteService;
import com.hackaton.core.service.dto.ClienteCriteria;
import com.hackaton.core.service.ClienteQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.hackaton.core.domain.enumeration.TipoCliente;
import com.hackaton.core.domain.enumeration.TipoIdentificacion;
import com.hackaton.core.domain.enumeration.Sexo;
import com.hackaton.core.domain.enumeration.EstadoCivil;
/**
 * Integration tests for the {@link ClienteResource} REST controller.
 */
@SpringBootTest(classes = FrontApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ClienteResourceIT {

    private static final TipoCliente DEFAULT_TIPO = TipoCliente.N;
    private static final TipoCliente UPDATED_TIPO = TipoCliente.J;

    private static final TipoIdentificacion DEFAULT_TIPO_IDENTIFICACION = TipoIdentificacion.C;
    private static final TipoIdentificacion UPDATED_TIPO_IDENTIFICACION = TipoIdentificacion.N;

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_FECHA_NACIMIENTO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA_NACIMIENTO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_FECHA_NACIMIENTO = LocalDate.ofEpochDay(-1L);

    private static final Sexo DEFAULT_SEXO = Sexo.M;
    private static final Sexo UPDATED_SEXO = Sexo.F;

    private static final EstadoCivil DEFAULT_ESTADO_CIVIL = EstadoCivil.S;
    private static final EstadoCivil UPDATED_ESTADO_CIVIL = EstadoCivil.C;

    private static final LocalDate DEFAULT_FECHA_INGRESO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA_INGRESO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_FECHA_INGRESO = LocalDate.ofEpochDay(-1L);

    private static final Boolean DEFAULT_RESIDE_COLOMBIA = false;
    private static final Boolean UPDATED_RESIDE_COLOMBIA = true;

    private static final Boolean DEFAULT_DECLARANTE = false;
    private static final Boolean UPDATED_DECLARANTE = true;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteQueryService clienteQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restClienteMockMvc;

    private Cliente cliente;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cliente createEntity(EntityManager em) {
        Cliente cliente = new Cliente()
            .tipo(DEFAULT_TIPO)
            .tipoIdentificacion(DEFAULT_TIPO_IDENTIFICACION)
            .nombre(DEFAULT_NOMBRE)
            .fechaNacimiento(DEFAULT_FECHA_NACIMIENTO)
            .sexo(DEFAULT_SEXO)
            .estadoCivil(DEFAULT_ESTADO_CIVIL)
            .fechaIngreso(DEFAULT_FECHA_INGRESO)
            .resideColombia(DEFAULT_RESIDE_COLOMBIA)
            .declarante(DEFAULT_DECLARANTE);
        return cliente;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cliente createUpdatedEntity(EntityManager em) {
        Cliente cliente = new Cliente()
            .tipo(UPDATED_TIPO)
            .tipoIdentificacion(UPDATED_TIPO_IDENTIFICACION)
            .nombre(UPDATED_NOMBRE)
            .fechaNacimiento(UPDATED_FECHA_NACIMIENTO)
            .sexo(UPDATED_SEXO)
            .estadoCivil(UPDATED_ESTADO_CIVIL)
            .fechaIngreso(UPDATED_FECHA_INGRESO)
            .resideColombia(UPDATED_RESIDE_COLOMBIA)
            .declarante(UPDATED_DECLARANTE);
        return cliente;
    }

    @BeforeEach
    public void initTest() {
        cliente = createEntity(em);
    }

    @Test
    @Transactional
    public void createCliente() throws Exception {
        int databaseSizeBeforeCreate = clienteRepository.findAll().size();
        // Create the Cliente
        restClienteMockMvc.perform(post("/api/clientes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isCreated());

        // Validate the Cliente in the database
        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeCreate + 1);
        Cliente testCliente = clienteList.get(clienteList.size() - 1);
        assertThat(testCliente.getTipo()).isEqualTo(DEFAULT_TIPO);
        assertThat(testCliente.getTipoIdentificacion()).isEqualTo(DEFAULT_TIPO_IDENTIFICACION);
        assertThat(testCliente.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testCliente.getFechaNacimiento()).isEqualTo(DEFAULT_FECHA_NACIMIENTO);
        assertThat(testCliente.getSexo()).isEqualTo(DEFAULT_SEXO);
        assertThat(testCliente.getEstadoCivil()).isEqualTo(DEFAULT_ESTADO_CIVIL);
        assertThat(testCliente.getFechaIngreso()).isEqualTo(DEFAULT_FECHA_INGRESO);
        assertThat(testCliente.isResideColombia()).isEqualTo(DEFAULT_RESIDE_COLOMBIA);
        assertThat(testCliente.isDeclarante()).isEqualTo(DEFAULT_DECLARANTE);
    }

    @Test
    @Transactional
    public void createClienteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clienteRepository.findAll().size();

        // Create the Cliente with an existing ID
        cliente.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClienteMockMvc.perform(post("/api/clientes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isBadRequest());

        // Validate the Cliente in the database
        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllClientes() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList
        restClienteMockMvc.perform(get("/api/clientes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cliente.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipo").value(hasItem(DEFAULT_TIPO.toString())))
            .andExpect(jsonPath("$.[*].tipoIdentificacion").value(hasItem(DEFAULT_TIPO_IDENTIFICACION.toString())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE)))
            .andExpect(jsonPath("$.[*].fechaNacimiento").value(hasItem(DEFAULT_FECHA_NACIMIENTO.toString())))
            .andExpect(jsonPath("$.[*].sexo").value(hasItem(DEFAULT_SEXO.toString())))
            .andExpect(jsonPath("$.[*].estadoCivil").value(hasItem(DEFAULT_ESTADO_CIVIL.toString())))
            .andExpect(jsonPath("$.[*].fechaIngreso").value(hasItem(DEFAULT_FECHA_INGRESO.toString())))
            .andExpect(jsonPath("$.[*].resideColombia").value(hasItem(DEFAULT_RESIDE_COLOMBIA.booleanValue())))
            .andExpect(jsonPath("$.[*].declarante").value(hasItem(DEFAULT_DECLARANTE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getCliente() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get the cliente
        restClienteMockMvc.perform(get("/api/clientes/{id}", cliente.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(cliente.getId().intValue()))
            .andExpect(jsonPath("$.tipo").value(DEFAULT_TIPO.toString()))
            .andExpect(jsonPath("$.tipoIdentificacion").value(DEFAULT_TIPO_IDENTIFICACION.toString()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE))
            .andExpect(jsonPath("$.fechaNacimiento").value(DEFAULT_FECHA_NACIMIENTO.toString()))
            .andExpect(jsonPath("$.sexo").value(DEFAULT_SEXO.toString()))
            .andExpect(jsonPath("$.estadoCivil").value(DEFAULT_ESTADO_CIVIL.toString()))
            .andExpect(jsonPath("$.fechaIngreso").value(DEFAULT_FECHA_INGRESO.toString()))
            .andExpect(jsonPath("$.resideColombia").value(DEFAULT_RESIDE_COLOMBIA.booleanValue()))
            .andExpect(jsonPath("$.declarante").value(DEFAULT_DECLARANTE.booleanValue()));
    }


    @Test
    @Transactional
    public void getClientesByIdFiltering() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        Long id = cliente.getId();

        defaultClienteShouldBeFound("id.equals=" + id);
        defaultClienteShouldNotBeFound("id.notEquals=" + id);

        defaultClienteShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultClienteShouldNotBeFound("id.greaterThan=" + id);

        defaultClienteShouldBeFound("id.lessThanOrEqual=" + id);
        defaultClienteShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllClientesByTipoIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where tipo equals to DEFAULT_TIPO
        defaultClienteShouldBeFound("tipo.equals=" + DEFAULT_TIPO);

        // Get all the clienteList where tipo equals to UPDATED_TIPO
        defaultClienteShouldNotBeFound("tipo.equals=" + UPDATED_TIPO);
    }

    @Test
    @Transactional
    public void getAllClientesByTipoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where tipo not equals to DEFAULT_TIPO
        defaultClienteShouldNotBeFound("tipo.notEquals=" + DEFAULT_TIPO);

        // Get all the clienteList where tipo not equals to UPDATED_TIPO
        defaultClienteShouldBeFound("tipo.notEquals=" + UPDATED_TIPO);
    }

    @Test
    @Transactional
    public void getAllClientesByTipoIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where tipo in DEFAULT_TIPO or UPDATED_TIPO
        defaultClienteShouldBeFound("tipo.in=" + DEFAULT_TIPO + "," + UPDATED_TIPO);

        // Get all the clienteList where tipo equals to UPDATED_TIPO
        defaultClienteShouldNotBeFound("tipo.in=" + UPDATED_TIPO);
    }

    @Test
    @Transactional
    public void getAllClientesByTipoIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where tipo is not null
        defaultClienteShouldBeFound("tipo.specified=true");

        // Get all the clienteList where tipo is null
        defaultClienteShouldNotBeFound("tipo.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByTipoIdentificacionIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where tipoIdentificacion equals to DEFAULT_TIPO_IDENTIFICACION
        defaultClienteShouldBeFound("tipoIdentificacion.equals=" + DEFAULT_TIPO_IDENTIFICACION);

        // Get all the clienteList where tipoIdentificacion equals to UPDATED_TIPO_IDENTIFICACION
        defaultClienteShouldNotBeFound("tipoIdentificacion.equals=" + UPDATED_TIPO_IDENTIFICACION);
    }

    @Test
    @Transactional
    public void getAllClientesByTipoIdentificacionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where tipoIdentificacion not equals to DEFAULT_TIPO_IDENTIFICACION
        defaultClienteShouldNotBeFound("tipoIdentificacion.notEquals=" + DEFAULT_TIPO_IDENTIFICACION);

        // Get all the clienteList where tipoIdentificacion not equals to UPDATED_TIPO_IDENTIFICACION
        defaultClienteShouldBeFound("tipoIdentificacion.notEquals=" + UPDATED_TIPO_IDENTIFICACION);
    }

    @Test
    @Transactional
    public void getAllClientesByTipoIdentificacionIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where tipoIdentificacion in DEFAULT_TIPO_IDENTIFICACION or UPDATED_TIPO_IDENTIFICACION
        defaultClienteShouldBeFound("tipoIdentificacion.in=" + DEFAULT_TIPO_IDENTIFICACION + "," + UPDATED_TIPO_IDENTIFICACION);

        // Get all the clienteList where tipoIdentificacion equals to UPDATED_TIPO_IDENTIFICACION
        defaultClienteShouldNotBeFound("tipoIdentificacion.in=" + UPDATED_TIPO_IDENTIFICACION);
    }

    @Test
    @Transactional
    public void getAllClientesByTipoIdentificacionIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where tipoIdentificacion is not null
        defaultClienteShouldBeFound("tipoIdentificacion.specified=true");

        // Get all the clienteList where tipoIdentificacion is null
        defaultClienteShouldNotBeFound("tipoIdentificacion.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByNombreIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where nombre equals to DEFAULT_NOMBRE
        defaultClienteShouldBeFound("nombre.equals=" + DEFAULT_NOMBRE);

        // Get all the clienteList where nombre equals to UPDATED_NOMBRE
        defaultClienteShouldNotBeFound("nombre.equals=" + UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void getAllClientesByNombreIsNotEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where nombre not equals to DEFAULT_NOMBRE
        defaultClienteShouldNotBeFound("nombre.notEquals=" + DEFAULT_NOMBRE);

        // Get all the clienteList where nombre not equals to UPDATED_NOMBRE
        defaultClienteShouldBeFound("nombre.notEquals=" + UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void getAllClientesByNombreIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where nombre in DEFAULT_NOMBRE or UPDATED_NOMBRE
        defaultClienteShouldBeFound("nombre.in=" + DEFAULT_NOMBRE + "," + UPDATED_NOMBRE);

        // Get all the clienteList where nombre equals to UPDATED_NOMBRE
        defaultClienteShouldNotBeFound("nombre.in=" + UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void getAllClientesByNombreIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where nombre is not null
        defaultClienteShouldBeFound("nombre.specified=true");

        // Get all the clienteList where nombre is null
        defaultClienteShouldNotBeFound("nombre.specified=false");
    }
                @Test
    @Transactional
    public void getAllClientesByNombreContainsSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where nombre contains DEFAULT_NOMBRE
        defaultClienteShouldBeFound("nombre.contains=" + DEFAULT_NOMBRE);

        // Get all the clienteList where nombre contains UPDATED_NOMBRE
        defaultClienteShouldNotBeFound("nombre.contains=" + UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void getAllClientesByNombreNotContainsSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where nombre does not contain DEFAULT_NOMBRE
        defaultClienteShouldNotBeFound("nombre.doesNotContain=" + DEFAULT_NOMBRE);

        // Get all the clienteList where nombre does not contain UPDATED_NOMBRE
        defaultClienteShouldBeFound("nombre.doesNotContain=" + UPDATED_NOMBRE);
    }


    @Test
    @Transactional
    public void getAllClientesByFechaNacimientoIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaNacimiento equals to DEFAULT_FECHA_NACIMIENTO
        defaultClienteShouldBeFound("fechaNacimiento.equals=" + DEFAULT_FECHA_NACIMIENTO);

        // Get all the clienteList where fechaNacimiento equals to UPDATED_FECHA_NACIMIENTO
        defaultClienteShouldNotBeFound("fechaNacimiento.equals=" + UPDATED_FECHA_NACIMIENTO);
    }

    @Test
    @Transactional
    public void getAllClientesByFechaNacimientoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaNacimiento not equals to DEFAULT_FECHA_NACIMIENTO
        defaultClienteShouldNotBeFound("fechaNacimiento.notEquals=" + DEFAULT_FECHA_NACIMIENTO);

        // Get all the clienteList where fechaNacimiento not equals to UPDATED_FECHA_NACIMIENTO
        defaultClienteShouldBeFound("fechaNacimiento.notEquals=" + UPDATED_FECHA_NACIMIENTO);
    }

    @Test
    @Transactional
    public void getAllClientesByFechaNacimientoIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaNacimiento in DEFAULT_FECHA_NACIMIENTO or UPDATED_FECHA_NACIMIENTO
        defaultClienteShouldBeFound("fechaNacimiento.in=" + DEFAULT_FECHA_NACIMIENTO + "," + UPDATED_FECHA_NACIMIENTO);

        // Get all the clienteList where fechaNacimiento equals to UPDATED_FECHA_NACIMIENTO
        defaultClienteShouldNotBeFound("fechaNacimiento.in=" + UPDATED_FECHA_NACIMIENTO);
    }

    @Test
    @Transactional
    public void getAllClientesByFechaNacimientoIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaNacimiento is not null
        defaultClienteShouldBeFound("fechaNacimiento.specified=true");

        // Get all the clienteList where fechaNacimiento is null
        defaultClienteShouldNotBeFound("fechaNacimiento.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByFechaNacimientoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaNacimiento is greater than or equal to DEFAULT_FECHA_NACIMIENTO
        defaultClienteShouldBeFound("fechaNacimiento.greaterThanOrEqual=" + DEFAULT_FECHA_NACIMIENTO);

        // Get all the clienteList where fechaNacimiento is greater than or equal to UPDATED_FECHA_NACIMIENTO
        defaultClienteShouldNotBeFound("fechaNacimiento.greaterThanOrEqual=" + UPDATED_FECHA_NACIMIENTO);
    }

    @Test
    @Transactional
    public void getAllClientesByFechaNacimientoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaNacimiento is less than or equal to DEFAULT_FECHA_NACIMIENTO
        defaultClienteShouldBeFound("fechaNacimiento.lessThanOrEqual=" + DEFAULT_FECHA_NACIMIENTO);

        // Get all the clienteList where fechaNacimiento is less than or equal to SMALLER_FECHA_NACIMIENTO
        defaultClienteShouldNotBeFound("fechaNacimiento.lessThanOrEqual=" + SMALLER_FECHA_NACIMIENTO);
    }

    @Test
    @Transactional
    public void getAllClientesByFechaNacimientoIsLessThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaNacimiento is less than DEFAULT_FECHA_NACIMIENTO
        defaultClienteShouldNotBeFound("fechaNacimiento.lessThan=" + DEFAULT_FECHA_NACIMIENTO);

        // Get all the clienteList where fechaNacimiento is less than UPDATED_FECHA_NACIMIENTO
        defaultClienteShouldBeFound("fechaNacimiento.lessThan=" + UPDATED_FECHA_NACIMIENTO);
    }

    @Test
    @Transactional
    public void getAllClientesByFechaNacimientoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaNacimiento is greater than DEFAULT_FECHA_NACIMIENTO
        defaultClienteShouldNotBeFound("fechaNacimiento.greaterThan=" + DEFAULT_FECHA_NACIMIENTO);

        // Get all the clienteList where fechaNacimiento is greater than SMALLER_FECHA_NACIMIENTO
        defaultClienteShouldBeFound("fechaNacimiento.greaterThan=" + SMALLER_FECHA_NACIMIENTO);
    }


    @Test
    @Transactional
    public void getAllClientesBySexoIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where sexo equals to DEFAULT_SEXO
        defaultClienteShouldBeFound("sexo.equals=" + DEFAULT_SEXO);

        // Get all the clienteList where sexo equals to UPDATED_SEXO
        defaultClienteShouldNotBeFound("sexo.equals=" + UPDATED_SEXO);
    }

    @Test
    @Transactional
    public void getAllClientesBySexoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where sexo not equals to DEFAULT_SEXO
        defaultClienteShouldNotBeFound("sexo.notEquals=" + DEFAULT_SEXO);

        // Get all the clienteList where sexo not equals to UPDATED_SEXO
        defaultClienteShouldBeFound("sexo.notEquals=" + UPDATED_SEXO);
    }

    @Test
    @Transactional
    public void getAllClientesBySexoIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where sexo in DEFAULT_SEXO or UPDATED_SEXO
        defaultClienteShouldBeFound("sexo.in=" + DEFAULT_SEXO + "," + UPDATED_SEXO);

        // Get all the clienteList where sexo equals to UPDATED_SEXO
        defaultClienteShouldNotBeFound("sexo.in=" + UPDATED_SEXO);
    }

    @Test
    @Transactional
    public void getAllClientesBySexoIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where sexo is not null
        defaultClienteShouldBeFound("sexo.specified=true");

        // Get all the clienteList where sexo is null
        defaultClienteShouldNotBeFound("sexo.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByEstadoCivilIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where estadoCivil equals to DEFAULT_ESTADO_CIVIL
        defaultClienteShouldBeFound("estadoCivil.equals=" + DEFAULT_ESTADO_CIVIL);

        // Get all the clienteList where estadoCivil equals to UPDATED_ESTADO_CIVIL
        defaultClienteShouldNotBeFound("estadoCivil.equals=" + UPDATED_ESTADO_CIVIL);
    }

    @Test
    @Transactional
    public void getAllClientesByEstadoCivilIsNotEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where estadoCivil not equals to DEFAULT_ESTADO_CIVIL
        defaultClienteShouldNotBeFound("estadoCivil.notEquals=" + DEFAULT_ESTADO_CIVIL);

        // Get all the clienteList where estadoCivil not equals to UPDATED_ESTADO_CIVIL
        defaultClienteShouldBeFound("estadoCivil.notEquals=" + UPDATED_ESTADO_CIVIL);
    }

    @Test
    @Transactional
    public void getAllClientesByEstadoCivilIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where estadoCivil in DEFAULT_ESTADO_CIVIL or UPDATED_ESTADO_CIVIL
        defaultClienteShouldBeFound("estadoCivil.in=" + DEFAULT_ESTADO_CIVIL + "," + UPDATED_ESTADO_CIVIL);

        // Get all the clienteList where estadoCivil equals to UPDATED_ESTADO_CIVIL
        defaultClienteShouldNotBeFound("estadoCivil.in=" + UPDATED_ESTADO_CIVIL);
    }

    @Test
    @Transactional
    public void getAllClientesByEstadoCivilIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where estadoCivil is not null
        defaultClienteShouldBeFound("estadoCivil.specified=true");

        // Get all the clienteList where estadoCivil is null
        defaultClienteShouldNotBeFound("estadoCivil.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByFechaIngresoIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaIngreso equals to DEFAULT_FECHA_INGRESO
        defaultClienteShouldBeFound("fechaIngreso.equals=" + DEFAULT_FECHA_INGRESO);

        // Get all the clienteList where fechaIngreso equals to UPDATED_FECHA_INGRESO
        defaultClienteShouldNotBeFound("fechaIngreso.equals=" + UPDATED_FECHA_INGRESO);
    }

    @Test
    @Transactional
    public void getAllClientesByFechaIngresoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaIngreso not equals to DEFAULT_FECHA_INGRESO
        defaultClienteShouldNotBeFound("fechaIngreso.notEquals=" + DEFAULT_FECHA_INGRESO);

        // Get all the clienteList where fechaIngreso not equals to UPDATED_FECHA_INGRESO
        defaultClienteShouldBeFound("fechaIngreso.notEquals=" + UPDATED_FECHA_INGRESO);
    }

    @Test
    @Transactional
    public void getAllClientesByFechaIngresoIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaIngreso in DEFAULT_FECHA_INGRESO or UPDATED_FECHA_INGRESO
        defaultClienteShouldBeFound("fechaIngreso.in=" + DEFAULT_FECHA_INGRESO + "," + UPDATED_FECHA_INGRESO);

        // Get all the clienteList where fechaIngreso equals to UPDATED_FECHA_INGRESO
        defaultClienteShouldNotBeFound("fechaIngreso.in=" + UPDATED_FECHA_INGRESO);
    }

    @Test
    @Transactional
    public void getAllClientesByFechaIngresoIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaIngreso is not null
        defaultClienteShouldBeFound("fechaIngreso.specified=true");

        // Get all the clienteList where fechaIngreso is null
        defaultClienteShouldNotBeFound("fechaIngreso.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByFechaIngresoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaIngreso is greater than or equal to DEFAULT_FECHA_INGRESO
        defaultClienteShouldBeFound("fechaIngreso.greaterThanOrEqual=" + DEFAULT_FECHA_INGRESO);

        // Get all the clienteList where fechaIngreso is greater than or equal to UPDATED_FECHA_INGRESO
        defaultClienteShouldNotBeFound("fechaIngreso.greaterThanOrEqual=" + UPDATED_FECHA_INGRESO);
    }

    @Test
    @Transactional
    public void getAllClientesByFechaIngresoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaIngreso is less than or equal to DEFAULT_FECHA_INGRESO
        defaultClienteShouldBeFound("fechaIngreso.lessThanOrEqual=" + DEFAULT_FECHA_INGRESO);

        // Get all the clienteList where fechaIngreso is less than or equal to SMALLER_FECHA_INGRESO
        defaultClienteShouldNotBeFound("fechaIngreso.lessThanOrEqual=" + SMALLER_FECHA_INGRESO);
    }

    @Test
    @Transactional
    public void getAllClientesByFechaIngresoIsLessThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaIngreso is less than DEFAULT_FECHA_INGRESO
        defaultClienteShouldNotBeFound("fechaIngreso.lessThan=" + DEFAULT_FECHA_INGRESO);

        // Get all the clienteList where fechaIngreso is less than UPDATED_FECHA_INGRESO
        defaultClienteShouldBeFound("fechaIngreso.lessThan=" + UPDATED_FECHA_INGRESO);
    }

    @Test
    @Transactional
    public void getAllClientesByFechaIngresoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where fechaIngreso is greater than DEFAULT_FECHA_INGRESO
        defaultClienteShouldNotBeFound("fechaIngreso.greaterThan=" + DEFAULT_FECHA_INGRESO);

        // Get all the clienteList where fechaIngreso is greater than SMALLER_FECHA_INGRESO
        defaultClienteShouldBeFound("fechaIngreso.greaterThan=" + SMALLER_FECHA_INGRESO);
    }


    @Test
    @Transactional
    public void getAllClientesByResideColombiaIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where resideColombia equals to DEFAULT_RESIDE_COLOMBIA
        defaultClienteShouldBeFound("resideColombia.equals=" + DEFAULT_RESIDE_COLOMBIA);

        // Get all the clienteList where resideColombia equals to UPDATED_RESIDE_COLOMBIA
        defaultClienteShouldNotBeFound("resideColombia.equals=" + UPDATED_RESIDE_COLOMBIA);
    }

    @Test
    @Transactional
    public void getAllClientesByResideColombiaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where resideColombia not equals to DEFAULT_RESIDE_COLOMBIA
        defaultClienteShouldNotBeFound("resideColombia.notEquals=" + DEFAULT_RESIDE_COLOMBIA);

        // Get all the clienteList where resideColombia not equals to UPDATED_RESIDE_COLOMBIA
        defaultClienteShouldBeFound("resideColombia.notEquals=" + UPDATED_RESIDE_COLOMBIA);
    }

    @Test
    @Transactional
    public void getAllClientesByResideColombiaIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where resideColombia in DEFAULT_RESIDE_COLOMBIA or UPDATED_RESIDE_COLOMBIA
        defaultClienteShouldBeFound("resideColombia.in=" + DEFAULT_RESIDE_COLOMBIA + "," + UPDATED_RESIDE_COLOMBIA);

        // Get all the clienteList where resideColombia equals to UPDATED_RESIDE_COLOMBIA
        defaultClienteShouldNotBeFound("resideColombia.in=" + UPDATED_RESIDE_COLOMBIA);
    }

    @Test
    @Transactional
    public void getAllClientesByResideColombiaIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where resideColombia is not null
        defaultClienteShouldBeFound("resideColombia.specified=true");

        // Get all the clienteList where resideColombia is null
        defaultClienteShouldNotBeFound("resideColombia.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByDeclaranteIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where declarante equals to DEFAULT_DECLARANTE
        defaultClienteShouldBeFound("declarante.equals=" + DEFAULT_DECLARANTE);

        // Get all the clienteList where declarante equals to UPDATED_DECLARANTE
        defaultClienteShouldNotBeFound("declarante.equals=" + UPDATED_DECLARANTE);
    }

    @Test
    @Transactional
    public void getAllClientesByDeclaranteIsNotEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where declarante not equals to DEFAULT_DECLARANTE
        defaultClienteShouldNotBeFound("declarante.notEquals=" + DEFAULT_DECLARANTE);

        // Get all the clienteList where declarante not equals to UPDATED_DECLARANTE
        defaultClienteShouldBeFound("declarante.notEquals=" + UPDATED_DECLARANTE);
    }

    @Test
    @Transactional
    public void getAllClientesByDeclaranteIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where declarante in DEFAULT_DECLARANTE or UPDATED_DECLARANTE
        defaultClienteShouldBeFound("declarante.in=" + DEFAULT_DECLARANTE + "," + UPDATED_DECLARANTE);

        // Get all the clienteList where declarante equals to UPDATED_DECLARANTE
        defaultClienteShouldNotBeFound("declarante.in=" + UPDATED_DECLARANTE);
    }

    @Test
    @Transactional
    public void getAllClientesByDeclaranteIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where declarante is not null
        defaultClienteShouldBeFound("declarante.specified=true");

        // Get all the clienteList where declarante is null
        defaultClienteShouldNotBeFound("declarante.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultClienteShouldBeFound(String filter) throws Exception {
        restClienteMockMvc.perform(get("/api/clientes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cliente.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipo").value(hasItem(DEFAULT_TIPO.toString())))
            .andExpect(jsonPath("$.[*].tipoIdentificacion").value(hasItem(DEFAULT_TIPO_IDENTIFICACION.toString())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE)))
            .andExpect(jsonPath("$.[*].fechaNacimiento").value(hasItem(DEFAULT_FECHA_NACIMIENTO.toString())))
            .andExpect(jsonPath("$.[*].sexo").value(hasItem(DEFAULT_SEXO.toString())))
            .andExpect(jsonPath("$.[*].estadoCivil").value(hasItem(DEFAULT_ESTADO_CIVIL.toString())))
            .andExpect(jsonPath("$.[*].fechaIngreso").value(hasItem(DEFAULT_FECHA_INGRESO.toString())))
            .andExpect(jsonPath("$.[*].resideColombia").value(hasItem(DEFAULT_RESIDE_COLOMBIA.booleanValue())))
            .andExpect(jsonPath("$.[*].declarante").value(hasItem(DEFAULT_DECLARANTE.booleanValue())));

        // Check, that the count call also returns 1
        restClienteMockMvc.perform(get("/api/clientes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultClienteShouldNotBeFound(String filter) throws Exception {
        restClienteMockMvc.perform(get("/api/clientes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restClienteMockMvc.perform(get("/api/clientes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCliente() throws Exception {
        // Get the cliente
        restClienteMockMvc.perform(get("/api/clientes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCliente() throws Exception {
        // Initialize the database
        clienteService.save(cliente);

        int databaseSizeBeforeUpdate = clienteRepository.findAll().size();

        // Update the cliente
        Cliente updatedCliente = clienteRepository.findById(cliente.getId()).get();
        // Disconnect from session so that the updates on updatedCliente are not directly saved in db
        em.detach(updatedCliente);
        updatedCliente
            .tipo(UPDATED_TIPO)
            .tipoIdentificacion(UPDATED_TIPO_IDENTIFICACION)
            .nombre(UPDATED_NOMBRE)
            .fechaNacimiento(UPDATED_FECHA_NACIMIENTO)
            .sexo(UPDATED_SEXO)
            .estadoCivil(UPDATED_ESTADO_CIVIL)
            .fechaIngreso(UPDATED_FECHA_INGRESO)
            .resideColombia(UPDATED_RESIDE_COLOMBIA)
            .declarante(UPDATED_DECLARANTE);

        restClienteMockMvc.perform(put("/api/clientes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCliente)))
            .andExpect(status().isOk());

        // Validate the Cliente in the database
        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeUpdate);
        Cliente testCliente = clienteList.get(clienteList.size() - 1);
        assertThat(testCliente.getTipo()).isEqualTo(UPDATED_TIPO);
        assertThat(testCliente.getTipoIdentificacion()).isEqualTo(UPDATED_TIPO_IDENTIFICACION);
        assertThat(testCliente.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testCliente.getFechaNacimiento()).isEqualTo(UPDATED_FECHA_NACIMIENTO);
        assertThat(testCliente.getSexo()).isEqualTo(UPDATED_SEXO);
        assertThat(testCliente.getEstadoCivil()).isEqualTo(UPDATED_ESTADO_CIVIL);
        assertThat(testCliente.getFechaIngreso()).isEqualTo(UPDATED_FECHA_INGRESO);
        assertThat(testCliente.isResideColombia()).isEqualTo(UPDATED_RESIDE_COLOMBIA);
        assertThat(testCliente.isDeclarante()).isEqualTo(UPDATED_DECLARANTE);
    }

    @Test
    @Transactional
    public void updateNonExistingCliente() throws Exception {
        int databaseSizeBeforeUpdate = clienteRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClienteMockMvc.perform(put("/api/clientes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isBadRequest());

        // Validate the Cliente in the database
        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCliente() throws Exception {
        // Initialize the database
        clienteService.save(cliente);

        int databaseSizeBeforeDelete = clienteRepository.findAll().size();

        // Delete the cliente
        restClienteMockMvc.perform(delete("/api/clientes/{id}", cliente.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
