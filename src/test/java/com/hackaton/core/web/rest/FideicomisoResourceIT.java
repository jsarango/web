package com.hackaton.core.web.rest;

import com.hackaton.core.FrontApp;
import com.hackaton.core.domain.Fideicomiso;
import com.hackaton.core.domain.Fondo;
import com.hackaton.core.domain.Comercial;
import com.hackaton.core.domain.Cliente;
import com.hackaton.core.repository.FideicomisoRepository;
import com.hackaton.core.service.FideicomisoService;
import com.hackaton.core.service.dto.FideicomisoCriteria;
import com.hackaton.core.service.FideicomisoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FideicomisoResource} REST controller.
 */
@SpringBootTest(classes = FrontApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FideicomisoResourceIT {

    private static final String DEFAULT_FIDEICOMISO = "AAAAAA";
    private static final String UPDATED_FIDEICOMISO = "BBBBBB";

    private static final LocalDate DEFAULT_FECHA_CONSTITUCION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA_CONSTITUCION = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_FECHA_CONSTITUCION = LocalDate.ofEpochDay(-1L);

    @Autowired
    private FideicomisoRepository fideicomisoRepository;

    @Autowired
    private FideicomisoService fideicomisoService;

    @Autowired
    private FideicomisoQueryService fideicomisoQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFideicomisoMockMvc;

    private Fideicomiso fideicomiso;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fideicomiso createEntity(EntityManager em) {
        Fideicomiso fideicomiso = new Fideicomiso()
            .fideicomiso(DEFAULT_FIDEICOMISO)
            .fechaConstitucion(DEFAULT_FECHA_CONSTITUCION);
        return fideicomiso;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fideicomiso createUpdatedEntity(EntityManager em) {
        Fideicomiso fideicomiso = new Fideicomiso()
            .fideicomiso(UPDATED_FIDEICOMISO)
            .fechaConstitucion(UPDATED_FECHA_CONSTITUCION);
        return fideicomiso;
    }

    @BeforeEach
    public void initTest() {
        fideicomiso = createEntity(em);
    }

    @Test
    @Transactional
    public void createFideicomiso() throws Exception {
        int databaseSizeBeforeCreate = fideicomisoRepository.findAll().size();
        // Create the Fideicomiso
        restFideicomisoMockMvc.perform(post("/api/fideicomisos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fideicomiso)))
            .andExpect(status().isCreated());

        // Validate the Fideicomiso in the database
        List<Fideicomiso> fideicomisoList = fideicomisoRepository.findAll();
        assertThat(fideicomisoList).hasSize(databaseSizeBeforeCreate + 1);
        Fideicomiso testFideicomiso = fideicomisoList.get(fideicomisoList.size() - 1);
        assertThat(testFideicomiso.getFideicomiso()).isEqualTo(DEFAULT_FIDEICOMISO);
        assertThat(testFideicomiso.getFechaConstitucion()).isEqualTo(DEFAULT_FECHA_CONSTITUCION);
    }

    @Test
    @Transactional
    public void createFideicomisoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fideicomisoRepository.findAll().size();

        // Create the Fideicomiso with an existing ID
        fideicomiso.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFideicomisoMockMvc.perform(post("/api/fideicomisos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fideicomiso)))
            .andExpect(status().isBadRequest());

        // Validate the Fideicomiso in the database
        List<Fideicomiso> fideicomisoList = fideicomisoRepository.findAll();
        assertThat(fideicomisoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFideicomisos() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get all the fideicomisoList
        restFideicomisoMockMvc.perform(get("/api/fideicomisos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fideicomiso.getId().intValue())))
            .andExpect(jsonPath("$.[*].fideicomiso").value(hasItem(DEFAULT_FIDEICOMISO)))
            .andExpect(jsonPath("$.[*].fechaConstitucion").value(hasItem(DEFAULT_FECHA_CONSTITUCION.toString())));
    }
    
    @Test
    @Transactional
    public void getFideicomiso() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get the fideicomiso
        restFideicomisoMockMvc.perform(get("/api/fideicomisos/{id}", fideicomiso.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fideicomiso.getId().intValue()))
            .andExpect(jsonPath("$.fideicomiso").value(DEFAULT_FIDEICOMISO))
            .andExpect(jsonPath("$.fechaConstitucion").value(DEFAULT_FECHA_CONSTITUCION.toString()));
    }


    @Test
    @Transactional
    public void getFideicomisosByIdFiltering() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        Long id = fideicomiso.getId();

        defaultFideicomisoShouldBeFound("id.equals=" + id);
        defaultFideicomisoShouldNotBeFound("id.notEquals=" + id);

        defaultFideicomisoShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultFideicomisoShouldNotBeFound("id.greaterThan=" + id);

        defaultFideicomisoShouldBeFound("id.lessThanOrEqual=" + id);
        defaultFideicomisoShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllFideicomisosByFideicomisoIsEqualToSomething() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get all the fideicomisoList where fideicomiso equals to DEFAULT_FIDEICOMISO
        defaultFideicomisoShouldBeFound("fideicomiso.equals=" + DEFAULT_FIDEICOMISO);

        // Get all the fideicomisoList where fideicomiso equals to UPDATED_FIDEICOMISO
        defaultFideicomisoShouldNotBeFound("fideicomiso.equals=" + UPDATED_FIDEICOMISO);
    }

    @Test
    @Transactional
    public void getAllFideicomisosByFideicomisoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get all the fideicomisoList where fideicomiso not equals to DEFAULT_FIDEICOMISO
        defaultFideicomisoShouldNotBeFound("fideicomiso.notEquals=" + DEFAULT_FIDEICOMISO);

        // Get all the fideicomisoList where fideicomiso not equals to UPDATED_FIDEICOMISO
        defaultFideicomisoShouldBeFound("fideicomiso.notEquals=" + UPDATED_FIDEICOMISO);
    }

    @Test
    @Transactional
    public void getAllFideicomisosByFideicomisoIsInShouldWork() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get all the fideicomisoList where fideicomiso in DEFAULT_FIDEICOMISO or UPDATED_FIDEICOMISO
        defaultFideicomisoShouldBeFound("fideicomiso.in=" + DEFAULT_FIDEICOMISO + "," + UPDATED_FIDEICOMISO);

        // Get all the fideicomisoList where fideicomiso equals to UPDATED_FIDEICOMISO
        defaultFideicomisoShouldNotBeFound("fideicomiso.in=" + UPDATED_FIDEICOMISO);
    }

    @Test
    @Transactional
    public void getAllFideicomisosByFideicomisoIsNullOrNotNull() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get all the fideicomisoList where fideicomiso is not null
        defaultFideicomisoShouldBeFound("fideicomiso.specified=true");

        // Get all the fideicomisoList where fideicomiso is null
        defaultFideicomisoShouldNotBeFound("fideicomiso.specified=false");
    }
                @Test
    @Transactional
    public void getAllFideicomisosByFideicomisoContainsSomething() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get all the fideicomisoList where fideicomiso contains DEFAULT_FIDEICOMISO
        defaultFideicomisoShouldBeFound("fideicomiso.contains=" + DEFAULT_FIDEICOMISO);

        // Get all the fideicomisoList where fideicomiso contains UPDATED_FIDEICOMISO
        defaultFideicomisoShouldNotBeFound("fideicomiso.contains=" + UPDATED_FIDEICOMISO);
    }

    @Test
    @Transactional
    public void getAllFideicomisosByFideicomisoNotContainsSomething() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get all the fideicomisoList where fideicomiso does not contain DEFAULT_FIDEICOMISO
        defaultFideicomisoShouldNotBeFound("fideicomiso.doesNotContain=" + DEFAULT_FIDEICOMISO);

        // Get all the fideicomisoList where fideicomiso does not contain UPDATED_FIDEICOMISO
        defaultFideicomisoShouldBeFound("fideicomiso.doesNotContain=" + UPDATED_FIDEICOMISO);
    }


    @Test
    @Transactional
    public void getAllFideicomisosByFechaConstitucionIsEqualToSomething() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get all the fideicomisoList where fechaConstitucion equals to DEFAULT_FECHA_CONSTITUCION
        defaultFideicomisoShouldBeFound("fechaConstitucion.equals=" + DEFAULT_FECHA_CONSTITUCION);

        // Get all the fideicomisoList where fechaConstitucion equals to UPDATED_FECHA_CONSTITUCION
        defaultFideicomisoShouldNotBeFound("fechaConstitucion.equals=" + UPDATED_FECHA_CONSTITUCION);
    }

    @Test
    @Transactional
    public void getAllFideicomisosByFechaConstitucionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get all the fideicomisoList where fechaConstitucion not equals to DEFAULT_FECHA_CONSTITUCION
        defaultFideicomisoShouldNotBeFound("fechaConstitucion.notEquals=" + DEFAULT_FECHA_CONSTITUCION);

        // Get all the fideicomisoList where fechaConstitucion not equals to UPDATED_FECHA_CONSTITUCION
        defaultFideicomisoShouldBeFound("fechaConstitucion.notEquals=" + UPDATED_FECHA_CONSTITUCION);
    }

    @Test
    @Transactional
    public void getAllFideicomisosByFechaConstitucionIsInShouldWork() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get all the fideicomisoList where fechaConstitucion in DEFAULT_FECHA_CONSTITUCION or UPDATED_FECHA_CONSTITUCION
        defaultFideicomisoShouldBeFound("fechaConstitucion.in=" + DEFAULT_FECHA_CONSTITUCION + "," + UPDATED_FECHA_CONSTITUCION);

        // Get all the fideicomisoList where fechaConstitucion equals to UPDATED_FECHA_CONSTITUCION
        defaultFideicomisoShouldNotBeFound("fechaConstitucion.in=" + UPDATED_FECHA_CONSTITUCION);
    }

    @Test
    @Transactional
    public void getAllFideicomisosByFechaConstitucionIsNullOrNotNull() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get all the fideicomisoList where fechaConstitucion is not null
        defaultFideicomisoShouldBeFound("fechaConstitucion.specified=true");

        // Get all the fideicomisoList where fechaConstitucion is null
        defaultFideicomisoShouldNotBeFound("fechaConstitucion.specified=false");
    }

    @Test
    @Transactional
    public void getAllFideicomisosByFechaConstitucionIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get all the fideicomisoList where fechaConstitucion is greater than or equal to DEFAULT_FECHA_CONSTITUCION
        defaultFideicomisoShouldBeFound("fechaConstitucion.greaterThanOrEqual=" + DEFAULT_FECHA_CONSTITUCION);

        // Get all the fideicomisoList where fechaConstitucion is greater than or equal to UPDATED_FECHA_CONSTITUCION
        defaultFideicomisoShouldNotBeFound("fechaConstitucion.greaterThanOrEqual=" + UPDATED_FECHA_CONSTITUCION);
    }

    @Test
    @Transactional
    public void getAllFideicomisosByFechaConstitucionIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get all the fideicomisoList where fechaConstitucion is less than or equal to DEFAULT_FECHA_CONSTITUCION
        defaultFideicomisoShouldBeFound("fechaConstitucion.lessThanOrEqual=" + DEFAULT_FECHA_CONSTITUCION);

        // Get all the fideicomisoList where fechaConstitucion is less than or equal to SMALLER_FECHA_CONSTITUCION
        defaultFideicomisoShouldNotBeFound("fechaConstitucion.lessThanOrEqual=" + SMALLER_FECHA_CONSTITUCION);
    }

    @Test
    @Transactional
    public void getAllFideicomisosByFechaConstitucionIsLessThanSomething() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get all the fideicomisoList where fechaConstitucion is less than DEFAULT_FECHA_CONSTITUCION
        defaultFideicomisoShouldNotBeFound("fechaConstitucion.lessThan=" + DEFAULT_FECHA_CONSTITUCION);

        // Get all the fideicomisoList where fechaConstitucion is less than UPDATED_FECHA_CONSTITUCION
        defaultFideicomisoShouldBeFound("fechaConstitucion.lessThan=" + UPDATED_FECHA_CONSTITUCION);
    }

    @Test
    @Transactional
    public void getAllFideicomisosByFechaConstitucionIsGreaterThanSomething() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);

        // Get all the fideicomisoList where fechaConstitucion is greater than DEFAULT_FECHA_CONSTITUCION
        defaultFideicomisoShouldNotBeFound("fechaConstitucion.greaterThan=" + DEFAULT_FECHA_CONSTITUCION);

        // Get all the fideicomisoList where fechaConstitucion is greater than SMALLER_FECHA_CONSTITUCION
        defaultFideicomisoShouldBeFound("fechaConstitucion.greaterThan=" + SMALLER_FECHA_CONSTITUCION);
    }


    @Test
    @Transactional
    public void getAllFideicomisosByIdFondoIsEqualToSomething() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);
        Fondo idFondo = FondoResourceIT.createEntity(em);
        em.persist(idFondo);
        em.flush();
        fideicomiso.setIdFondo(idFondo);
        fideicomisoRepository.saveAndFlush(fideicomiso);
        Long idFondoId = idFondo.getId();

        // Get all the fideicomisoList where idFondo equals to idFondoId
        defaultFideicomisoShouldBeFound("idFondoId.equals=" + idFondoId);

        // Get all the fideicomisoList where idFondo equals to idFondoId + 1
        defaultFideicomisoShouldNotBeFound("idFondoId.equals=" + (idFondoId + 1));
    }


    @Test
    @Transactional
    public void getAllFideicomisosByIdComercialIsEqualToSomething() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);
        Comercial idComercial = ComercialResourceIT.createEntity(em);
        em.persist(idComercial);
        em.flush();
        fideicomiso.setIdComercial(idComercial);
        fideicomisoRepository.saveAndFlush(fideicomiso);
        Long idComercialId = idComercial.getId();

        // Get all the fideicomisoList where idComercial equals to idComercialId
        defaultFideicomisoShouldBeFound("idComercialId.equals=" + idComercialId);

        // Get all the fideicomisoList where idComercial equals to idComercialId + 1
        defaultFideicomisoShouldNotBeFound("idComercialId.equals=" + (idComercialId + 1));
    }


    @Test
    @Transactional
    public void getAllFideicomisosByIdClienteIsEqualToSomething() throws Exception {
        // Initialize the database
        fideicomisoRepository.saveAndFlush(fideicomiso);
        Cliente idCliente = ClienteResourceIT.createEntity(em);
        em.persist(idCliente);
        em.flush();
        fideicomiso.setIdCliente(idCliente);
        fideicomisoRepository.saveAndFlush(fideicomiso);
        Long idClienteId = idCliente.getId();

        // Get all the fideicomisoList where idCliente equals to idClienteId
        defaultFideicomisoShouldBeFound("idClienteId.equals=" + idClienteId);

        // Get all the fideicomisoList where idCliente equals to idClienteId + 1
        defaultFideicomisoShouldNotBeFound("idClienteId.equals=" + (idClienteId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFideicomisoShouldBeFound(String filter) throws Exception {
        restFideicomisoMockMvc.perform(get("/api/fideicomisos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fideicomiso.getId().intValue())))
            .andExpect(jsonPath("$.[*].fideicomiso").value(hasItem(DEFAULT_FIDEICOMISO)))
            .andExpect(jsonPath("$.[*].fechaConstitucion").value(hasItem(DEFAULT_FECHA_CONSTITUCION.toString())));

        // Check, that the count call also returns 1
        restFideicomisoMockMvc.perform(get("/api/fideicomisos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFideicomisoShouldNotBeFound(String filter) throws Exception {
        restFideicomisoMockMvc.perform(get("/api/fideicomisos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFideicomisoMockMvc.perform(get("/api/fideicomisos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingFideicomiso() throws Exception {
        // Get the fideicomiso
        restFideicomisoMockMvc.perform(get("/api/fideicomisos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFideicomiso() throws Exception {
        // Initialize the database
        fideicomisoService.save(fideicomiso);

        int databaseSizeBeforeUpdate = fideicomisoRepository.findAll().size();

        // Update the fideicomiso
        Fideicomiso updatedFideicomiso = fideicomisoRepository.findById(fideicomiso.getId()).get();
        // Disconnect from session so that the updates on updatedFideicomiso are not directly saved in db
        em.detach(updatedFideicomiso);
        updatedFideicomiso
            .fideicomiso(UPDATED_FIDEICOMISO)
            .fechaConstitucion(UPDATED_FECHA_CONSTITUCION);

        restFideicomisoMockMvc.perform(put("/api/fideicomisos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFideicomiso)))
            .andExpect(status().isOk());

        // Validate the Fideicomiso in the database
        List<Fideicomiso> fideicomisoList = fideicomisoRepository.findAll();
        assertThat(fideicomisoList).hasSize(databaseSizeBeforeUpdate);
        Fideicomiso testFideicomiso = fideicomisoList.get(fideicomisoList.size() - 1);
        assertThat(testFideicomiso.getFideicomiso()).isEqualTo(UPDATED_FIDEICOMISO);
        assertThat(testFideicomiso.getFechaConstitucion()).isEqualTo(UPDATED_FECHA_CONSTITUCION);
    }

    @Test
    @Transactional
    public void updateNonExistingFideicomiso() throws Exception {
        int databaseSizeBeforeUpdate = fideicomisoRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFideicomisoMockMvc.perform(put("/api/fideicomisos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fideicomiso)))
            .andExpect(status().isBadRequest());

        // Validate the Fideicomiso in the database
        List<Fideicomiso> fideicomisoList = fideicomisoRepository.findAll();
        assertThat(fideicomisoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFideicomiso() throws Exception {
        // Initialize the database
        fideicomisoService.save(fideicomiso);

        int databaseSizeBeforeDelete = fideicomisoRepository.findAll().size();

        // Delete the fideicomiso
        restFideicomisoMockMvc.perform(delete("/api/fideicomisos/{id}", fideicomiso.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Fideicomiso> fideicomisoList = fideicomisoRepository.findAll();
        assertThat(fideicomisoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
