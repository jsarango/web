package com.hackaton.core.web.rest;

import com.hackaton.core.FrontApp;
import com.hackaton.core.domain.PerfilComercial;
import com.hackaton.core.repository.PerfilComercialRepository;
import com.hackaton.core.service.PerfilComercialService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PerfilComercialResource} REST controller.
 */
@SpringBootTest(classes = FrontApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PerfilComercialResourceIT {

    private static final String DEFAULT_DESCRIPCION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION = "BBBBBBBBBB";

    private static final Integer DEFAULT_SALDO_MINIMO = 1;
    private static final Integer UPDATED_SALDO_MINIMO = 2;

    private static final Integer DEFAULT_SALDO_MAXIMO = 1;
    private static final Integer UPDATED_SALDO_MAXIMO = 2;

    @Autowired
    private PerfilComercialRepository perfilComercialRepository;

    @Autowired
    private PerfilComercialService perfilComercialService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPerfilComercialMockMvc;

    private PerfilComercial perfilComercial;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PerfilComercial createEntity(EntityManager em) {
        PerfilComercial perfilComercial = new PerfilComercial()
            .descripcion(DEFAULT_DESCRIPCION)
            .saldoMinimo(DEFAULT_SALDO_MINIMO)
            .saldoMaximo(DEFAULT_SALDO_MAXIMO);
        return perfilComercial;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PerfilComercial createUpdatedEntity(EntityManager em) {
        PerfilComercial perfilComercial = new PerfilComercial()
            .descripcion(UPDATED_DESCRIPCION)
            .saldoMinimo(UPDATED_SALDO_MINIMO)
            .saldoMaximo(UPDATED_SALDO_MAXIMO);
        return perfilComercial;
    }

    @BeforeEach
    public void initTest() {
        perfilComercial = createEntity(em);
    }

    @Test
    @Transactional
    public void createPerfilComercial() throws Exception {
        int databaseSizeBeforeCreate = perfilComercialRepository.findAll().size();
        // Create the PerfilComercial
        restPerfilComercialMockMvc.perform(post("/api/perfil-comercials")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(perfilComercial)))
            .andExpect(status().isCreated());

        // Validate the PerfilComercial in the database
        List<PerfilComercial> perfilComercialList = perfilComercialRepository.findAll();
        assertThat(perfilComercialList).hasSize(databaseSizeBeforeCreate + 1);
        PerfilComercial testPerfilComercial = perfilComercialList.get(perfilComercialList.size() - 1);
        assertThat(testPerfilComercial.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testPerfilComercial.getSaldoMinimo()).isEqualTo(DEFAULT_SALDO_MINIMO);
        assertThat(testPerfilComercial.getSaldoMaximo()).isEqualTo(DEFAULT_SALDO_MAXIMO);
    }

    @Test
    @Transactional
    public void createPerfilComercialWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = perfilComercialRepository.findAll().size();

        // Create the PerfilComercial with an existing ID
        perfilComercial.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPerfilComercialMockMvc.perform(post("/api/perfil-comercials")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(perfilComercial)))
            .andExpect(status().isBadRequest());

        // Validate the PerfilComercial in the database
        List<PerfilComercial> perfilComercialList = perfilComercialRepository.findAll();
        assertThat(perfilComercialList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPerfilComercials() throws Exception {
        // Initialize the database
        perfilComercialRepository.saveAndFlush(perfilComercial);

        // Get all the perfilComercialList
        restPerfilComercialMockMvc.perform(get("/api/perfil-comercials?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(perfilComercial.getId().intValue())))
            .andExpect(jsonPath("$.[*].descripcion").value(hasItem(DEFAULT_DESCRIPCION)))
            .andExpect(jsonPath("$.[*].saldoMinimo").value(hasItem(DEFAULT_SALDO_MINIMO)))
            .andExpect(jsonPath("$.[*].saldoMaximo").value(hasItem(DEFAULT_SALDO_MAXIMO)));
    }
    
    @Test
    @Transactional
    public void getPerfilComercial() throws Exception {
        // Initialize the database
        perfilComercialRepository.saveAndFlush(perfilComercial);

        // Get the perfilComercial
        restPerfilComercialMockMvc.perform(get("/api/perfil-comercials/{id}", perfilComercial.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(perfilComercial.getId().intValue()))
            .andExpect(jsonPath("$.descripcion").value(DEFAULT_DESCRIPCION))
            .andExpect(jsonPath("$.saldoMinimo").value(DEFAULT_SALDO_MINIMO))
            .andExpect(jsonPath("$.saldoMaximo").value(DEFAULT_SALDO_MAXIMO));
    }
    @Test
    @Transactional
    public void getNonExistingPerfilComercial() throws Exception {
        // Get the perfilComercial
        restPerfilComercialMockMvc.perform(get("/api/perfil-comercials/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePerfilComercial() throws Exception {
        // Initialize the database
        perfilComercialService.save(perfilComercial);

        int databaseSizeBeforeUpdate = perfilComercialRepository.findAll().size();

        // Update the perfilComercial
        PerfilComercial updatedPerfilComercial = perfilComercialRepository.findById(perfilComercial.getId()).get();
        // Disconnect from session so that the updates on updatedPerfilComercial are not directly saved in db
        em.detach(updatedPerfilComercial);
        updatedPerfilComercial
            .descripcion(UPDATED_DESCRIPCION)
            .saldoMinimo(UPDATED_SALDO_MINIMO)
            .saldoMaximo(UPDATED_SALDO_MAXIMO);

        restPerfilComercialMockMvc.perform(put("/api/perfil-comercials")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedPerfilComercial)))
            .andExpect(status().isOk());

        // Validate the PerfilComercial in the database
        List<PerfilComercial> perfilComercialList = perfilComercialRepository.findAll();
        assertThat(perfilComercialList).hasSize(databaseSizeBeforeUpdate);
        PerfilComercial testPerfilComercial = perfilComercialList.get(perfilComercialList.size() - 1);
        assertThat(testPerfilComercial.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testPerfilComercial.getSaldoMinimo()).isEqualTo(UPDATED_SALDO_MINIMO);
        assertThat(testPerfilComercial.getSaldoMaximo()).isEqualTo(UPDATED_SALDO_MAXIMO);
    }

    @Test
    @Transactional
    public void updateNonExistingPerfilComercial() throws Exception {
        int databaseSizeBeforeUpdate = perfilComercialRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPerfilComercialMockMvc.perform(put("/api/perfil-comercials")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(perfilComercial)))
            .andExpect(status().isBadRequest());

        // Validate the PerfilComercial in the database
        List<PerfilComercial> perfilComercialList = perfilComercialRepository.findAll();
        assertThat(perfilComercialList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePerfilComercial() throws Exception {
        // Initialize the database
        perfilComercialService.save(perfilComercial);

        int databaseSizeBeforeDelete = perfilComercialRepository.findAll().size();

        // Delete the perfilComercial
        restPerfilComercialMockMvc.perform(delete("/api/perfil-comercials/{id}", perfilComercial.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PerfilComercial> perfilComercialList = perfilComercialRepository.findAll();
        assertThat(perfilComercialList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
