package com.hackaton.core.web.rest;

import com.hackaton.core.FrontApp;
import com.hackaton.core.domain.Fondo;
import com.hackaton.core.repository.FondoRepository;
import com.hackaton.core.service.FondoService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FondoResource} REST controller.
 */
@SpringBootTest(classes = FrontApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FondoResourceIT {

    private static final String DEFAULT_CODIGO = "AAAAA";
    private static final String UPDATED_CODIGO = "BBBBB";

    private static final String DEFAULT_NIT = "AAAAAAAAAA";
    private static final String UPDATED_NIT = "BBBBBBBBBB";

    private static final String DEFAULT_NOMBRE_CORTO = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_CORTO = "BBBBBBBBBB";

    private static final String DEFAULT_NOMBRE_LARGO = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_LARGO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_FECHA_FONDO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA_FONDO = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private FondoRepository fondoRepository;

    @Autowired
    private FondoService fondoService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFondoMockMvc;

    private Fondo fondo;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fondo createEntity(EntityManager em) {
        Fondo fondo = new Fondo()
            .codigo(DEFAULT_CODIGO)
            .nit(DEFAULT_NIT)
            .nombreCorto(DEFAULT_NOMBRE_CORTO)
            .nombreLargo(DEFAULT_NOMBRE_LARGO)
            .fechaFondo(DEFAULT_FECHA_FONDO);
        return fondo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fondo createUpdatedEntity(EntityManager em) {
        Fondo fondo = new Fondo()
            .codigo(UPDATED_CODIGO)
            .nit(UPDATED_NIT)
            .nombreCorto(UPDATED_NOMBRE_CORTO)
            .nombreLargo(UPDATED_NOMBRE_LARGO)
            .fechaFondo(UPDATED_FECHA_FONDO);
        return fondo;
    }

    @BeforeEach
    public void initTest() {
        fondo = createEntity(em);
    }

    @Test
    @Transactional
    public void createFondo() throws Exception {
        int databaseSizeBeforeCreate = fondoRepository.findAll().size();
        // Create the Fondo
        restFondoMockMvc.perform(post("/api/fondos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fondo)))
            .andExpect(status().isCreated());

        // Validate the Fondo in the database
        List<Fondo> fondoList = fondoRepository.findAll();
        assertThat(fondoList).hasSize(databaseSizeBeforeCreate + 1);
        Fondo testFondo = fondoList.get(fondoList.size() - 1);
        assertThat(testFondo.getCodigo()).isEqualTo(DEFAULT_CODIGO);
        assertThat(testFondo.getNit()).isEqualTo(DEFAULT_NIT);
        assertThat(testFondo.getNombreCorto()).isEqualTo(DEFAULT_NOMBRE_CORTO);
        assertThat(testFondo.getNombreLargo()).isEqualTo(DEFAULT_NOMBRE_LARGO);
        assertThat(testFondo.getFechaFondo()).isEqualTo(DEFAULT_FECHA_FONDO);
    }

    @Test
    @Transactional
    public void createFondoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fondoRepository.findAll().size();

        // Create the Fondo with an existing ID
        fondo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFondoMockMvc.perform(post("/api/fondos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fondo)))
            .andExpect(status().isBadRequest());

        // Validate the Fondo in the database
        List<Fondo> fondoList = fondoRepository.findAll();
        assertThat(fondoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFondos() throws Exception {
        // Initialize the database
        fondoRepository.saveAndFlush(fondo);

        // Get all the fondoList
        restFondoMockMvc.perform(get("/api/fondos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fondo.getId().intValue())))
            .andExpect(jsonPath("$.[*].codigo").value(hasItem(DEFAULT_CODIGO)))
            .andExpect(jsonPath("$.[*].nit").value(hasItem(DEFAULT_NIT)))
            .andExpect(jsonPath("$.[*].nombreCorto").value(hasItem(DEFAULT_NOMBRE_CORTO)))
            .andExpect(jsonPath("$.[*].nombreLargo").value(hasItem(DEFAULT_NOMBRE_LARGO)))
            .andExpect(jsonPath("$.[*].fechaFondo").value(hasItem(DEFAULT_FECHA_FONDO.toString())));
    }
    
    @Test
    @Transactional
    public void getFondo() throws Exception {
        // Initialize the database
        fondoRepository.saveAndFlush(fondo);

        // Get the fondo
        restFondoMockMvc.perform(get("/api/fondos/{id}", fondo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fondo.getId().intValue()))
            .andExpect(jsonPath("$.codigo").value(DEFAULT_CODIGO))
            .andExpect(jsonPath("$.nit").value(DEFAULT_NIT))
            .andExpect(jsonPath("$.nombreCorto").value(DEFAULT_NOMBRE_CORTO))
            .andExpect(jsonPath("$.nombreLargo").value(DEFAULT_NOMBRE_LARGO))
            .andExpect(jsonPath("$.fechaFondo").value(DEFAULT_FECHA_FONDO.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingFondo() throws Exception {
        // Get the fondo
        restFondoMockMvc.perform(get("/api/fondos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFondo() throws Exception {
        // Initialize the database
        fondoService.save(fondo);

        int databaseSizeBeforeUpdate = fondoRepository.findAll().size();

        // Update the fondo
        Fondo updatedFondo = fondoRepository.findById(fondo.getId()).get();
        // Disconnect from session so that the updates on updatedFondo are not directly saved in db
        em.detach(updatedFondo);
        updatedFondo
            .codigo(UPDATED_CODIGO)
            .nit(UPDATED_NIT)
            .nombreCorto(UPDATED_NOMBRE_CORTO)
            .nombreLargo(UPDATED_NOMBRE_LARGO)
            .fechaFondo(UPDATED_FECHA_FONDO);

        restFondoMockMvc.perform(put("/api/fondos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFondo)))
            .andExpect(status().isOk());

        // Validate the Fondo in the database
        List<Fondo> fondoList = fondoRepository.findAll();
        assertThat(fondoList).hasSize(databaseSizeBeforeUpdate);
        Fondo testFondo = fondoList.get(fondoList.size() - 1);
        assertThat(testFondo.getCodigo()).isEqualTo(UPDATED_CODIGO);
        assertThat(testFondo.getNit()).isEqualTo(UPDATED_NIT);
        assertThat(testFondo.getNombreCorto()).isEqualTo(UPDATED_NOMBRE_CORTO);
        assertThat(testFondo.getNombreLargo()).isEqualTo(UPDATED_NOMBRE_LARGO);
        assertThat(testFondo.getFechaFondo()).isEqualTo(UPDATED_FECHA_FONDO);
    }

    @Test
    @Transactional
    public void updateNonExistingFondo() throws Exception {
        int databaseSizeBeforeUpdate = fondoRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFondoMockMvc.perform(put("/api/fondos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fondo)))
            .andExpect(status().isBadRequest());

        // Validate the Fondo in the database
        List<Fondo> fondoList = fondoRepository.findAll();
        assertThat(fondoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFondo() throws Exception {
        // Initialize the database
        fondoService.save(fondo);

        int databaseSizeBeforeDelete = fondoRepository.findAll().size();

        // Delete the fondo
        restFondoMockMvc.perform(delete("/api/fondos/{id}", fondo.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Fondo> fondoList = fondoRepository.findAll();
        assertThat(fondoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
