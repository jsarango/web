package com.hackaton.core.web.rest;

import com.hackaton.core.domain.PerfilAhorro;
import com.hackaton.core.service.PerfilAhorroService;
import com.hackaton.core.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hackaton.core.domain.PerfilAhorro}.
 */
@RestController
@RequestMapping("/api")
public class PerfilAhorroResource {

    private final Logger log = LoggerFactory.getLogger(PerfilAhorroResource.class);

    private static final String ENTITY_NAME = "perfilAhorro";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PerfilAhorroService perfilAhorroService;

    public PerfilAhorroResource(PerfilAhorroService perfilAhorroService) {
        this.perfilAhorroService = perfilAhorroService;
    }

    /**
     * {@code POST  /perfil-ahorros} : Create a new perfilAhorro.
     *
     * @param perfilAhorro the perfilAhorro to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new perfilAhorro, or with status {@code 400 (Bad Request)} if the perfilAhorro has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/perfil-ahorros")
    public ResponseEntity<PerfilAhorro> createPerfilAhorro(@RequestBody PerfilAhorro perfilAhorro) throws URISyntaxException {
        log.debug("REST request to save PerfilAhorro : {}", perfilAhorro);
        if (perfilAhorro.getId() != null) {
            throw new BadRequestAlertException("A new perfilAhorro cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PerfilAhorro result = perfilAhorroService.save(perfilAhorro);
        return ResponseEntity.created(new URI("/api/perfil-ahorros/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /perfil-ahorros} : Updates an existing perfilAhorro.
     *
     * @param perfilAhorro the perfilAhorro to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated perfilAhorro,
     * or with status {@code 400 (Bad Request)} if the perfilAhorro is not valid,
     * or with status {@code 500 (Internal Server Error)} if the perfilAhorro couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/perfil-ahorros")
    public ResponseEntity<PerfilAhorro> updatePerfilAhorro(@RequestBody PerfilAhorro perfilAhorro) throws URISyntaxException {
        log.debug("REST request to update PerfilAhorro : {}", perfilAhorro);
        if (perfilAhorro.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PerfilAhorro result = perfilAhorroService.save(perfilAhorro);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, perfilAhorro.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /perfil-ahorros} : get all the perfilAhorros.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of perfilAhorros in body.
     */
    @GetMapping("/perfil-ahorros")
    public List<PerfilAhorro> getAllPerfilAhorros() {
        log.debug("REST request to get all PerfilAhorros");
        return perfilAhorroService.findAll();
    }

    /**
     * {@code GET  /perfil-ahorros/:id} : get the "id" perfilAhorro.
     *
     * @param id the id of the perfilAhorro to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the perfilAhorro, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/perfil-ahorros/{id}")
    public ResponseEntity<PerfilAhorro> getPerfilAhorro(@PathVariable Long id) {
        log.debug("REST request to get PerfilAhorro : {}", id);
        Optional<PerfilAhorro> perfilAhorro = perfilAhorroService.findOne(id);
        return ResponseUtil.wrapOrNotFound(perfilAhorro);
    }

    /**
     * {@code DELETE  /perfil-ahorros/:id} : delete the "id" perfilAhorro.
     *
     * @param id the id of the perfilAhorro to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/perfil-ahorros/{id}")
    public ResponseEntity<Void> deletePerfilAhorro(@PathVariable Long id) {
        log.debug("REST request to delete PerfilAhorro : {}", id);
        perfilAhorroService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
