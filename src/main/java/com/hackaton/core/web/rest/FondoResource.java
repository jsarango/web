package com.hackaton.core.web.rest;

import com.hackaton.core.domain.Fondo;
import com.hackaton.core.service.FondoService;
import com.hackaton.core.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hackaton.core.domain.Fondo}.
 */
@RestController
@RequestMapping("/api")
public class FondoResource {

    private final Logger log = LoggerFactory.getLogger(FondoResource.class);

    private static final String ENTITY_NAME = "fondo";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FondoService fondoService;

    public FondoResource(FondoService fondoService) {
        this.fondoService = fondoService;
    }

    /**
     * {@code POST  /fondos} : Create a new fondo.
     *
     * @param fondo the fondo to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fondo, or with status {@code 400 (Bad Request)} if the fondo has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fondos")
    public ResponseEntity<Fondo> createFondo(@Valid @RequestBody Fondo fondo) throws URISyntaxException {
        log.debug("REST request to save Fondo : {}", fondo);
        if (fondo.getId() != null) {
            throw new BadRequestAlertException("A new fondo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Fondo result = fondoService.save(fondo);
        return ResponseEntity.created(new URI("/api/fondos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fondos} : Updates an existing fondo.
     *
     * @param fondo the fondo to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fondo,
     * or with status {@code 400 (Bad Request)} if the fondo is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fondo couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fondos")
    public ResponseEntity<Fondo> updateFondo(@Valid @RequestBody Fondo fondo) throws URISyntaxException {
        log.debug("REST request to update Fondo : {}", fondo);
        if (fondo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Fondo result = fondoService.save(fondo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, fondo.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fondos} : get all the fondos.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fondos in body.
     */
    @GetMapping("/fondos")
    public List<Fondo> getAllFondos() {
        log.debug("REST request to get all Fondos");
        return fondoService.findAll();
    }

    /**
     * {@code GET  /fondos/:id} : get the "id" fondo.
     *
     * @param id the id of the fondo to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fondo, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fondos/{id}")
    public ResponseEntity<Fondo> getFondo(@PathVariable Long id) {
        log.debug("REST request to get Fondo : {}", id);
        Optional<Fondo> fondo = fondoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(fondo);
    }

    /**
     * {@code DELETE  /fondos/:id} : delete the "id" fondo.
     *
     * @param id the id of the fondo to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fondos/{id}")
    public ResponseEntity<Void> deleteFondo(@PathVariable Long id) {
        log.debug("REST request to delete Fondo : {}", id);
        fondoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
