package com.hackaton.core.web.rest;

import com.hackaton.core.domain.Comercial;
import com.hackaton.core.service.ComercialService;
import com.hackaton.core.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hackaton.core.domain.Comercial}.
 */
@RestController
@RequestMapping("/api")
public class ComercialResource {

    private final Logger log = LoggerFactory.getLogger(ComercialResource.class);

    private static final String ENTITY_NAME = "comercial";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ComercialService comercialService;

    public ComercialResource(ComercialService comercialService) {
        this.comercialService = comercialService;
    }

    /**
     * {@code POST  /comercials} : Create a new comercial.
     *
     * @param comercial the comercial to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new comercial, or with status {@code 400 (Bad Request)} if the comercial has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/comercials")
    public ResponseEntity<Comercial> createComercial(@Valid @RequestBody Comercial comercial) throws URISyntaxException {
        log.debug("REST request to save Comercial : {}", comercial);
        if (comercial.getId() != null) {
            throw new BadRequestAlertException("A new comercial cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Comercial result = comercialService.save(comercial);
        return ResponseEntity.created(new URI("/api/comercials/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /comercials} : Updates an existing comercial.
     *
     * @param comercial the comercial to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated comercial,
     * or with status {@code 400 (Bad Request)} if the comercial is not valid,
     * or with status {@code 500 (Internal Server Error)} if the comercial couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/comercials")
    public ResponseEntity<Comercial> updateComercial(@Valid @RequestBody Comercial comercial) throws URISyntaxException {
        log.debug("REST request to update Comercial : {}", comercial);
        if (comercial.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Comercial result = comercialService.save(comercial);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, comercial.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /comercials} : get all the comercials.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of comercials in body.
     */
    @GetMapping("/comercials")
    public List<Comercial> getAllComercials() {
        log.debug("REST request to get all Comercials");
        return comercialService.findAll();
    }

    /**
     * {@code GET  /comercials/:id} : get the "id" comercial.
     *
     * @param id the id of the comercial to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the comercial, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/comercials/{id}")
    public ResponseEntity<Comercial> getComercial(@PathVariable Long id) {
        log.debug("REST request to get Comercial : {}", id);
        Optional<Comercial> comercial = comercialService.findOne(id);
        return ResponseUtil.wrapOrNotFound(comercial);
    }

    /**
     * {@code DELETE  /comercials/:id} : delete the "id" comercial.
     *
     * @param id the id of the comercial to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/comercials/{id}")
    public ResponseEntity<Void> deleteComercial(@PathVariable Long id) {
        log.debug("REST request to delete Comercial : {}", id);
        comercialService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
