package com.hackaton.core.web.rest;

import com.hackaton.core.domain.Desercion;
import com.hackaton.core.service.DesercionService;
import com.hackaton.core.web.rest.errors.BadRequestAlertException;
import com.hackaton.core.service.dto.DesercionCriteria;
import com.hackaton.core.service.DesercionQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hackaton.core.domain.Desercion}.
 */
@RestController
@RequestMapping("/api")
public class DesercionResource {

    private final Logger log = LoggerFactory.getLogger(DesercionResource.class);

    private static final String ENTITY_NAME = "desercion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DesercionService desercionService;

    private final DesercionQueryService desercionQueryService;

    public DesercionResource(DesercionService desercionService, DesercionQueryService desercionQueryService) {
        this.desercionService = desercionService;
        this.desercionQueryService = desercionQueryService;
    }

    /**
     * {@code POST  /desercions} : Create a new desercion.
     *
     * @param desercion the desercion to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new desercion, or with status {@code 400 (Bad Request)} if the desercion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/desercions")
    public ResponseEntity<Desercion> createDesercion(@Valid @RequestBody Desercion desercion) throws URISyntaxException {
        log.debug("REST request to save Desercion : {}", desercion);
        if (desercion.getId() != null) {
            throw new BadRequestAlertException("A new desercion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Desercion result = desercionService.save(desercion);
        return ResponseEntity.created(new URI("/api/desercions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /desercions} : Updates an existing desercion.
     *
     * @param desercion the desercion to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated desercion,
     * or with status {@code 400 (Bad Request)} if the desercion is not valid,
     * or with status {@code 500 (Internal Server Error)} if the desercion couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/desercions")
    public ResponseEntity<Desercion> updateDesercion(@Valid @RequestBody Desercion desercion) throws URISyntaxException {
        log.debug("REST request to update Desercion : {}", desercion);
        if (desercion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Desercion result = desercionService.save(desercion);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, desercion.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /desercions} : get all the desercions.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of desercions in body.
     */
    @GetMapping("/desercions")
    public ResponseEntity<List<Desercion>> getAllDesercions(DesercionCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Desercions by criteria: {}", criteria);
        Page<Desercion> page = desercionQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /desercions/count} : count all the desercions.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/desercions/count")
    public ResponseEntity<Long> countDesercions(DesercionCriteria criteria) {
        log.debug("REST request to count Desercions by criteria: {}", criteria);
        return ResponseEntity.ok().body(desercionQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /desercions/:id} : get the "id" desercion.
     *
     * @param id the id of the desercion to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the desercion, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/desercions/{id}")
    public ResponseEntity<Desercion> getDesercion(@PathVariable Long id) {
        log.debug("REST request to get Desercion : {}", id);
        Optional<Desercion> desercion = desercionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(desercion);
    }

    /**
     * {@code DELETE  /desercions/:id} : delete the "id" desercion.
     *
     * @param id the id of the desercion to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/desercions/{id}")
    public ResponseEntity<Void> deleteDesercion(@PathVariable Long id) {
        log.debug("REST request to delete Desercion : {}", id);
        desercionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
