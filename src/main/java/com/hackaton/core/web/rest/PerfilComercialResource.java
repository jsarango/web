package com.hackaton.core.web.rest;

import com.hackaton.core.domain.PerfilComercial;
import com.hackaton.core.service.PerfilComercialService;
import com.hackaton.core.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hackaton.core.domain.PerfilComercial}.
 */
@RestController
@RequestMapping("/api")
public class PerfilComercialResource {

    private final Logger log = LoggerFactory.getLogger(PerfilComercialResource.class);

    private static final String ENTITY_NAME = "perfilComercial";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PerfilComercialService perfilComercialService;

    public PerfilComercialResource(PerfilComercialService perfilComercialService) {
        this.perfilComercialService = perfilComercialService;
    }

    /**
     * {@code POST  /perfil-comercials} : Create a new perfilComercial.
     *
     * @param perfilComercial the perfilComercial to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new perfilComercial, or with status {@code 400 (Bad Request)} if the perfilComercial has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/perfil-comercials")
    public ResponseEntity<PerfilComercial> createPerfilComercial(@RequestBody PerfilComercial perfilComercial) throws URISyntaxException {
        log.debug("REST request to save PerfilComercial : {}", perfilComercial);
        if (perfilComercial.getId() != null) {
            throw new BadRequestAlertException("A new perfilComercial cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PerfilComercial result = perfilComercialService.save(perfilComercial);
        return ResponseEntity.created(new URI("/api/perfil-comercials/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /perfil-comercials} : Updates an existing perfilComercial.
     *
     * @param perfilComercial the perfilComercial to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated perfilComercial,
     * or with status {@code 400 (Bad Request)} if the perfilComercial is not valid,
     * or with status {@code 500 (Internal Server Error)} if the perfilComercial couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/perfil-comercials")
    public ResponseEntity<PerfilComercial> updatePerfilComercial(@RequestBody PerfilComercial perfilComercial) throws URISyntaxException {
        log.debug("REST request to update PerfilComercial : {}", perfilComercial);
        if (perfilComercial.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PerfilComercial result = perfilComercialService.save(perfilComercial);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, perfilComercial.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /perfil-comercials} : get all the perfilComercials.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of perfilComercials in body.
     */
    @GetMapping("/perfil-comercials")
    public List<PerfilComercial> getAllPerfilComercials() {
        log.debug("REST request to get all PerfilComercials");
        return perfilComercialService.findAll();
    }

    /**
     * {@code GET  /perfil-comercials/:id} : get the "id" perfilComercial.
     *
     * @param id the id of the perfilComercial to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the perfilComercial, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/perfil-comercials/{id}")
    public ResponseEntity<PerfilComercial> getPerfilComercial(@PathVariable Long id) {
        log.debug("REST request to get PerfilComercial : {}", id);
        Optional<PerfilComercial> perfilComercial = perfilComercialService.findOne(id);
        return ResponseUtil.wrapOrNotFound(perfilComercial);
    }

    /**
     * {@code DELETE  /perfil-comercials/:id} : delete the "id" perfilComercial.
     *
     * @param id the id of the perfilComercial to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/perfil-comercials/{id}")
    public ResponseEntity<Void> deletePerfilComercial(@PathVariable Long id) {
        log.debug("REST request to delete PerfilComercial : {}", id);
        perfilComercialService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
