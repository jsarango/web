package com.hackaton.core.web.rest;

import com.hackaton.core.domain.Fideicomiso;
import com.hackaton.core.service.FideicomisoService;
import com.hackaton.core.web.rest.errors.BadRequestAlertException;
import com.hackaton.core.service.dto.FideicomisoCriteria;
import com.hackaton.core.service.FideicomisoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hackaton.core.domain.Fideicomiso}.
 */
@RestController
@RequestMapping("/api")
public class FideicomisoResource {

    private final Logger log = LoggerFactory.getLogger(FideicomisoResource.class);

    private static final String ENTITY_NAME = "fideicomiso";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FideicomisoService fideicomisoService;

    private final FideicomisoQueryService fideicomisoQueryService;

    public FideicomisoResource(FideicomisoService fideicomisoService, FideicomisoQueryService fideicomisoQueryService) {
        this.fideicomisoService = fideicomisoService;
        this.fideicomisoQueryService = fideicomisoQueryService;
    }

    /**
     * {@code POST  /fideicomisos} : Create a new fideicomiso.
     *
     * @param fideicomiso the fideicomiso to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fideicomiso, or with status {@code 400 (Bad Request)} if the fideicomiso has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fideicomisos")
    public ResponseEntity<Fideicomiso> createFideicomiso(@Valid @RequestBody Fideicomiso fideicomiso) throws URISyntaxException {
        log.debug("REST request to save Fideicomiso : {}", fideicomiso);
        if (fideicomiso.getId() != null) {
            throw new BadRequestAlertException("A new fideicomiso cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Fideicomiso result = fideicomisoService.save(fideicomiso);
        return ResponseEntity.created(new URI("/api/fideicomisos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fideicomisos} : Updates an existing fideicomiso.
     *
     * @param fideicomiso the fideicomiso to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fideicomiso,
     * or with status {@code 400 (Bad Request)} if the fideicomiso is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fideicomiso couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fideicomisos")
    public ResponseEntity<Fideicomiso> updateFideicomiso(@Valid @RequestBody Fideicomiso fideicomiso) throws URISyntaxException {
        log.debug("REST request to update Fideicomiso : {}", fideicomiso);
        if (fideicomiso.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Fideicomiso result = fideicomisoService.save(fideicomiso);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, fideicomiso.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fideicomisos} : get all the fideicomisos.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fideicomisos in body.
     */
    @GetMapping("/fideicomisos")
    public ResponseEntity<List<Fideicomiso>> getAllFideicomisos(FideicomisoCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Fideicomisos by criteria: {}", criteria);
        Page<Fideicomiso> page = fideicomisoQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /fideicomisos/count} : count all the fideicomisos.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/fideicomisos/count")
    public ResponseEntity<Long> countFideicomisos(FideicomisoCriteria criteria) {
        log.debug("REST request to count Fideicomisos by criteria: {}", criteria);
        return ResponseEntity.ok().body(fideicomisoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /fideicomisos/:id} : get the "id" fideicomiso.
     *
     * @param id the id of the fideicomiso to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fideicomiso, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fideicomisos/{id}")
    public ResponseEntity<Fideicomiso> getFideicomiso(@PathVariable Long id) {
        log.debug("REST request to get Fideicomiso : {}", id);
        Optional<Fideicomiso> fideicomiso = fideicomisoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(fideicomiso);
    }

    /**
     * {@code DELETE  /fideicomisos/:id} : delete the "id" fideicomiso.
     *
     * @param id the id of the fideicomiso to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fideicomisos/{id}")
    public ResponseEntity<Void> deleteFideicomiso(@PathVariable Long id) {
        log.debug("REST request to delete Fideicomiso : {}", id);
        fideicomisoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
