/**
 * View Models used by Spring MVC REST controllers.
 */
package com.hackaton.core.web.rest.vm;
