package com.hackaton.core.domain.enumeration;

/**
 * The TipoCliente enumeration.
 */
public enum TipoCliente {
    N, J
}
