package com.hackaton.core.domain.enumeration;

/**
 * The Sexo enumeration.
 */
public enum Sexo {
    M, F
}
