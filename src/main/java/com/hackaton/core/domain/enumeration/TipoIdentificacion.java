package com.hackaton.core.domain.enumeration;

/**
 * The TipoIdentificacion enumeration.
 */
public enum TipoIdentificacion {
    C, N, CE, TI, RC
}
