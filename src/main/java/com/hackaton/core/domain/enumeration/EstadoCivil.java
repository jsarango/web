package com.hackaton.core.domain.enumeration;

/**
 * The EstadoCivil enumeration.
 */
public enum EstadoCivil {
    S, C, D, UL, V
}
