package com.hackaton.core.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Fondo.
 */
@Entity
@Table(name = "fondo")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Fondo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(min = 3, max = 5)
    @Column(name = "codigo", length = 5)
    private String codigo;

    @Size(min = 6, max = 15)
    @Column(name = "nit", length = 15)
    private String nit;

    @Column(name = "nombre_corto")
    private String nombreCorto;

    @Column(name = "nombre_largo")
    private String nombreLargo;

    @Column(name = "fecha_fondo")
    private LocalDate fechaFondo;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public Fondo codigo(String codigo) {
        this.codigo = codigo;
        return this;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNit() {
        return nit;
    }

    public Fondo nit(String nit) {
        this.nit = nit;
        return this;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public Fondo nombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
        return this;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public String getNombreLargo() {
        return nombreLargo;
    }

    public Fondo nombreLargo(String nombreLargo) {
        this.nombreLargo = nombreLargo;
        return this;
    }

    public void setNombreLargo(String nombreLargo) {
        this.nombreLargo = nombreLargo;
    }

    public LocalDate getFechaFondo() {
        return fechaFondo;
    }

    public Fondo fechaFondo(LocalDate fechaFondo) {
        this.fechaFondo = fechaFondo;
        return this;
    }

    public void setFechaFondo(LocalDate fechaFondo) {
        this.fechaFondo = fechaFondo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fondo)) {
            return false;
        }
        return id != null && id.equals(((Fondo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Fondo{" +
            "id=" + getId() +
            ", codigo='" + getCodigo() + "'" +
            ", nit='" + getNit() + "'" +
            ", nombreCorto='" + getNombreCorto() + "'" +
            ", nombreLargo='" + getNombreLargo() + "'" +
            ", fechaFondo='" + getFechaFondo() + "'" +
            "}";
    }
}
