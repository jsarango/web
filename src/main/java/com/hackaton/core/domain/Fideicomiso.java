package com.hackaton.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Fideicomiso.
 */
@Entity
@Table(name = "fideicomiso")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Fideicomiso implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(min = 2, max = 6)
    @Column(name = "fideicomiso", length = 6)
    private String fideicomiso;

    @Column(name = "fecha_constitucion")
    private LocalDate fechaConstitucion;

    @ManyToOne
    @JsonIgnoreProperties(value = "fideicomisos", allowSetters = true)
    private Fondo idFondo;

    @ManyToOne
    @JsonIgnoreProperties(value = "fideicomisos", allowSetters = true)
    private Comercial idComercial;

    @ManyToOne
    @JsonIgnoreProperties(value = "fideicomisos", allowSetters = true)
    private Cliente idCliente;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFideicomiso() {
        return fideicomiso;
    }

    public Fideicomiso fideicomiso(String fideicomiso) {
        this.fideicomiso = fideicomiso;
        return this;
    }

    public void setFideicomiso(String fideicomiso) {
        this.fideicomiso = fideicomiso;
    }

    public LocalDate getFechaConstitucion() {
        return fechaConstitucion;
    }

    public Fideicomiso fechaConstitucion(LocalDate fechaConstitucion) {
        this.fechaConstitucion = fechaConstitucion;
        return this;
    }

    public void setFechaConstitucion(LocalDate fechaConstitucion) {
        this.fechaConstitucion = fechaConstitucion;
    }

    public Fondo getIdFondo() {
        return idFondo;
    }

    public Fideicomiso idFondo(Fondo fondo) {
        this.idFondo = fondo;
        return this;
    }

    public void setIdFondo(Fondo fondo) {
        this.idFondo = fondo;
    }

    public Comercial getIdComercial() {
        return idComercial;
    }

    public Fideicomiso idComercial(Comercial comercial) {
        this.idComercial = comercial;
        return this;
    }

    public void setIdComercial(Comercial comercial) {
        this.idComercial = comercial;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public Fideicomiso idCliente(Cliente cliente) {
        this.idCliente = cliente;
        return this;
    }

    public void setIdCliente(Cliente cliente) {
        this.idCliente = cliente;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fideicomiso)) {
            return false;
        }
        return id != null && id.equals(((Fideicomiso) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Fideicomiso{" +
            "id=" + getId() +
            ", fideicomiso='" + getFideicomiso() + "'" +
            ", fechaConstitucion='" + getFechaConstitucion() + "'" +
            "}";
    }
}
