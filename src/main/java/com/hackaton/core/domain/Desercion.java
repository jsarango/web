package com.hackaton.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Desercion.
 */
@Entity
@Table(name = "desercion")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Desercion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "fecha")
    private LocalDate fecha;

    @Size(min = 1, max = 3)
    @Column(name = "edad", length = 3)
    private String edad;

    @Column(name = "nro_promedio_retiros")
    private Integer nroPromedioRetiros;

    @Column(name = "nro_promedio_aportes")
    private Integer nroPromedioAportes;

    @Column(name = "neto_prom_movimientos")
    private Integer netoPromMovimientos;

    @Column(name = "dias_ultimo_mov")
    private Integer diasUltimoMov;

    @Column(name = "posible_sul")
    private Boolean posibleSul;

    @ManyToOne
    @JsonIgnoreProperties(value = "desercions", allowSetters = true)
    private Cliente idCliente;

    @ManyToOne
    @JsonIgnoreProperties(value = "desercions", allowSetters = true)
    private Fondo idFondo;

    @ManyToOne
    @JsonIgnoreProperties(value = "desercions", allowSetters = true)
    private Fideicomiso idFideicomiso;

    @ManyToOne
    @JsonIgnoreProperties(value = "desercions", allowSetters = true)
    private Comercial idComercial;

    @ManyToOne
    @JsonIgnoreProperties(value = "desercions", allowSetters = true)
    private PerfilAhorro idPerfilAhorro;

    @ManyToOne
    @JsonIgnoreProperties(value = "desercions", allowSetters = true)
    private PerfilComercial idPerfilComercial;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public Desercion fecha(LocalDate fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getEdad() {
        return edad;
    }

    public Desercion edad(String edad) {
        this.edad = edad;
        return this;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public Integer getNroPromedioRetiros() {
        return nroPromedioRetiros;
    }

    public Desercion nroPromedioRetiros(Integer nroPromedioRetiros) {
        this.nroPromedioRetiros = nroPromedioRetiros;
        return this;
    }

    public void setNroPromedioRetiros(Integer nroPromedioRetiros) {
        this.nroPromedioRetiros = nroPromedioRetiros;
    }

    public Integer getNroPromedioAportes() {
        return nroPromedioAportes;
    }

    public Desercion nroPromedioAportes(Integer nroPromedioAportes) {
        this.nroPromedioAportes = nroPromedioAportes;
        return this;
    }

    public void setNroPromedioAportes(Integer nroPromedioAportes) {
        this.nroPromedioAportes = nroPromedioAportes;
    }

    public Integer getNetoPromMovimientos() {
        return netoPromMovimientos;
    }

    public Desercion netoPromMovimientos(Integer netoPromMovimientos) {
        this.netoPromMovimientos = netoPromMovimientos;
        return this;
    }

    public void setNetoPromMovimientos(Integer netoPromMovimientos) {
        this.netoPromMovimientos = netoPromMovimientos;
    }

    public Integer getDiasUltimoMov() {
        return diasUltimoMov;
    }

    public Desercion diasUltimoMov(Integer diasUltimoMov) {
        this.diasUltimoMov = diasUltimoMov;
        return this;
    }

    public void setDiasUltimoMov(Integer diasUltimoMov) {
        this.diasUltimoMov = diasUltimoMov;
    }

    public Boolean isPosibleSul() {
        return posibleSul;
    }

    public Desercion posibleSul(Boolean posibleSul) {
        this.posibleSul = posibleSul;
        return this;
    }

    public void setPosibleSul(Boolean posibleSul) {
        this.posibleSul = posibleSul;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public Desercion idCliente(Cliente cliente) {
        this.idCliente = cliente;
        return this;
    }

    public void setIdCliente(Cliente cliente) {
        this.idCliente = cliente;
    }

    public Fondo getIdFondo() {
        return idFondo;
    }

    public Desercion idFondo(Fondo fondo) {
        this.idFondo = fondo;
        return this;
    }

    public void setIdFondo(Fondo fondo) {
        this.idFondo = fondo;
    }

    public Fideicomiso getIdFideicomiso() {
        return idFideicomiso;
    }

    public Desercion idFideicomiso(Fideicomiso fideicomiso) {
        this.idFideicomiso = fideicomiso;
        return this;
    }

    public void setIdFideicomiso(Fideicomiso fideicomiso) {
        this.idFideicomiso = fideicomiso;
    }

    public Comercial getIdComercial() {
        return idComercial;
    }

    public Desercion idComercial(Comercial comercial) {
        this.idComercial = comercial;
        return this;
    }

    public void setIdComercial(Comercial comercial) {
        this.idComercial = comercial;
    }

    public PerfilAhorro getIdPerfilAhorro() {
        return idPerfilAhorro;
    }

    public Desercion idPerfilAhorro(PerfilAhorro perfilAhorro) {
        this.idPerfilAhorro = perfilAhorro;
        return this;
    }

    public void setIdPerfilAhorro(PerfilAhorro perfilAhorro) {
        this.idPerfilAhorro = perfilAhorro;
    }

    public PerfilComercial getIdPerfilComercial() {
        return idPerfilComercial;
    }

    public Desercion idPerfilComercial(PerfilComercial perfilComercial) {
        this.idPerfilComercial = perfilComercial;
        return this;
    }

    public void setIdPerfilComercial(PerfilComercial perfilComercial) {
        this.idPerfilComercial = perfilComercial;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Desercion)) {
            return false;
        }
        return id != null && id.equals(((Desercion) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Desercion{" +
            "id=" + getId() +
            ", fecha='" + getFecha() + "'" +
            ", edad='" + getEdad() + "'" +
            ", nroPromedioRetiros=" + getNroPromedioRetiros() +
            ", nroPromedioAportes=" + getNroPromedioAportes() +
            ", netoPromMovimientos=" + getNetoPromMovimientos() +
            ", diasUltimoMov=" + getDiasUltimoMov() +
            ", posibleSul='" + isPosibleSul() + "'" +
            "}";
    }
}
