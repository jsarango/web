package com.hackaton.core.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A PerfilComercial.
 */
@Entity
@Table(name = "perfil_comercial")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PerfilComercial implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "saldo_minimo")
    private Integer saldoMinimo;

    @Column(name = "saldo_maximo")
    private Integer saldoMaximo;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public PerfilComercial descripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getSaldoMinimo() {
        return saldoMinimo;
    }

    public PerfilComercial saldoMinimo(Integer saldoMinimo) {
        this.saldoMinimo = saldoMinimo;
        return this;
    }

    public void setSaldoMinimo(Integer saldoMinimo) {
        this.saldoMinimo = saldoMinimo;
    }

    public Integer getSaldoMaximo() {
        return saldoMaximo;
    }

    public PerfilComercial saldoMaximo(Integer saldoMaximo) {
        this.saldoMaximo = saldoMaximo;
        return this;
    }

    public void setSaldoMaximo(Integer saldoMaximo) {
        this.saldoMaximo = saldoMaximo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PerfilComercial)) {
            return false;
        }
        return id != null && id.equals(((PerfilComercial) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PerfilComercial{" +
            "id=" + getId() +
            ", descripcion='" + getDescripcion() + "'" +
            ", saldoMinimo=" + getSaldoMinimo() +
            ", saldoMaximo=" + getSaldoMaximo() +
            "}";
    }
}
