package com.hackaton.core.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hackaton.core.domain.Desercion;
import com.hackaton.core.domain.*; // for static metamodels
import com.hackaton.core.repository.DesercionRepository;
import com.hackaton.core.service.dto.DesercionCriteria;

/**
 * Service for executing complex queries for {@link Desercion} entities in the database.
 * The main input is a {@link DesercionCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Desercion} or a {@link Page} of {@link Desercion} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DesercionQueryService extends QueryService<Desercion> {

    private final Logger log = LoggerFactory.getLogger(DesercionQueryService.class);

    private final DesercionRepository desercionRepository;

    public DesercionQueryService(DesercionRepository desercionRepository) {
        this.desercionRepository = desercionRepository;
    }

    /**
     * Return a {@link List} of {@link Desercion} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Desercion> findByCriteria(DesercionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Desercion> specification = createSpecification(criteria);
        return desercionRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Desercion} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Desercion> findByCriteria(DesercionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Desercion> specification = createSpecification(criteria);
        return desercionRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DesercionCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Desercion> specification = createSpecification(criteria);
        return desercionRepository.count(specification);
    }

    /**
     * Function to convert {@link DesercionCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Desercion> createSpecification(DesercionCriteria criteria) {
        Specification<Desercion> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Desercion_.id));
            }
            if (criteria.getFecha() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFecha(), Desercion_.fecha));
            }
            if (criteria.getEdad() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEdad(), Desercion_.edad));
            }
            if (criteria.getNroPromedioRetiros() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNroPromedioRetiros(), Desercion_.nroPromedioRetiros));
            }
            if (criteria.getNroPromedioAportes() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNroPromedioAportes(), Desercion_.nroPromedioAportes));
            }
            if (criteria.getNetoPromMovimientos() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNetoPromMovimientos(), Desercion_.netoPromMovimientos));
            }
            if (criteria.getDiasUltimoMov() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDiasUltimoMov(), Desercion_.diasUltimoMov));
            }
            if (criteria.getPosibleSul() != null) {
                specification = specification.and(buildSpecification(criteria.getPosibleSul(), Desercion_.posibleSul));
            }
            if (criteria.getIdClienteId() != null) {
                specification = specification.and(buildSpecification(criteria.getIdClienteId(),
                    root -> root.join(Desercion_.idCliente, JoinType.LEFT).get(Cliente_.id)));
            }
            if (criteria.getIdFondoId() != null) {
                specification = specification.and(buildSpecification(criteria.getIdFondoId(),
                    root -> root.join(Desercion_.idFondo, JoinType.LEFT).get(Fondo_.id)));
            }
            if (criteria.getIdFideicomisoId() != null) {
                specification = specification.and(buildSpecification(criteria.getIdFideicomisoId(),
                    root -> root.join(Desercion_.idFideicomiso, JoinType.LEFT).get(Fideicomiso_.id)));
            }
            if (criteria.getIdComercialId() != null) {
                specification = specification.and(buildSpecification(criteria.getIdComercialId(),
                    root -> root.join(Desercion_.idComercial, JoinType.LEFT).get(Comercial_.id)));
            }
            if (criteria.getIdPerfilAhorroId() != null) {
                specification = specification.and(buildSpecification(criteria.getIdPerfilAhorroId(),
                    root -> root.join(Desercion_.idPerfilAhorro, JoinType.LEFT).get(PerfilAhorro_.id)));
            }
            if (criteria.getIdPerfilComercialId() != null) {
                specification = specification.and(buildSpecification(criteria.getIdPerfilComercialId(),
                    root -> root.join(Desercion_.idPerfilComercial, JoinType.LEFT).get(PerfilComercial_.id)));
            }
        }
        return specification;
    }
}
