package com.hackaton.core.service;

import com.hackaton.core.domain.Comercial;
import com.hackaton.core.repository.ComercialRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Comercial}.
 */
@Service
@Transactional
public class ComercialService {

    private final Logger log = LoggerFactory.getLogger(ComercialService.class);

    private final ComercialRepository comercialRepository;

    public ComercialService(ComercialRepository comercialRepository) {
        this.comercialRepository = comercialRepository;
    }

    /**
     * Save a comercial.
     *
     * @param comercial the entity to save.
     * @return the persisted entity.
     */
    public Comercial save(Comercial comercial) {
        log.debug("Request to save Comercial : {}", comercial);
        return comercialRepository.save(comercial);
    }

    /**
     * Get all the comercials.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Comercial> findAll() {
        log.debug("Request to get all Comercials");
        return comercialRepository.findAll();
    }


    /**
     * Get one comercial by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Comercial> findOne(Long id) {
        log.debug("Request to get Comercial : {}", id);
        return comercialRepository.findById(id);
    }

    /**
     * Delete the comercial by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Comercial : {}", id);
        comercialRepository.deleteById(id);
    }
}
