package com.hackaton.core.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hackaton.core.domain.Fideicomiso;
import com.hackaton.core.domain.*; // for static metamodels
import com.hackaton.core.repository.FideicomisoRepository;
import com.hackaton.core.service.dto.FideicomisoCriteria;

/**
 * Service for executing complex queries for {@link Fideicomiso} entities in the database.
 * The main input is a {@link FideicomisoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Fideicomiso} or a {@link Page} of {@link Fideicomiso} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FideicomisoQueryService extends QueryService<Fideicomiso> {

    private final Logger log = LoggerFactory.getLogger(FideicomisoQueryService.class);

    private final FideicomisoRepository fideicomisoRepository;

    public FideicomisoQueryService(FideicomisoRepository fideicomisoRepository) {
        this.fideicomisoRepository = fideicomisoRepository;
    }

    /**
     * Return a {@link List} of {@link Fideicomiso} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Fideicomiso> findByCriteria(FideicomisoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Fideicomiso> specification = createSpecification(criteria);
        return fideicomisoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Fideicomiso} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Fideicomiso> findByCriteria(FideicomisoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Fideicomiso> specification = createSpecification(criteria);
        return fideicomisoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FideicomisoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Fideicomiso> specification = createSpecification(criteria);
        return fideicomisoRepository.count(specification);
    }

    /**
     * Function to convert {@link FideicomisoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Fideicomiso> createSpecification(FideicomisoCriteria criteria) {
        Specification<Fideicomiso> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Fideicomiso_.id));
            }
            if (criteria.getFideicomiso() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFideicomiso(), Fideicomiso_.fideicomiso));
            }
            if (criteria.getFechaConstitucion() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFechaConstitucion(), Fideicomiso_.fechaConstitucion));
            }
            if (criteria.getIdFondoId() != null) {
                specification = specification.and(buildSpecification(criteria.getIdFondoId(),
                    root -> root.join(Fideicomiso_.idFondo, JoinType.LEFT).get(Fondo_.id)));
            }
            if (criteria.getIdComercialId() != null) {
                specification = specification.and(buildSpecification(criteria.getIdComercialId(),
                    root -> root.join(Fideicomiso_.idComercial, JoinType.LEFT).get(Comercial_.id)));
            }
            if (criteria.getIdClienteId() != null) {
                specification = specification.and(buildSpecification(criteria.getIdClienteId(),
                    root -> root.join(Fideicomiso_.idCliente, JoinType.LEFT).get(Cliente_.id)));
            }
        }
        return specification;
    }
}
