package com.hackaton.core.service;

import com.hackaton.core.domain.PerfilComercial;
import com.hackaton.core.repository.PerfilComercialRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link PerfilComercial}.
 */
@Service
@Transactional
public class PerfilComercialService {

    private final Logger log = LoggerFactory.getLogger(PerfilComercialService.class);

    private final PerfilComercialRepository perfilComercialRepository;

    public PerfilComercialService(PerfilComercialRepository perfilComercialRepository) {
        this.perfilComercialRepository = perfilComercialRepository;
    }

    /**
     * Save a perfilComercial.
     *
     * @param perfilComercial the entity to save.
     * @return the persisted entity.
     */
    public PerfilComercial save(PerfilComercial perfilComercial) {
        log.debug("Request to save PerfilComercial : {}", perfilComercial);
        return perfilComercialRepository.save(perfilComercial);
    }

    /**
     * Get all the perfilComercials.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PerfilComercial> findAll() {
        log.debug("Request to get all PerfilComercials");
        return perfilComercialRepository.findAll();
    }


    /**
     * Get one perfilComercial by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PerfilComercial> findOne(Long id) {
        log.debug("Request to get PerfilComercial : {}", id);
        return perfilComercialRepository.findById(id);
    }

    /**
     * Delete the perfilComercial by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PerfilComercial : {}", id);
        perfilComercialRepository.deleteById(id);
    }
}
