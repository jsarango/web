package com.hackaton.core.service;

import com.hackaton.core.domain.Fideicomiso;
import com.hackaton.core.repository.FideicomisoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Fideicomiso}.
 */
@Service
@Transactional
public class FideicomisoService {

    private final Logger log = LoggerFactory.getLogger(FideicomisoService.class);

    private final FideicomisoRepository fideicomisoRepository;

    public FideicomisoService(FideicomisoRepository fideicomisoRepository) {
        this.fideicomisoRepository = fideicomisoRepository;
    }

    /**
     * Save a fideicomiso.
     *
     * @param fideicomiso the entity to save.
     * @return the persisted entity.
     */
    public Fideicomiso save(Fideicomiso fideicomiso) {
        log.debug("Request to save Fideicomiso : {}", fideicomiso);
        return fideicomisoRepository.save(fideicomiso);
    }

    /**
     * Get all the fideicomisos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Fideicomiso> findAll(Pageable pageable) {
        log.debug("Request to get all Fideicomisos");
        return fideicomisoRepository.findAll(pageable);
    }


    /**
     * Get one fideicomiso by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Fideicomiso> findOne(Long id) {
        log.debug("Request to get Fideicomiso : {}", id);
        return fideicomisoRepository.findById(id);
    }

    /**
     * Delete the fideicomiso by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Fideicomiso : {}", id);
        fideicomisoRepository.deleteById(id);
    }
}
