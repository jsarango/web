package com.hackaton.core.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.hackaton.core.domain.Desercion} entity. This class is used
 * in {@link com.hackaton.core.web.rest.DesercionResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /desercions?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DesercionCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LocalDateFilter fecha;

    private StringFilter edad;

    private IntegerFilter nroPromedioRetiros;

    private IntegerFilter nroPromedioAportes;

    private IntegerFilter netoPromMovimientos;

    private IntegerFilter diasUltimoMov;

    private BooleanFilter posibleSul;

    private LongFilter idClienteId;

    private LongFilter idFondoId;

    private LongFilter idFideicomisoId;

    private LongFilter idComercialId;

    private LongFilter idPerfilAhorroId;

    private LongFilter idPerfilComercialId;

    public DesercionCriteria() {
    }

    public DesercionCriteria(DesercionCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.fecha = other.fecha == null ? null : other.fecha.copy();
        this.edad = other.edad == null ? null : other.edad.copy();
        this.nroPromedioRetiros = other.nroPromedioRetiros == null ? null : other.nroPromedioRetiros.copy();
        this.nroPromedioAportes = other.nroPromedioAportes == null ? null : other.nroPromedioAportes.copy();
        this.netoPromMovimientos = other.netoPromMovimientos == null ? null : other.netoPromMovimientos.copy();
        this.diasUltimoMov = other.diasUltimoMov == null ? null : other.diasUltimoMov.copy();
        this.posibleSul = other.posibleSul == null ? null : other.posibleSul.copy();
        this.idClienteId = other.idClienteId == null ? null : other.idClienteId.copy();
        this.idFondoId = other.idFondoId == null ? null : other.idFondoId.copy();
        this.idFideicomisoId = other.idFideicomisoId == null ? null : other.idFideicomisoId.copy();
        this.idComercialId = other.idComercialId == null ? null : other.idComercialId.copy();
        this.idPerfilAhorroId = other.idPerfilAhorroId == null ? null : other.idPerfilAhorroId.copy();
        this.idPerfilComercialId = other.idPerfilComercialId == null ? null : other.idPerfilComercialId.copy();
    }

    @Override
    public DesercionCriteria copy() {
        return new DesercionCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LocalDateFilter getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateFilter fecha) {
        this.fecha = fecha;
    }

    public StringFilter getEdad() {
        return edad;
    }

    public void setEdad(StringFilter edad) {
        this.edad = edad;
    }

    public IntegerFilter getNroPromedioRetiros() {
        return nroPromedioRetiros;
    }

    public void setNroPromedioRetiros(IntegerFilter nroPromedioRetiros) {
        this.nroPromedioRetiros = nroPromedioRetiros;
    }

    public IntegerFilter getNroPromedioAportes() {
        return nroPromedioAportes;
    }

    public void setNroPromedioAportes(IntegerFilter nroPromedioAportes) {
        this.nroPromedioAportes = nroPromedioAportes;
    }

    public IntegerFilter getNetoPromMovimientos() {
        return netoPromMovimientos;
    }

    public void setNetoPromMovimientos(IntegerFilter netoPromMovimientos) {
        this.netoPromMovimientos = netoPromMovimientos;
    }

    public IntegerFilter getDiasUltimoMov() {
        return diasUltimoMov;
    }

    public void setDiasUltimoMov(IntegerFilter diasUltimoMov) {
        this.diasUltimoMov = diasUltimoMov;
    }

    public BooleanFilter getPosibleSul() {
        return posibleSul;
    }

    public void setPosibleSul(BooleanFilter posibleSul) {
        this.posibleSul = posibleSul;
    }

    public LongFilter getIdClienteId() {
        return idClienteId;
    }

    public void setIdClienteId(LongFilter idClienteId) {
        this.idClienteId = idClienteId;
    }

    public LongFilter getIdFondoId() {
        return idFondoId;
    }

    public void setIdFondoId(LongFilter idFondoId) {
        this.idFondoId = idFondoId;
    }

    public LongFilter getIdFideicomisoId() {
        return idFideicomisoId;
    }

    public void setIdFideicomisoId(LongFilter idFideicomisoId) {
        this.idFideicomisoId = idFideicomisoId;
    }

    public LongFilter getIdComercialId() {
        return idComercialId;
    }

    public void setIdComercialId(LongFilter idComercialId) {
        this.idComercialId = idComercialId;
    }

    public LongFilter getIdPerfilAhorroId() {
        return idPerfilAhorroId;
    }

    public void setIdPerfilAhorroId(LongFilter idPerfilAhorroId) {
        this.idPerfilAhorroId = idPerfilAhorroId;
    }

    public LongFilter getIdPerfilComercialId() {
        return idPerfilComercialId;
    }

    public void setIdPerfilComercialId(LongFilter idPerfilComercialId) {
        this.idPerfilComercialId = idPerfilComercialId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DesercionCriteria that = (DesercionCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(fecha, that.fecha) &&
            Objects.equals(edad, that.edad) &&
            Objects.equals(nroPromedioRetiros, that.nroPromedioRetiros) &&
            Objects.equals(nroPromedioAportes, that.nroPromedioAportes) &&
            Objects.equals(netoPromMovimientos, that.netoPromMovimientos) &&
            Objects.equals(diasUltimoMov, that.diasUltimoMov) &&
            Objects.equals(posibleSul, that.posibleSul) &&
            Objects.equals(idClienteId, that.idClienteId) &&
            Objects.equals(idFondoId, that.idFondoId) &&
            Objects.equals(idFideicomisoId, that.idFideicomisoId) &&
            Objects.equals(idComercialId, that.idComercialId) &&
            Objects.equals(idPerfilAhorroId, that.idPerfilAhorroId) &&
            Objects.equals(idPerfilComercialId, that.idPerfilComercialId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        fecha,
        edad,
        nroPromedioRetiros,
        nroPromedioAportes,
        netoPromMovimientos,
        diasUltimoMov,
        posibleSul,
        idClienteId,
        idFondoId,
        idFideicomisoId,
        idComercialId,
        idPerfilAhorroId,
        idPerfilComercialId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DesercionCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (fecha != null ? "fecha=" + fecha + ", " : "") +
                (edad != null ? "edad=" + edad + ", " : "") +
                (nroPromedioRetiros != null ? "nroPromedioRetiros=" + nroPromedioRetiros + ", " : "") +
                (nroPromedioAportes != null ? "nroPromedioAportes=" + nroPromedioAportes + ", " : "") +
                (netoPromMovimientos != null ? "netoPromMovimientos=" + netoPromMovimientos + ", " : "") +
                (diasUltimoMov != null ? "diasUltimoMov=" + diasUltimoMov + ", " : "") +
                (posibleSul != null ? "posibleSul=" + posibleSul + ", " : "") +
                (idClienteId != null ? "idClienteId=" + idClienteId + ", " : "") +
                (idFondoId != null ? "idFondoId=" + idFondoId + ", " : "") +
                (idFideicomisoId != null ? "idFideicomisoId=" + idFideicomisoId + ", " : "") +
                (idComercialId != null ? "idComercialId=" + idComercialId + ", " : "") +
                (idPerfilAhorroId != null ? "idPerfilAhorroId=" + idPerfilAhorroId + ", " : "") +
                (idPerfilComercialId != null ? "idPerfilComercialId=" + idPerfilComercialId + ", " : "") +
            "}";
    }

}
