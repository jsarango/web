package com.hackaton.core.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.hackaton.core.domain.Fideicomiso} entity. This class is used
 * in {@link com.hackaton.core.web.rest.FideicomisoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /fideicomisos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class FideicomisoCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter fideicomiso;

    private LocalDateFilter fechaConstitucion;

    private LongFilter idFondoId;

    private LongFilter idComercialId;

    private LongFilter idClienteId;

    public FideicomisoCriteria() {
    }

    public FideicomisoCriteria(FideicomisoCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.fideicomiso = other.fideicomiso == null ? null : other.fideicomiso.copy();
        this.fechaConstitucion = other.fechaConstitucion == null ? null : other.fechaConstitucion.copy();
        this.idFondoId = other.idFondoId == null ? null : other.idFondoId.copy();
        this.idComercialId = other.idComercialId == null ? null : other.idComercialId.copy();
        this.idClienteId = other.idClienteId == null ? null : other.idClienteId.copy();
    }

    @Override
    public FideicomisoCriteria copy() {
        return new FideicomisoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getFideicomiso() {
        return fideicomiso;
    }

    public void setFideicomiso(StringFilter fideicomiso) {
        this.fideicomiso = fideicomiso;
    }

    public LocalDateFilter getFechaConstitucion() {
        return fechaConstitucion;
    }

    public void setFechaConstitucion(LocalDateFilter fechaConstitucion) {
        this.fechaConstitucion = fechaConstitucion;
    }

    public LongFilter getIdFondoId() {
        return idFondoId;
    }

    public void setIdFondoId(LongFilter idFondoId) {
        this.idFondoId = idFondoId;
    }

    public LongFilter getIdComercialId() {
        return idComercialId;
    }

    public void setIdComercialId(LongFilter idComercialId) {
        this.idComercialId = idComercialId;
    }

    public LongFilter getIdClienteId() {
        return idClienteId;
    }

    public void setIdClienteId(LongFilter idClienteId) {
        this.idClienteId = idClienteId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FideicomisoCriteria that = (FideicomisoCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(fideicomiso, that.fideicomiso) &&
            Objects.equals(fechaConstitucion, that.fechaConstitucion) &&
            Objects.equals(idFondoId, that.idFondoId) &&
            Objects.equals(idComercialId, that.idComercialId) &&
            Objects.equals(idClienteId, that.idClienteId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        fideicomiso,
        fechaConstitucion,
        idFondoId,
        idComercialId,
        idClienteId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FideicomisoCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (fideicomiso != null ? "fideicomiso=" + fideicomiso + ", " : "") +
                (fechaConstitucion != null ? "fechaConstitucion=" + fechaConstitucion + ", " : "") +
                (idFondoId != null ? "idFondoId=" + idFondoId + ", " : "") +
                (idComercialId != null ? "idComercialId=" + idComercialId + ", " : "") +
                (idClienteId != null ? "idClienteId=" + idClienteId + ", " : "") +
            "}";
    }

}
