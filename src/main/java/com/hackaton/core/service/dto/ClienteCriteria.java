package com.hackaton.core.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.hackaton.core.domain.enumeration.TipoCliente;
import com.hackaton.core.domain.enumeration.TipoIdentificacion;
import com.hackaton.core.domain.enumeration.Sexo;
import com.hackaton.core.domain.enumeration.EstadoCivil;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.hackaton.core.domain.Cliente} entity. This class is used
 * in {@link com.hackaton.core.web.rest.ClienteResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /clientes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ClienteCriteria implements Serializable, Criteria {
    /**
     * Class for filtering TipoCliente
     */
    public static class TipoClienteFilter extends Filter<TipoCliente> {

        public TipoClienteFilter() {
        }

        public TipoClienteFilter(TipoClienteFilter filter) {
            super(filter);
        }

        @Override
        public TipoClienteFilter copy() {
            return new TipoClienteFilter(this);
        }

    }
    /**
     * Class for filtering TipoIdentificacion
     */
    public static class TipoIdentificacionFilter extends Filter<TipoIdentificacion> {

        public TipoIdentificacionFilter() {
        }

        public TipoIdentificacionFilter(TipoIdentificacionFilter filter) {
            super(filter);
        }

        @Override
        public TipoIdentificacionFilter copy() {
            return new TipoIdentificacionFilter(this);
        }

    }
    /**
     * Class for filtering Sexo
     */
    public static class SexoFilter extends Filter<Sexo> {

        public SexoFilter() {
        }

        public SexoFilter(SexoFilter filter) {
            super(filter);
        }

        @Override
        public SexoFilter copy() {
            return new SexoFilter(this);
        }

    }
    /**
     * Class for filtering EstadoCivil
     */
    public static class EstadoCivilFilter extends Filter<EstadoCivil> {

        public EstadoCivilFilter() {
        }

        public EstadoCivilFilter(EstadoCivilFilter filter) {
            super(filter);
        }

        @Override
        public EstadoCivilFilter copy() {
            return new EstadoCivilFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private TipoClienteFilter tipo;

    private TipoIdentificacionFilter tipoIdentificacion;

    private StringFilter nombre;

    private LocalDateFilter fechaNacimiento;

    private SexoFilter sexo;

    private EstadoCivilFilter estadoCivil;

    private LocalDateFilter fechaIngreso;

    private BooleanFilter resideColombia;

    private BooleanFilter declarante;

    public ClienteCriteria() {
    }

    public ClienteCriteria(ClienteCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.tipo = other.tipo == null ? null : other.tipo.copy();
        this.tipoIdentificacion = other.tipoIdentificacion == null ? null : other.tipoIdentificacion.copy();
        this.nombre = other.nombre == null ? null : other.nombre.copy();
        this.fechaNacimiento = other.fechaNacimiento == null ? null : other.fechaNacimiento.copy();
        this.sexo = other.sexo == null ? null : other.sexo.copy();
        this.estadoCivil = other.estadoCivil == null ? null : other.estadoCivil.copy();
        this.fechaIngreso = other.fechaIngreso == null ? null : other.fechaIngreso.copy();
        this.resideColombia = other.resideColombia == null ? null : other.resideColombia.copy();
        this.declarante = other.declarante == null ? null : other.declarante.copy();
    }

    @Override
    public ClienteCriteria copy() {
        return new ClienteCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public TipoClienteFilter getTipo() {
        return tipo;
    }

    public void setTipo(TipoClienteFilter tipo) {
        this.tipo = tipo;
    }

    public TipoIdentificacionFilter getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(TipoIdentificacionFilter tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public StringFilter getNombre() {
        return nombre;
    }

    public void setNombre(StringFilter nombre) {
        this.nombre = nombre;
    }

    public LocalDateFilter getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDateFilter fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public SexoFilter getSexo() {
        return sexo;
    }

    public void setSexo(SexoFilter sexo) {
        this.sexo = sexo;
    }

    public EstadoCivilFilter getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(EstadoCivilFilter estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public LocalDateFilter getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(LocalDateFilter fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public BooleanFilter getResideColombia() {
        return resideColombia;
    }

    public void setResideColombia(BooleanFilter resideColombia) {
        this.resideColombia = resideColombia;
    }

    public BooleanFilter getDeclarante() {
        return declarante;
    }

    public void setDeclarante(BooleanFilter declarante) {
        this.declarante = declarante;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ClienteCriteria that = (ClienteCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(tipo, that.tipo) &&
            Objects.equals(tipoIdentificacion, that.tipoIdentificacion) &&
            Objects.equals(nombre, that.nombre) &&
            Objects.equals(fechaNacimiento, that.fechaNacimiento) &&
            Objects.equals(sexo, that.sexo) &&
            Objects.equals(estadoCivil, that.estadoCivil) &&
            Objects.equals(fechaIngreso, that.fechaIngreso) &&
            Objects.equals(resideColombia, that.resideColombia) &&
            Objects.equals(declarante, that.declarante);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        tipo,
        tipoIdentificacion,
        nombre,
        fechaNacimiento,
        sexo,
        estadoCivil,
        fechaIngreso,
        resideColombia,
        declarante
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ClienteCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (tipo != null ? "tipo=" + tipo + ", " : "") +
                (tipoIdentificacion != null ? "tipoIdentificacion=" + tipoIdentificacion + ", " : "") +
                (nombre != null ? "nombre=" + nombre + ", " : "") +
                (fechaNacimiento != null ? "fechaNacimiento=" + fechaNacimiento + ", " : "") +
                (sexo != null ? "sexo=" + sexo + ", " : "") +
                (estadoCivil != null ? "estadoCivil=" + estadoCivil + ", " : "") +
                (fechaIngreso != null ? "fechaIngreso=" + fechaIngreso + ", " : "") +
                (resideColombia != null ? "resideColombia=" + resideColombia + ", " : "") +
                (declarante != null ? "declarante=" + declarante + ", " : "") +
            "}";
    }

}
