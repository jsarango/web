package com.hackaton.core.service;

import com.hackaton.core.domain.PerfilAhorro;
import com.hackaton.core.repository.PerfilAhorroRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link PerfilAhorro}.
 */
@Service
@Transactional
public class PerfilAhorroService {

    private final Logger log = LoggerFactory.getLogger(PerfilAhorroService.class);

    private final PerfilAhorroRepository perfilAhorroRepository;

    public PerfilAhorroService(PerfilAhorroRepository perfilAhorroRepository) {
        this.perfilAhorroRepository = perfilAhorroRepository;
    }

    /**
     * Save a perfilAhorro.
     *
     * @param perfilAhorro the entity to save.
     * @return the persisted entity.
     */
    public PerfilAhorro save(PerfilAhorro perfilAhorro) {
        log.debug("Request to save PerfilAhorro : {}", perfilAhorro);
        return perfilAhorroRepository.save(perfilAhorro);
    }

    /**
     * Get all the perfilAhorros.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PerfilAhorro> findAll() {
        log.debug("Request to get all PerfilAhorros");
        return perfilAhorroRepository.findAll();
    }


    /**
     * Get one perfilAhorro by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PerfilAhorro> findOne(Long id) {
        log.debug("Request to get PerfilAhorro : {}", id);
        return perfilAhorroRepository.findById(id);
    }

    /**
     * Delete the perfilAhorro by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PerfilAhorro : {}", id);
        perfilAhorroRepository.deleteById(id);
    }
}
