package com.hackaton.core.service;

import com.hackaton.core.domain.Fondo;
import com.hackaton.core.repository.FondoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Fondo}.
 */
@Service
@Transactional
public class FondoService {

    private final Logger log = LoggerFactory.getLogger(FondoService.class);

    private final FondoRepository fondoRepository;

    public FondoService(FondoRepository fondoRepository) {
        this.fondoRepository = fondoRepository;
    }

    /**
     * Save a fondo.
     *
     * @param fondo the entity to save.
     * @return the persisted entity.
     */
    public Fondo save(Fondo fondo) {
        log.debug("Request to save Fondo : {}", fondo);
        return fondoRepository.save(fondo);
    }

    /**
     * Get all the fondos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Fondo> findAll() {
        log.debug("Request to get all Fondos");
        return fondoRepository.findAll();
    }


    /**
     * Get one fondo by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Fondo> findOne(Long id) {
        log.debug("Request to get Fondo : {}", id);
        return fondoRepository.findById(id);
    }

    /**
     * Delete the fondo by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Fondo : {}", id);
        fondoRepository.deleteById(id);
    }
}
