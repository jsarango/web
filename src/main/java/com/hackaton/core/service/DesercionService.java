package com.hackaton.core.service;

import com.hackaton.core.domain.Desercion;
import com.hackaton.core.repository.DesercionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Desercion}.
 */
@Service
@Transactional
public class DesercionService {

    private final Logger log = LoggerFactory.getLogger(DesercionService.class);

    private final DesercionRepository desercionRepository;

    public DesercionService(DesercionRepository desercionRepository) {
        this.desercionRepository = desercionRepository;
    }

    /**
     * Save a desercion.
     *
     * @param desercion the entity to save.
     * @return the persisted entity.
     */
    public Desercion save(Desercion desercion) {
        log.debug("Request to save Desercion : {}", desercion);
        return desercionRepository.save(desercion);
    }

    /**
     * Get all the desercions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Desercion> findAll(Pageable pageable) {
        log.debug("Request to get all Desercions");
        return desercionRepository.findAll(pageable);
    }


    /**
     * Get one desercion by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Desercion> findOne(Long id) {
        log.debug("Request to get Desercion : {}", id);
        return desercionRepository.findById(id);
    }

    /**
     * Delete the desercion by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Desercion : {}", id);
        desercionRepository.deleteById(id);
    }
}
