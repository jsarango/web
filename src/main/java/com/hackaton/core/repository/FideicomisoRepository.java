package com.hackaton.core.repository;

import com.hackaton.core.domain.Fideicomiso;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Fideicomiso entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FideicomisoRepository extends JpaRepository<Fideicomiso, Long>, JpaSpecificationExecutor<Fideicomiso> {
}
