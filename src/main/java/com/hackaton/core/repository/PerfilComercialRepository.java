package com.hackaton.core.repository;

import com.hackaton.core.domain.PerfilComercial;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the PerfilComercial entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PerfilComercialRepository extends JpaRepository<PerfilComercial, Long> {
}
