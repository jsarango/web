package com.hackaton.core.repository;

import com.hackaton.core.domain.Desercion;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Desercion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DesercionRepository extends JpaRepository<Desercion, Long>, JpaSpecificationExecutor<Desercion> {
}
