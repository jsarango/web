package com.hackaton.core.repository;

import com.hackaton.core.domain.Fondo;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Fondo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FondoRepository extends JpaRepository<Fondo, Long> {
}
