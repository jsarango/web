package com.hackaton.core.repository;

import com.hackaton.core.domain.PerfilAhorro;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the PerfilAhorro entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PerfilAhorroRepository extends JpaRepository<PerfilAhorro, Long> {
}
