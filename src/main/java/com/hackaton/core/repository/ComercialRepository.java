package com.hackaton.core.repository;

import com.hackaton.core.domain.Comercial;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Comercial entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ComercialRepository extends JpaRepository<Comercial, Long> {
}
