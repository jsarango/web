export interface IPerfilComercial {
  id?: number;
  descripcion?: string;
  saldoMinimo?: number;
  saldoMaximo?: number;
}

export const defaultValue: Readonly<IPerfilComercial> = {};
