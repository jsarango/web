export interface IComercial {
  id?: number;
  codigo?: string;
  nombre?: string;
}

export const defaultValue: Readonly<IComercial> = {};
