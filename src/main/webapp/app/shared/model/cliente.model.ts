import { Moment } from 'moment';
import { TipoCliente } from 'app/shared/model/enumerations/tipo-cliente.model';
import { TipoIdentificacion } from 'app/shared/model/enumerations/tipo-identificacion.model';
import { Sexo } from 'app/shared/model/enumerations/sexo.model';
import { EstadoCivil } from 'app/shared/model/enumerations/estado-civil.model';

export interface ICliente {
  id?: number;
  tipo?: TipoCliente;
  tipoIdentificacion?: TipoIdentificacion;
  nombre?: string;
  fechaNacimiento?: string;
  sexo?: Sexo;
  estadoCivil?: EstadoCivil;
  fechaIngreso?: string;
  resideColombia?: boolean;
  declarante?: boolean;
}

export const defaultValue: Readonly<ICliente> = {
  resideColombia: false,
  declarante: false,
};
