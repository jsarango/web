import { Moment } from 'moment';

export interface IFondo {
  id?: number;
  codigo?: string;
  nit?: string;
  nombreCorto?: string;
  nombreLargo?: string;
  fechaFondo?: string;
}

export const defaultValue: Readonly<IFondo> = {};
