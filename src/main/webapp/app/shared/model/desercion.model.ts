import { Moment } from 'moment';
import { ICliente } from 'app/shared/model/cliente.model';
import { IFondo } from 'app/shared/model/fondo.model';
import { IFideicomiso } from 'app/shared/model/fideicomiso.model';
import { IComercial } from 'app/shared/model/comercial.model';
import { IPerfilAhorro } from 'app/shared/model/perfil-ahorro.model';
import { IPerfilComercial } from 'app/shared/model/perfil-comercial.model';

export interface IDesercion {
  id?: number;
  fecha?: string;
  edad?: string;
  nroPromedioRetiros?: number;
  nroPromedioAportes?: number;
  netoPromMovimientos?: number;
  diasUltimoMov?: number;
  posibleSul?: boolean;
  idCliente?: ICliente;
  idFondo?: IFondo;
  idFideicomiso?: IFideicomiso;
  idComercial?: IComercial;
  idPerfilAhorro?: IPerfilAhorro;
  idPerfilComercial?: IPerfilComercial;
}

export const defaultValue: Readonly<IDesercion> = {
  posibleSul: false,
};
