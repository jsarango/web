import { Moment } from 'moment';
import { IFondo } from 'app/shared/model/fondo.model';
import { IComercial } from 'app/shared/model/comercial.model';
import { ICliente } from 'app/shared/model/cliente.model';

export interface IFideicomiso {
  id?: number;
  fideicomiso?: string;
  fechaConstitucion?: string;
  idFondo?: IFondo;
  idComercial?: IComercial;
  idCliente?: ICliente;
}

export const defaultValue: Readonly<IFideicomiso> = {};
