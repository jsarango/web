export interface IPerfilAhorro {
  id?: number;
  codigo?: string;
  descripcion?: string;
}

export const defaultValue: Readonly<IPerfilAhorro> = {};
