export const enum TipoIdentificacion {
  C = 'C',

  N = 'N',

  CE = 'CE',

  TI = 'TI',

  RC = 'RC',
}
