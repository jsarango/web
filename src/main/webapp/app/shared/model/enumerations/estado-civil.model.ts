export const enum EstadoCivil {
  S = 'S',

  C = 'C',

  D = 'D',

  UL = 'UL',

  V = 'V',
}
