import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
// prettier-ignore
import fondo, {
  FondoState
} from 'app/entities/fondo/fondo.reducer';
// prettier-ignore
import comercial, {
  ComercialState
} from 'app/entities/comercial/comercial.reducer';
// prettier-ignore
import cliente, {
  ClienteState
} from 'app/entities/cliente/cliente.reducer';
// prettier-ignore
import fideicomiso, {
  FideicomisoState
} from 'app/entities/fideicomiso/fideicomiso.reducer';
// prettier-ignore
import perfilComercial, {
  PerfilComercialState
} from 'app/entities/perfil-comercial/perfil-comercial.reducer';
// prettier-ignore
import perfilAhorro, {
  PerfilAhorroState
} from 'app/entities/perfil-ahorro/perfil-ahorro.reducer';
// prettier-ignore
import desercion, {
  DesercionState
} from 'app/entities/desercion/desercion.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly fondo: FondoState;
  readonly comercial: ComercialState;
  readonly cliente: ClienteState;
  readonly fideicomiso: FideicomisoState;
  readonly perfilComercial: PerfilComercialState;
  readonly perfilAhorro: PerfilAhorroState;
  readonly desercion: DesercionState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  applicationProfile,
  administration,
  fondo,
  comercial,
  cliente,
  fideicomiso,
  perfilComercial,
  perfilAhorro,
  desercion,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar,
});

export default rootReducer;
