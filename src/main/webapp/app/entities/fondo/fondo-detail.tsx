import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './fondo.reducer';
import { IFondo } from 'app/shared/model/fondo.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IFondoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const FondoDetail = (props: IFondoDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { fondoEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Fondo [<b>{fondoEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="codigo">Codigo</span>
          </dt>
          <dd>{fondoEntity.codigo}</dd>
          <dt>
            <span id="nit">Nit</span>
          </dt>
          <dd>{fondoEntity.nit}</dd>
          <dt>
            <span id="nombreCorto">Nombre Corto</span>
          </dt>
          <dd>{fondoEntity.nombreCorto}</dd>
          <dt>
            <span id="nombreLargo">Nombre Largo</span>
          </dt>
          <dd>{fondoEntity.nombreLargo}</dd>
          <dt>
            <span id="fechaFondo">Fecha Fondo</span>
          </dt>
          <dd>
            {fondoEntity.fechaFondo ? <TextFormat value={fondoEntity.fechaFondo} type="date" format={APP_LOCAL_DATE_FORMAT} /> : null}
          </dd>
        </dl>
        <Button tag={Link} to="/fondo" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/fondo/${fondoEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ fondo }: IRootState) => ({
  fondoEntity: fondo.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(FondoDetail);
