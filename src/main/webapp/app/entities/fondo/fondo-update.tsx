import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './fondo.reducer';
import { IFondo } from 'app/shared/model/fondo.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IFondoUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const FondoUpdate = (props: IFondoUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { fondoEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/fondo');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...fondoEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="frontApp.fondo.home.createOrEditLabel">Create or edit a Fondo</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : fondoEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="fondo-id">ID</Label>
                  <AvInput id="fondo-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="codigoLabel" for="fondo-codigo">
                  Codigo
                </Label>
                <AvField
                  id="fondo-codigo"
                  type="text"
                  name="codigo"
                  validate={{
                    minLength: { value: 3, errorMessage: 'This field is required to be at least 3 characters.' },
                    maxLength: { value: 5, errorMessage: 'This field cannot be longer than 5 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="nitLabel" for="fondo-nit">
                  Nit
                </Label>
                <AvField
                  id="fondo-nit"
                  type="text"
                  name="nit"
                  validate={{
                    minLength: { value: 6, errorMessage: 'This field is required to be at least 6 characters.' },
                    maxLength: { value: 15, errorMessage: 'This field cannot be longer than 15 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="nombreCortoLabel" for="fondo-nombreCorto">
                  Nombre Corto
                </Label>
                <AvField id="fondo-nombreCorto" type="text" name="nombreCorto" />
              </AvGroup>
              <AvGroup>
                <Label id="nombreLargoLabel" for="fondo-nombreLargo">
                  Nombre Largo
                </Label>
                <AvField id="fondo-nombreLargo" type="text" name="nombreLargo" />
              </AvGroup>
              <AvGroup>
                <Label id="fechaFondoLabel" for="fondo-fechaFondo">
                  Fecha Fondo
                </Label>
                <AvField id="fondo-fechaFondo" type="date" className="form-control" name="fechaFondo" />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/fondo" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  fondoEntity: storeState.fondo.entity,
  loading: storeState.fondo.loading,
  updating: storeState.fondo.updating,
  updateSuccess: storeState.fondo.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(FondoUpdate);
