import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IFondo, defaultValue } from 'app/shared/model/fondo.model';

export const ACTION_TYPES = {
  FETCH_FONDO_LIST: 'fondo/FETCH_FONDO_LIST',
  FETCH_FONDO: 'fondo/FETCH_FONDO',
  CREATE_FONDO: 'fondo/CREATE_FONDO',
  UPDATE_FONDO: 'fondo/UPDATE_FONDO',
  DELETE_FONDO: 'fondo/DELETE_FONDO',
  RESET: 'fondo/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IFondo>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type FondoState = Readonly<typeof initialState>;

// Reducer

export default (state: FondoState = initialState, action): FondoState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_FONDO_LIST):
    case REQUEST(ACTION_TYPES.FETCH_FONDO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_FONDO):
    case REQUEST(ACTION_TYPES.UPDATE_FONDO):
    case REQUEST(ACTION_TYPES.DELETE_FONDO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_FONDO_LIST):
    case FAILURE(ACTION_TYPES.FETCH_FONDO):
    case FAILURE(ACTION_TYPES.CREATE_FONDO):
    case FAILURE(ACTION_TYPES.UPDATE_FONDO):
    case FAILURE(ACTION_TYPES.DELETE_FONDO):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_FONDO_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_FONDO):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_FONDO):
    case SUCCESS(ACTION_TYPES.UPDATE_FONDO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_FONDO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/fondos';

// Actions

export const getEntities: ICrudGetAllAction<IFondo> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_FONDO_LIST,
  payload: axios.get<IFondo>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IFondo> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_FONDO,
    payload: axios.get<IFondo>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IFondo> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_FONDO,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IFondo> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_FONDO,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IFondo> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_FONDO,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
