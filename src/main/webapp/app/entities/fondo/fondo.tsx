import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './fondo.reducer';
import { IFondo } from 'app/shared/model/fondo.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IFondoProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Fondo = (props: IFondoProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { fondoList, match, loading } = props;
  return (
    <div>
      <h2 id="fondo-heading">
        Fondos
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Fondo
        </Link>
      </h2>
      <div className="table-responsive">
        {fondoList && fondoList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Codigo</th>
                <th>Nit</th>
                <th>Nombre Corto</th>
                <th>Nombre Largo</th>
                <th>Fecha Fondo</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {fondoList.map((fondo, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${fondo.id}`} color="link" size="sm">
                      {fondo.id}
                    </Button>
                  </td>
                  <td>{fondo.codigo}</td>
                  <td>{fondo.nit}</td>
                  <td>{fondo.nombreCorto}</td>
                  <td>{fondo.nombreLargo}</td>
                  <td>{fondo.fechaFondo ? <TextFormat type="date" value={fondo.fechaFondo} format={APP_LOCAL_DATE_FORMAT} /> : null}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${fondo.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${fondo.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${fondo.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Fondos found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ fondo }: IRootState) => ({
  fondoList: fondo.entities,
  loading: fondo.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Fondo);
