import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Fondo from './fondo';
import FondoDetail from './fondo-detail';
import FondoUpdate from './fondo-update';
import FondoDeleteDialog from './fondo-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={FondoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={FondoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={FondoDetail} />
      <ErrorBoundaryRoute path={match.url} component={Fondo} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={FondoDeleteDialog} />
  </>
);

export default Routes;
