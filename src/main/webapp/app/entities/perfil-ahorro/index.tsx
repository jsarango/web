import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PerfilAhorro from './perfil-ahorro';
import PerfilAhorroDetail from './perfil-ahorro-detail';
import PerfilAhorroUpdate from './perfil-ahorro-update';
import PerfilAhorroDeleteDialog from './perfil-ahorro-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PerfilAhorroUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PerfilAhorroUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PerfilAhorroDetail} />
      <ErrorBoundaryRoute path={match.url} component={PerfilAhorro} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={PerfilAhorroDeleteDialog} />
  </>
);

export default Routes;
