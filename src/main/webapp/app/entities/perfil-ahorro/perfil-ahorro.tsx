import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './perfil-ahorro.reducer';
import { IPerfilAhorro } from 'app/shared/model/perfil-ahorro.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPerfilAhorroProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const PerfilAhorro = (props: IPerfilAhorroProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { perfilAhorroList, match, loading } = props;
  return (
    <div>
      <h2 id="perfil-ahorro-heading">
        Perfil Ahorros
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Perfil Ahorro
        </Link>
      </h2>
      <div className="table-responsive">
        {perfilAhorroList && perfilAhorroList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Codigo</th>
                <th>Descripcion</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {perfilAhorroList.map((perfilAhorro, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${perfilAhorro.id}`} color="link" size="sm">
                      {perfilAhorro.id}
                    </Button>
                  </td>
                  <td>{perfilAhorro.codigo}</td>
                  <td>{perfilAhorro.descripcion}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${perfilAhorro.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${perfilAhorro.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${perfilAhorro.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Perfil Ahorros found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ perfilAhorro }: IRootState) => ({
  perfilAhorroList: perfilAhorro.entities,
  loading: perfilAhorro.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PerfilAhorro);
