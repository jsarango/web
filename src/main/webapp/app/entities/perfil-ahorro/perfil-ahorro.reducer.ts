import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPerfilAhorro, defaultValue } from 'app/shared/model/perfil-ahorro.model';

export const ACTION_TYPES = {
  FETCH_PERFILAHORRO_LIST: 'perfilAhorro/FETCH_PERFILAHORRO_LIST',
  FETCH_PERFILAHORRO: 'perfilAhorro/FETCH_PERFILAHORRO',
  CREATE_PERFILAHORRO: 'perfilAhorro/CREATE_PERFILAHORRO',
  UPDATE_PERFILAHORRO: 'perfilAhorro/UPDATE_PERFILAHORRO',
  DELETE_PERFILAHORRO: 'perfilAhorro/DELETE_PERFILAHORRO',
  RESET: 'perfilAhorro/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPerfilAhorro>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type PerfilAhorroState = Readonly<typeof initialState>;

// Reducer

export default (state: PerfilAhorroState = initialState, action): PerfilAhorroState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PERFILAHORRO_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PERFILAHORRO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_PERFILAHORRO):
    case REQUEST(ACTION_TYPES.UPDATE_PERFILAHORRO):
    case REQUEST(ACTION_TYPES.DELETE_PERFILAHORRO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PERFILAHORRO_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PERFILAHORRO):
    case FAILURE(ACTION_TYPES.CREATE_PERFILAHORRO):
    case FAILURE(ACTION_TYPES.UPDATE_PERFILAHORRO):
    case FAILURE(ACTION_TYPES.DELETE_PERFILAHORRO):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PERFILAHORRO_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PERFILAHORRO):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_PERFILAHORRO):
    case SUCCESS(ACTION_TYPES.UPDATE_PERFILAHORRO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_PERFILAHORRO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/perfil-ahorros';

// Actions

export const getEntities: ICrudGetAllAction<IPerfilAhorro> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PERFILAHORRO_LIST,
  payload: axios.get<IPerfilAhorro>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IPerfilAhorro> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PERFILAHORRO,
    payload: axios.get<IPerfilAhorro>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IPerfilAhorro> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PERFILAHORRO,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPerfilAhorro> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PERFILAHORRO,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPerfilAhorro> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PERFILAHORRO,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
