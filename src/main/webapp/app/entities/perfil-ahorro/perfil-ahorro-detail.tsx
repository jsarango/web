import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './perfil-ahorro.reducer';
import { IPerfilAhorro } from 'app/shared/model/perfil-ahorro.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPerfilAhorroDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PerfilAhorroDetail = (props: IPerfilAhorroDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { perfilAhorroEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          PerfilAhorro [<b>{perfilAhorroEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="codigo">Codigo</span>
          </dt>
          <dd>{perfilAhorroEntity.codigo}</dd>
          <dt>
            <span id="descripcion">Descripcion</span>
          </dt>
          <dd>{perfilAhorroEntity.descripcion}</dd>
        </dl>
        <Button tag={Link} to="/perfil-ahorro" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/perfil-ahorro/${perfilAhorroEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ perfilAhorro }: IRootState) => ({
  perfilAhorroEntity: perfilAhorro.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PerfilAhorroDetail);
