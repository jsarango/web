import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Comercial from './comercial';
import ComercialDetail from './comercial-detail';
import ComercialUpdate from './comercial-update';
import ComercialDeleteDialog from './comercial-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ComercialUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ComercialUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ComercialDetail} />
      <ErrorBoundaryRoute path={match.url} component={Comercial} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ComercialDeleteDialog} />
  </>
);

export default Routes;
