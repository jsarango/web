import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IComercial, defaultValue } from 'app/shared/model/comercial.model';

export const ACTION_TYPES = {
  FETCH_COMERCIAL_LIST: 'comercial/FETCH_COMERCIAL_LIST',
  FETCH_COMERCIAL: 'comercial/FETCH_COMERCIAL',
  CREATE_COMERCIAL: 'comercial/CREATE_COMERCIAL',
  UPDATE_COMERCIAL: 'comercial/UPDATE_COMERCIAL',
  DELETE_COMERCIAL: 'comercial/DELETE_COMERCIAL',
  RESET: 'comercial/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IComercial>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type ComercialState = Readonly<typeof initialState>;

// Reducer

export default (state: ComercialState = initialState, action): ComercialState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_COMERCIAL_LIST):
    case REQUEST(ACTION_TYPES.FETCH_COMERCIAL):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_COMERCIAL):
    case REQUEST(ACTION_TYPES.UPDATE_COMERCIAL):
    case REQUEST(ACTION_TYPES.DELETE_COMERCIAL):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_COMERCIAL_LIST):
    case FAILURE(ACTION_TYPES.FETCH_COMERCIAL):
    case FAILURE(ACTION_TYPES.CREATE_COMERCIAL):
    case FAILURE(ACTION_TYPES.UPDATE_COMERCIAL):
    case FAILURE(ACTION_TYPES.DELETE_COMERCIAL):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_COMERCIAL_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_COMERCIAL):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_COMERCIAL):
    case SUCCESS(ACTION_TYPES.UPDATE_COMERCIAL):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_COMERCIAL):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/comercials';

// Actions

export const getEntities: ICrudGetAllAction<IComercial> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_COMERCIAL_LIST,
  payload: axios.get<IComercial>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IComercial> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_COMERCIAL,
    payload: axios.get<IComercial>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IComercial> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_COMERCIAL,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IComercial> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_COMERCIAL,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IComercial> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_COMERCIAL,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
