import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './comercial.reducer';
import { IComercial } from 'app/shared/model/comercial.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IComercialUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ComercialUpdate = (props: IComercialUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { comercialEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/comercial');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...comercialEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="frontApp.comercial.home.createOrEditLabel">Create or edit a Comercial</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : comercialEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="comercial-id">ID</Label>
                  <AvInput id="comercial-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="codigoLabel" for="comercial-codigo">
                  Codigo
                </Label>
                <AvField
                  id="comercial-codigo"
                  type="text"
                  name="codigo"
                  validate={{
                    minLength: { value: 3, errorMessage: 'This field is required to be at least 3 characters.' },
                    maxLength: { value: 5, errorMessage: 'This field cannot be longer than 5 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="nombreLabel" for="comercial-nombre">
                  Nombre
                </Label>
                <AvField id="comercial-nombre" type="text" name="nombre" />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/comercial" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  comercialEntity: storeState.comercial.entity,
  loading: storeState.comercial.loading,
  updating: storeState.comercial.updating,
  updateSuccess: storeState.comercial.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ComercialUpdate);
