import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './cliente.reducer';
import { ICliente } from 'app/shared/model/cliente.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IClienteDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ClienteDetail = (props: IClienteDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { clienteEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Cliente [<b>{clienteEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="tipo">Tipo</span>
          </dt>
          <dd>{clienteEntity.tipo}</dd>
          <dt>
            <span id="tipoIdentificacion">Tipo Identificacion</span>
          </dt>
          <dd>{clienteEntity.tipoIdentificacion}</dd>
          <dt>
            <span id="nombre">Nombre</span>
          </dt>
          <dd>{clienteEntity.nombre}</dd>
          <dt>
            <span id="fechaNacimiento">Fecha Nacimiento</span>
          </dt>
          <dd>
            {clienteEntity.fechaNacimiento ? (
              <TextFormat value={clienteEntity.fechaNacimiento} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="sexo">Sexo</span>
          </dt>
          <dd>{clienteEntity.sexo}</dd>
          <dt>
            <span id="estadoCivil">Estado Civil</span>
          </dt>
          <dd>{clienteEntity.estadoCivil}</dd>
          <dt>
            <span id="fechaIngreso">Fecha Ingreso</span>
          </dt>
          <dd>
            {clienteEntity.fechaIngreso ? (
              <TextFormat value={clienteEntity.fechaIngreso} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="resideColombia">Reside Colombia</span>
          </dt>
          <dd>{clienteEntity.resideColombia ? 'true' : 'false'}</dd>
          <dt>
            <span id="declarante">Declarante</span>
          </dt>
          <dd>{clienteEntity.declarante ? 'true' : 'false'}</dd>
        </dl>
        <Button tag={Link} to="/cliente" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/cliente/${clienteEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ cliente }: IRootState) => ({
  clienteEntity: cliente.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ClienteDetail);
