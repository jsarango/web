import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './cliente.reducer';
import { ICliente } from 'app/shared/model/cliente.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IClienteUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ClienteUpdate = (props: IClienteUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { clienteEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/cliente' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...clienteEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="frontApp.cliente.home.createOrEditLabel">Create or edit a Cliente</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : clienteEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="cliente-id">ID</Label>
                  <AvInput id="cliente-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="tipoLabel" for="cliente-tipo">
                  Tipo
                </Label>
                <AvInput id="cliente-tipo" type="select" className="form-control" name="tipo" value={(!isNew && clienteEntity.tipo) || 'N'}>
                  <option value="N">N</option>
                  <option value="J">J</option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label id="tipoIdentificacionLabel" for="cliente-tipoIdentificacion">
                  Tipo Identificacion
                </Label>
                <AvInput
                  id="cliente-tipoIdentificacion"
                  type="select"
                  className="form-control"
                  name="tipoIdentificacion"
                  value={(!isNew && clienteEntity.tipoIdentificacion) || 'C'}
                >
                  <option value="C">C</option>
                  <option value="N">N</option>
                  <option value="CE">CE</option>
                  <option value="TI">TI</option>
                  <option value="RC">RC</option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label id="nombreLabel" for="cliente-nombre">
                  Nombre
                </Label>
                <AvField id="cliente-nombre" type="text" name="nombre" />
              </AvGroup>
              <AvGroup>
                <Label id="fechaNacimientoLabel" for="cliente-fechaNacimiento">
                  Fecha Nacimiento
                </Label>
                <AvField id="cliente-fechaNacimiento" type="date" className="form-control" name="fechaNacimiento" />
              </AvGroup>
              <AvGroup>
                <Label id="sexoLabel" for="cliente-sexo">
                  Sexo
                </Label>
                <AvInput id="cliente-sexo" type="select" className="form-control" name="sexo" value={(!isNew && clienteEntity.sexo) || 'M'}>
                  <option value="M">M</option>
                  <option value="F">F</option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label id="estadoCivilLabel" for="cliente-estadoCivil">
                  Estado Civil
                </Label>
                <AvInput
                  id="cliente-estadoCivil"
                  type="select"
                  className="form-control"
                  name="estadoCivil"
                  value={(!isNew && clienteEntity.estadoCivil) || 'S'}
                >
                  <option value="S">S</option>
                  <option value="C">C</option>
                  <option value="D">D</option>
                  <option value="UL">UL</option>
                  <option value="V">V</option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label id="fechaIngresoLabel" for="cliente-fechaIngreso">
                  Fecha Ingreso
                </Label>
                <AvField id="cliente-fechaIngreso" type="date" className="form-control" name="fechaIngreso" />
              </AvGroup>
              <AvGroup check>
                <Label id="resideColombiaLabel">
                  <AvInput id="cliente-resideColombia" type="checkbox" className="form-check-input" name="resideColombia" />
                  Reside Colombia
                </Label>
              </AvGroup>
              <AvGroup check>
                <Label id="declaranteLabel">
                  <AvInput id="cliente-declarante" type="checkbox" className="form-check-input" name="declarante" />
                  Declarante
                </Label>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/cliente" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  clienteEntity: storeState.cliente.entity,
  loading: storeState.cliente.loading,
  updating: storeState.cliente.updating,
  updateSuccess: storeState.cliente.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ClienteUpdate);
