import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './perfil-comercial.reducer';
import { IPerfilComercial } from 'app/shared/model/perfil-comercial.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPerfilComercialProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const PerfilComercial = (props: IPerfilComercialProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { perfilComercialList, match, loading } = props;
  return (
    <div>
      <h2 id="perfil-comercial-heading">
        Perfil Comercials
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Perfil Comercial
        </Link>
      </h2>
      <div className="table-responsive">
        {perfilComercialList && perfilComercialList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Descripcion</th>
                <th>Saldo Minimo</th>
                <th>Saldo Maximo</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {perfilComercialList.map((perfilComercial, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${perfilComercial.id}`} color="link" size="sm">
                      {perfilComercial.id}
                    </Button>
                  </td>
                  <td>{perfilComercial.descripcion}</td>
                  <td>{perfilComercial.saldoMinimo}</td>
                  <td>{perfilComercial.saldoMaximo}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${perfilComercial.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${perfilComercial.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${perfilComercial.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Perfil Comercials found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ perfilComercial }: IRootState) => ({
  perfilComercialList: perfilComercial.entities,
  loading: perfilComercial.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PerfilComercial);
