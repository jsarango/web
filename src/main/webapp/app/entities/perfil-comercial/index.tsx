import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PerfilComercial from './perfil-comercial';
import PerfilComercialDetail from './perfil-comercial-detail';
import PerfilComercialUpdate from './perfil-comercial-update';
import PerfilComercialDeleteDialog from './perfil-comercial-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PerfilComercialUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PerfilComercialUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PerfilComercialDetail} />
      <ErrorBoundaryRoute path={match.url} component={PerfilComercial} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={PerfilComercialDeleteDialog} />
  </>
);

export default Routes;
