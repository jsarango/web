import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPerfilComercial, defaultValue } from 'app/shared/model/perfil-comercial.model';

export const ACTION_TYPES = {
  FETCH_PERFILCOMERCIAL_LIST: 'perfilComercial/FETCH_PERFILCOMERCIAL_LIST',
  FETCH_PERFILCOMERCIAL: 'perfilComercial/FETCH_PERFILCOMERCIAL',
  CREATE_PERFILCOMERCIAL: 'perfilComercial/CREATE_PERFILCOMERCIAL',
  UPDATE_PERFILCOMERCIAL: 'perfilComercial/UPDATE_PERFILCOMERCIAL',
  DELETE_PERFILCOMERCIAL: 'perfilComercial/DELETE_PERFILCOMERCIAL',
  RESET: 'perfilComercial/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPerfilComercial>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type PerfilComercialState = Readonly<typeof initialState>;

// Reducer

export default (state: PerfilComercialState = initialState, action): PerfilComercialState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PERFILCOMERCIAL_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PERFILCOMERCIAL):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_PERFILCOMERCIAL):
    case REQUEST(ACTION_TYPES.UPDATE_PERFILCOMERCIAL):
    case REQUEST(ACTION_TYPES.DELETE_PERFILCOMERCIAL):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PERFILCOMERCIAL_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PERFILCOMERCIAL):
    case FAILURE(ACTION_TYPES.CREATE_PERFILCOMERCIAL):
    case FAILURE(ACTION_TYPES.UPDATE_PERFILCOMERCIAL):
    case FAILURE(ACTION_TYPES.DELETE_PERFILCOMERCIAL):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PERFILCOMERCIAL_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PERFILCOMERCIAL):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_PERFILCOMERCIAL):
    case SUCCESS(ACTION_TYPES.UPDATE_PERFILCOMERCIAL):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_PERFILCOMERCIAL):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/perfil-comercials';

// Actions

export const getEntities: ICrudGetAllAction<IPerfilComercial> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PERFILCOMERCIAL_LIST,
  payload: axios.get<IPerfilComercial>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IPerfilComercial> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PERFILCOMERCIAL,
    payload: axios.get<IPerfilComercial>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IPerfilComercial> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PERFILCOMERCIAL,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPerfilComercial> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PERFILCOMERCIAL,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPerfilComercial> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PERFILCOMERCIAL,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
