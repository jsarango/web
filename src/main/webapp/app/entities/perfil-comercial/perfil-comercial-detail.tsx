import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './perfil-comercial.reducer';
import { IPerfilComercial } from 'app/shared/model/perfil-comercial.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPerfilComercialDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PerfilComercialDetail = (props: IPerfilComercialDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { perfilComercialEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          PerfilComercial [<b>{perfilComercialEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="descripcion">Descripcion</span>
          </dt>
          <dd>{perfilComercialEntity.descripcion}</dd>
          <dt>
            <span id="saldoMinimo">Saldo Minimo</span>
          </dt>
          <dd>{perfilComercialEntity.saldoMinimo}</dd>
          <dt>
            <span id="saldoMaximo">Saldo Maximo</span>
          </dt>
          <dd>{perfilComercialEntity.saldoMaximo}</dd>
        </dl>
        <Button tag={Link} to="/perfil-comercial" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/perfil-comercial/${perfilComercialEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ perfilComercial }: IRootState) => ({
  perfilComercialEntity: perfilComercial.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PerfilComercialDetail);
