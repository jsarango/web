import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './perfil-comercial.reducer';
import { IPerfilComercial } from 'app/shared/model/perfil-comercial.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPerfilComercialUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PerfilComercialUpdate = (props: IPerfilComercialUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { perfilComercialEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/perfil-comercial');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...perfilComercialEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="frontApp.perfilComercial.home.createOrEditLabel">Create or edit a PerfilComercial</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : perfilComercialEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="perfil-comercial-id">ID</Label>
                  <AvInput id="perfil-comercial-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="descripcionLabel" for="perfil-comercial-descripcion">
                  Descripcion
                </Label>
                <AvField id="perfil-comercial-descripcion" type="text" name="descripcion" />
              </AvGroup>
              <AvGroup>
                <Label id="saldoMinimoLabel" for="perfil-comercial-saldoMinimo">
                  Saldo Minimo
                </Label>
                <AvField id="perfil-comercial-saldoMinimo" type="string" className="form-control" name="saldoMinimo" />
              </AvGroup>
              <AvGroup>
                <Label id="saldoMaximoLabel" for="perfil-comercial-saldoMaximo">
                  Saldo Maximo
                </Label>
                <AvField id="perfil-comercial-saldoMaximo" type="string" className="form-control" name="saldoMaximo" />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/perfil-comercial" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  perfilComercialEntity: storeState.perfilComercial.entity,
  loading: storeState.perfilComercial.loading,
  updating: storeState.perfilComercial.updating,
  updateSuccess: storeState.perfilComercial.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PerfilComercialUpdate);
