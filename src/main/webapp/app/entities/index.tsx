import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Fondo from './fondo';
import Comercial from './comercial';
import Cliente from './cliente';
import Fideicomiso from './fideicomiso';
import PerfilComercial from './perfil-comercial';
import PerfilAhorro from './perfil-ahorro';
import Desercion from './desercion';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}fondo`} component={Fondo} />
      <ErrorBoundaryRoute path={`${match.url}comercial`} component={Comercial} />
      <ErrorBoundaryRoute path={`${match.url}cliente`} component={Cliente} />
      <ErrorBoundaryRoute path={`${match.url}fideicomiso`} component={Fideicomiso} />
      <ErrorBoundaryRoute path={`${match.url}perfil-comercial`} component={PerfilComercial} />
      <ErrorBoundaryRoute path={`${match.url}perfil-ahorro`} component={PerfilAhorro} />
      <ErrorBoundaryRoute path={`${match.url}desercion`} component={Desercion} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
