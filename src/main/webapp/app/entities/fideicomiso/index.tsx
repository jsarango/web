import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Fideicomiso from './fideicomiso';
import FideicomisoDetail from './fideicomiso-detail';
import FideicomisoUpdate from './fideicomiso-update';
import FideicomisoDeleteDialog from './fideicomiso-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={FideicomisoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={FideicomisoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={FideicomisoDetail} />
      <ErrorBoundaryRoute path={match.url} component={Fideicomiso} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={FideicomisoDeleteDialog} />
  </>
);

export default Routes;
