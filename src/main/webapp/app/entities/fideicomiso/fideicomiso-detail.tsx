import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './fideicomiso.reducer';
import { IFideicomiso } from 'app/shared/model/fideicomiso.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IFideicomisoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const FideicomisoDetail = (props: IFideicomisoDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { fideicomisoEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Fideicomiso [<b>{fideicomisoEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="fideicomiso">Fideicomiso</span>
          </dt>
          <dd>{fideicomisoEntity.fideicomiso}</dd>
          <dt>
            <span id="fechaConstitucion">Fecha Constitucion</span>
          </dt>
          <dd>
            {fideicomisoEntity.fechaConstitucion ? (
              <TextFormat value={fideicomisoEntity.fechaConstitucion} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>Id Fondo</dt>
          <dd>{fideicomisoEntity.idFondo ? fideicomisoEntity.idFondo.id : ''}</dd>
          <dt>Id Comercial</dt>
          <dd>{fideicomisoEntity.idComercial ? fideicomisoEntity.idComercial.id : ''}</dd>
          <dt>Id Cliente</dt>
          <dd>{fideicomisoEntity.idCliente ? fideicomisoEntity.idCliente.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/fideicomiso" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/fideicomiso/${fideicomisoEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ fideicomiso }: IRootState) => ({
  fideicomisoEntity: fideicomiso.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(FideicomisoDetail);
