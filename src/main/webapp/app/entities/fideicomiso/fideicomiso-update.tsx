import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IFondo } from 'app/shared/model/fondo.model';
import { getEntities as getFondos } from 'app/entities/fondo/fondo.reducer';
import { IComercial } from 'app/shared/model/comercial.model';
import { getEntities as getComercials } from 'app/entities/comercial/comercial.reducer';
import { ICliente } from 'app/shared/model/cliente.model';
import { getEntities as getClientes } from 'app/entities/cliente/cliente.reducer';
import { getEntity, updateEntity, createEntity, reset } from './fideicomiso.reducer';
import { IFideicomiso } from 'app/shared/model/fideicomiso.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IFideicomisoUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const FideicomisoUpdate = (props: IFideicomisoUpdateProps) => {
  const [idFondoId, setIdFondoId] = useState('0');
  const [idComercialId, setIdComercialId] = useState('0');
  const [idClienteId, setIdClienteId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { fideicomisoEntity, fondos, comercials, clientes, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/fideicomiso' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getFondos();
    props.getComercials();
    props.getClientes();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...fideicomisoEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="frontApp.fideicomiso.home.createOrEditLabel">Create or edit a Fideicomiso</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : fideicomisoEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="fideicomiso-id">ID</Label>
                  <AvInput id="fideicomiso-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="fideicomisoLabel" for="fideicomiso-fideicomiso">
                  Fideicomiso
                </Label>
                <AvField
                  id="fideicomiso-fideicomiso"
                  type="text"
                  name="fideicomiso"
                  validate={{
                    minLength: { value: 2, errorMessage: 'This field is required to be at least 2 characters.' },
                    maxLength: { value: 6, errorMessage: 'This field cannot be longer than 6 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="fechaConstitucionLabel" for="fideicomiso-fechaConstitucion">
                  Fecha Constitucion
                </Label>
                <AvField id="fideicomiso-fechaConstitucion" type="date" className="form-control" name="fechaConstitucion" />
              </AvGroup>
              <AvGroup>
                <Label for="fideicomiso-idFondo">Id Fondo</Label>
                <AvInput id="fideicomiso-idFondo" type="select" className="form-control" name="idFondo.id">
                  <option value="" key="0" />
                  {fondos
                    ? fondos.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="fideicomiso-idComercial">Id Comercial</Label>
                <AvInput id="fideicomiso-idComercial" type="select" className="form-control" name="idComercial.id">
                  <option value="" key="0" />
                  {comercials
                    ? comercials.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="fideicomiso-idCliente">Id Cliente</Label>
                <AvInput id="fideicomiso-idCliente" type="select" className="form-control" name="idCliente.id">
                  <option value="" key="0" />
                  {clientes
                    ? clientes.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/fideicomiso" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  fondos: storeState.fondo.entities,
  comercials: storeState.comercial.entities,
  clientes: storeState.cliente.entities,
  fideicomisoEntity: storeState.fideicomiso.entity,
  loading: storeState.fideicomiso.loading,
  updating: storeState.fideicomiso.updating,
  updateSuccess: storeState.fideicomiso.updateSuccess,
});

const mapDispatchToProps = {
  getFondos,
  getComercials,
  getClientes,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(FideicomisoUpdate);
