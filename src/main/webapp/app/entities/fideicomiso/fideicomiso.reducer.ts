import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IFideicomiso, defaultValue } from 'app/shared/model/fideicomiso.model';

export const ACTION_TYPES = {
  FETCH_FIDEICOMISO_LIST: 'fideicomiso/FETCH_FIDEICOMISO_LIST',
  FETCH_FIDEICOMISO: 'fideicomiso/FETCH_FIDEICOMISO',
  CREATE_FIDEICOMISO: 'fideicomiso/CREATE_FIDEICOMISO',
  UPDATE_FIDEICOMISO: 'fideicomiso/UPDATE_FIDEICOMISO',
  DELETE_FIDEICOMISO: 'fideicomiso/DELETE_FIDEICOMISO',
  RESET: 'fideicomiso/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IFideicomiso>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type FideicomisoState = Readonly<typeof initialState>;

// Reducer

export default (state: FideicomisoState = initialState, action): FideicomisoState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_FIDEICOMISO_LIST):
    case REQUEST(ACTION_TYPES.FETCH_FIDEICOMISO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_FIDEICOMISO):
    case REQUEST(ACTION_TYPES.UPDATE_FIDEICOMISO):
    case REQUEST(ACTION_TYPES.DELETE_FIDEICOMISO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_FIDEICOMISO_LIST):
    case FAILURE(ACTION_TYPES.FETCH_FIDEICOMISO):
    case FAILURE(ACTION_TYPES.CREATE_FIDEICOMISO):
    case FAILURE(ACTION_TYPES.UPDATE_FIDEICOMISO):
    case FAILURE(ACTION_TYPES.DELETE_FIDEICOMISO):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_FIDEICOMISO_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_FIDEICOMISO):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_FIDEICOMISO):
    case SUCCESS(ACTION_TYPES.UPDATE_FIDEICOMISO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_FIDEICOMISO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/fideicomisos';

// Actions

export const getEntities: ICrudGetAllAction<IFideicomiso> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_FIDEICOMISO_LIST,
    payload: axios.get<IFideicomiso>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IFideicomiso> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_FIDEICOMISO,
    payload: axios.get<IFideicomiso>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IFideicomiso> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_FIDEICOMISO,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IFideicomiso> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_FIDEICOMISO,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IFideicomiso> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_FIDEICOMISO,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
