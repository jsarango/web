import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './desercion.reducer';
import { IDesercion } from 'app/shared/model/desercion.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IDesercionDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const DesercionDetail = (props: IDesercionDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { desercionEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Desercion [<b>{desercionEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="fecha">Fecha</span>
          </dt>
          <dd>{desercionEntity.fecha ? <TextFormat value={desercionEntity.fecha} type="date" format={APP_LOCAL_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="edad">Edad</span>
          </dt>
          <dd>{desercionEntity.edad}</dd>
          <dt>
            <span id="nroPromedioRetiros">Nro Promedio Retiros</span>
          </dt>
          <dd>{desercionEntity.nroPromedioRetiros}</dd>
          <dt>
            <span id="nroPromedioAportes">Nro Promedio Aportes</span>
          </dt>
          <dd>{desercionEntity.nroPromedioAportes}</dd>
          <dt>
            <span id="netoPromMovimientos">Neto Prom Movimientos</span>
          </dt>
          <dd>{desercionEntity.netoPromMovimientos}</dd>
          <dt>
            <span id="diasUltimoMov">Dias Ultimo Mov</span>
          </dt>
          <dd>{desercionEntity.diasUltimoMov}</dd>
          <dt>
            <span id="posibleSul">Posible Sul</span>
          </dt>
          <dd>{desercionEntity.posibleSul ? 'true' : 'false'}</dd>
          <dt>Id Cliente</dt>
          <dd>{desercionEntity.idCliente ? desercionEntity.idCliente.id : ''}</dd>
          <dt>Id Fondo</dt>
          <dd>{desercionEntity.idFondo ? desercionEntity.idFondo.id : ''}</dd>
          <dt>Id Fideicomiso</dt>
          <dd>{desercionEntity.idFideicomiso ? desercionEntity.idFideicomiso.id : ''}</dd>
          <dt>Id Comercial</dt>
          <dd>{desercionEntity.idComercial ? desercionEntity.idComercial.id : ''}</dd>
          <dt>Id Perfil Ahorro</dt>
          <dd>{desercionEntity.idPerfilAhorro ? desercionEntity.idPerfilAhorro.id : ''}</dd>
          <dt>Id Perfil Comercial</dt>
          <dd>{desercionEntity.idPerfilComercial ? desercionEntity.idPerfilComercial.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/desercion" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/desercion/${desercionEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ desercion }: IRootState) => ({
  desercionEntity: desercion.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DesercionDetail);
