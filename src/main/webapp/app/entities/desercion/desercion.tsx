import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './desercion.reducer';
import { IDesercion } from 'app/shared/model/desercion.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';

export interface IDesercionProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Desercion = (props: IDesercionProps) => {
  const [paginationState, setPaginationState] = useState(
    overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE), props.location.search)
  );

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  const sortEntities = () => {
    getAllEntities();
    const endURL = `?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`;
    if (props.location.search !== endURL) {
      props.history.push(`${props.location.pathname}${endURL}`);
    }
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  useEffect(() => {
    const params = new URLSearchParams(props.location.search);
    const page = params.get('page');
    const sort = params.get('sort');
    if (page && sort) {
      const sortSplit = sort.split(',');
      setPaginationState({
        ...paginationState,
        activePage: +page,
        sort: sortSplit[0],
        order: sortSplit[1],
      });
    }
  }, [props.location.search]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p,
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage,
    });

  const { desercionList, match, loading, totalItems } = props;
  return (
    <div>
      <h2 id="desercion-heading">
        Desercions
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Desercion
        </Link>
      </h2>
      <div className="table-responsive">
        {desercionList && desercionList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={sort('id')}>
                  ID <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('fecha')}>
                  Fecha <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('edad')}>
                  Edad <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('nroPromedioRetiros')}>
                  Nro Promedio Retiros <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('nroPromedioAportes')}>
                  Nro Promedio Aportes <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('netoPromMovimientos')}>
                  Neto Prom Movimientos <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('diasUltimoMov')}>
                  Dias Ultimo Mov <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('posibleSul')}>
                  Posible Sul <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  Id Cliente <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  Id Fondo <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  Id Fideicomiso <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  Id Comercial <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  Id Perfil Ahorro <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  Id Perfil Comercial <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {desercionList.map((desercion, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${desercion.id}`} color="link" size="sm">
                      {desercion.id}
                    </Button>
                  </td>
                  <td>{desercion.fecha ? <TextFormat type="date" value={desercion.fecha} format={APP_LOCAL_DATE_FORMAT} /> : null}</td>
                  <td>{desercion.edad}</td>
                  <td>{desercion.nroPromedioRetiros}</td>
                  <td>{desercion.nroPromedioAportes}</td>
                  <td>{desercion.netoPromMovimientos}</td>
                  <td>{desercion.diasUltimoMov}</td>
                  <td>{desercion.posibleSul ? 'true' : 'false'}</td>
                  <td>{desercion.idCliente ? <Link to={`cliente/${desercion.idCliente.id}`}>{desercion.idCliente.id}</Link> : ''}</td>
                  <td>{desercion.idFondo ? <Link to={`fondo/${desercion.idFondo.id}`}>{desercion.idFondo.id}</Link> : ''}</td>
                  <td>
                    {desercion.idFideicomiso ? (
                      <Link to={`fideicomiso/${desercion.idFideicomiso.id}`}>{desercion.idFideicomiso.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>
                    {desercion.idComercial ? <Link to={`comercial/${desercion.idComercial.id}`}>{desercion.idComercial.id}</Link> : ''}
                  </td>
                  <td>
                    {desercion.idPerfilAhorro ? (
                      <Link to={`perfil-ahorro/${desercion.idPerfilAhorro.id}`}>{desercion.idPerfilAhorro.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>
                    {desercion.idPerfilComercial ? (
                      <Link to={`perfil-comercial/${desercion.idPerfilComercial.id}`}>{desercion.idPerfilComercial.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${desercion.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${desercion.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="primary"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${desercion.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="danger"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Desercions found</div>
        )}
      </div>
      {props.totalItems ? (
        <div className={desercionList && desercionList.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage} />
          </Row>
          <Row className="justify-content-center">
            <JhiPagination
              activePage={paginationState.activePage}
              onSelect={handlePagination}
              maxButtons={5}
              itemsPerPage={paginationState.itemsPerPage}
              totalItems={props.totalItems}
            />
          </Row>
        </div>
      ) : (
        ''
      )}
    </div>
  );
};

const mapStateToProps = ({ desercion }: IRootState) => ({
  desercionList: desercion.entities,
  loading: desercion.loading,
  totalItems: desercion.totalItems,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Desercion);
