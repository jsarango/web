import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Desercion from './desercion';
import DesercionDetail from './desercion-detail';
import DesercionUpdate from './desercion-update';
import DesercionDeleteDialog from './desercion-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={DesercionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={DesercionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={DesercionDetail} />
      <ErrorBoundaryRoute path={match.url} component={Desercion} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={DesercionDeleteDialog} />
  </>
);

export default Routes;
