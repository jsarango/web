import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ICliente } from 'app/shared/model/cliente.model';
import { getEntities as getClientes } from 'app/entities/cliente/cliente.reducer';
import { IFondo } from 'app/shared/model/fondo.model';
import { getEntities as getFondos } from 'app/entities/fondo/fondo.reducer';
import { IFideicomiso } from 'app/shared/model/fideicomiso.model';
import { getEntities as getFideicomisos } from 'app/entities/fideicomiso/fideicomiso.reducer';
import { IComercial } from 'app/shared/model/comercial.model';
import { getEntities as getComercials } from 'app/entities/comercial/comercial.reducer';
import { IPerfilAhorro } from 'app/shared/model/perfil-ahorro.model';
import { getEntities as getPerfilAhorros } from 'app/entities/perfil-ahorro/perfil-ahorro.reducer';
import { IPerfilComercial } from 'app/shared/model/perfil-comercial.model';
import { getEntities as getPerfilComercials } from 'app/entities/perfil-comercial/perfil-comercial.reducer';
import { getEntity, updateEntity, createEntity, reset } from './desercion.reducer';
import { IDesercion } from 'app/shared/model/desercion.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IDesercionUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const DesercionUpdate = (props: IDesercionUpdateProps) => {
  const [idClienteId, setIdClienteId] = useState('0');
  const [idFondoId, setIdFondoId] = useState('0');
  const [idFideicomisoId, setIdFideicomisoId] = useState('0');
  const [idComercialId, setIdComercialId] = useState('0');
  const [idPerfilAhorroId, setIdPerfilAhorroId] = useState('0');
  const [idPerfilComercialId, setIdPerfilComercialId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { desercionEntity, clientes, fondos, fideicomisos, comercials, perfilAhorros, perfilComercials, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/desercion' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getClientes();
    props.getFondos();
    props.getFideicomisos();
    props.getComercials();
    props.getPerfilAhorros();
    props.getPerfilComercials();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...desercionEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="frontApp.desercion.home.createOrEditLabel">Create or edit a Desercion</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : desercionEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="desercion-id">ID</Label>
                  <AvInput id="desercion-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="fechaLabel" for="desercion-fecha">
                  Fecha
                </Label>
                <AvField id="desercion-fecha" type="date" className="form-control" name="fecha" />
              </AvGroup>
              <AvGroup>
                <Label id="edadLabel" for="desercion-edad">
                  Edad
                </Label>
                <AvField
                  id="desercion-edad"
                  type="text"
                  name="edad"
                  validate={{
                    minLength: { value: 1, errorMessage: 'This field is required to be at least 1 characters.' },
                    maxLength: { value: 3, errorMessage: 'This field cannot be longer than 3 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="nroPromedioRetirosLabel" for="desercion-nroPromedioRetiros">
                  Nro Promedio Retiros
                </Label>
                <AvField id="desercion-nroPromedioRetiros" type="string" className="form-control" name="nroPromedioRetiros" />
              </AvGroup>
              <AvGroup>
                <Label id="nroPromedioAportesLabel" for="desercion-nroPromedioAportes">
                  Nro Promedio Aportes
                </Label>
                <AvField id="desercion-nroPromedioAportes" type="string" className="form-control" name="nroPromedioAportes" />
              </AvGroup>
              <AvGroup>
                <Label id="netoPromMovimientosLabel" for="desercion-netoPromMovimientos">
                  Neto Prom Movimientos
                </Label>
                <AvField id="desercion-netoPromMovimientos" type="string" className="form-control" name="netoPromMovimientos" />
              </AvGroup>
              <AvGroup>
                <Label id="diasUltimoMovLabel" for="desercion-diasUltimoMov">
                  Dias Ultimo Mov
                </Label>
                <AvField id="desercion-diasUltimoMov" type="string" className="form-control" name="diasUltimoMov" />
              </AvGroup>
              <AvGroup check>
                <Label id="posibleSulLabel">
                  <AvInput id="desercion-posibleSul" type="checkbox" className="form-check-input" name="posibleSul" />
                  Posible Sul
                </Label>
              </AvGroup>
              <AvGroup>
                <Label for="desercion-idCliente">Id Cliente</Label>
                <AvInput id="desercion-idCliente" type="select" className="form-control" name="idCliente.id">
                  <option value="" key="0" />
                  {clientes
                    ? clientes.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="desercion-idFondo">Id Fondo</Label>
                <AvInput id="desercion-idFondo" type="select" className="form-control" name="idFondo.id">
                  <option value="" key="0" />
                  {fondos
                    ? fondos.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="desercion-idFideicomiso">Id Fideicomiso</Label>
                <AvInput id="desercion-idFideicomiso" type="select" className="form-control" name="idFideicomiso.id">
                  <option value="" key="0" />
                  {fideicomisos
                    ? fideicomisos.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="desercion-idComercial">Id Comercial</Label>
                <AvInput id="desercion-idComercial" type="select" className="form-control" name="idComercial.id">
                  <option value="" key="0" />
                  {comercials
                    ? comercials.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="desercion-idPerfilAhorro">Id Perfil Ahorro</Label>
                <AvInput id="desercion-idPerfilAhorro" type="select" className="form-control" name="idPerfilAhorro.id">
                  <option value="" key="0" />
                  {perfilAhorros
                    ? perfilAhorros.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="desercion-idPerfilComercial">Id Perfil Comercial</Label>
                <AvInput id="desercion-idPerfilComercial" type="select" className="form-control" name="idPerfilComercial.id">
                  <option value="" key="0" />
                  {perfilComercials
                    ? perfilComercials.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/desercion" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  clientes: storeState.cliente.entities,
  fondos: storeState.fondo.entities,
  fideicomisos: storeState.fideicomiso.entities,
  comercials: storeState.comercial.entities,
  perfilAhorros: storeState.perfilAhorro.entities,
  perfilComercials: storeState.perfilComercial.entities,
  desercionEntity: storeState.desercion.entity,
  loading: storeState.desercion.loading,
  updating: storeState.desercion.updating,
  updateSuccess: storeState.desercion.updateSuccess,
});

const mapDispatchToProps = {
  getClientes,
  getFondos,
  getFideicomisos,
  getComercials,
  getPerfilAhorros,
  getPerfilComercials,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DesercionUpdate);
