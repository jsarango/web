import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IDesercion, defaultValue } from 'app/shared/model/desercion.model';

export const ACTION_TYPES = {
  FETCH_DESERCION_LIST: 'desercion/FETCH_DESERCION_LIST',
  FETCH_DESERCION: 'desercion/FETCH_DESERCION',
  CREATE_DESERCION: 'desercion/CREATE_DESERCION',
  UPDATE_DESERCION: 'desercion/UPDATE_DESERCION',
  DELETE_DESERCION: 'desercion/DELETE_DESERCION',
  RESET: 'desercion/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IDesercion>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type DesercionState = Readonly<typeof initialState>;

// Reducer

export default (state: DesercionState = initialState, action): DesercionState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_DESERCION_LIST):
    case REQUEST(ACTION_TYPES.FETCH_DESERCION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_DESERCION):
    case REQUEST(ACTION_TYPES.UPDATE_DESERCION):
    case REQUEST(ACTION_TYPES.DELETE_DESERCION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_DESERCION_LIST):
    case FAILURE(ACTION_TYPES.FETCH_DESERCION):
    case FAILURE(ACTION_TYPES.CREATE_DESERCION):
    case FAILURE(ACTION_TYPES.UPDATE_DESERCION):
    case FAILURE(ACTION_TYPES.DELETE_DESERCION):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_DESERCION_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_DESERCION):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_DESERCION):
    case SUCCESS(ACTION_TYPES.UPDATE_DESERCION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_DESERCION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/desercions';

// Actions

export const getEntities: ICrudGetAllAction<IDesercion> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_DESERCION_LIST,
    payload: axios.get<IDesercion>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IDesercion> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_DESERCION,
    payload: axios.get<IDesercion>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IDesercion> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_DESERCION,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IDesercion> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_DESERCION,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IDesercion> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_DESERCION,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
